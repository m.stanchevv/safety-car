package com.teamproject.safetycar.repository;

import com.teamproject.safetycar.models.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MessagesRepository extends JpaRepository<Message, Integer> {

    List<Message> findByRespondedFalse();

    Message findById(int id);

}
