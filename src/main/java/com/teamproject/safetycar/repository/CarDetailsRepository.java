package com.teamproject.safetycar.repository;

import com.teamproject.safetycar.models.CarBrand;
import com.teamproject.safetycar.models.CarDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarDetailsRepository extends JpaRepository<CarDetails, Integer> {
}
