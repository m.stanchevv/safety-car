package com.teamproject.safetycar.repository;

import com.teamproject.safetycar.models.Coefficients;
import io.swagger.models.auth.In;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CoeffRepository extends JpaRepository<Coefficients, Integer> {

    @Query(
            value = "select accidentCoeff from Coefficients"
    )
    List<Double> getAccidentCoeff();

    @Query(
            value = "select ageCoeff from Coefficients"
    )
    List<Double> getAgeCoeff();


    @Query(
            value = "select taxCoeff from Coefficients"
    )
    List<Double> getTaxCoeff();

    @Query(
            value = "select userMaxAge from Coefficients"
    )
    List<Integer> getMinAge();

    Coefficients findFirstByActiveTrue();

}
