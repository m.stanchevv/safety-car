package com.teamproject.safetycar.repository;

import com.teamproject.safetycar.models.ReferenceTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.Date;
import java.util.List;

public interface ReferenceRepository extends JpaRepository<ReferenceTable, Integer> {


    @Query(
            value = "select baseAmount from ReferenceTable r \n" +
                    "where r.ageMin <= :age and r.ageMax >= :age\n" +
                    "and r.ccMin <=:cc and r.ccMax >=:cc\n and active = true"

    )
    Double findBaseAmount(@Param("cc") Integer cc, @Param("age") Integer age);


    List<ReferenceTable> findAllByActiveTrue();

    @Query(
            value = "select distinct r from ReferenceTable r where r.date =:fromDate"
    )
    List<ReferenceTable> findByDate (@Param("fromDate") Date fromDate);

    @Query(
            value = "select distinct r.date from ReferenceTable r"
    )
    List<Date> getDates ();
}
