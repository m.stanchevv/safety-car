package com.teamproject.safetycar.repository;

import com.teamproject.safetycar.models.Car;
import com.teamproject.safetycar.models.CarBrand;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarBrandRepository extends JpaRepository<CarBrand, Integer> {

    CarBrand findByBrand(String brand);

}
