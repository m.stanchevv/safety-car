package com.teamproject.safetycar.repository;

import com.teamproject.safetycar.models.Address;
import com.teamproject.safetycar.models.Car;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Integer> {
}
