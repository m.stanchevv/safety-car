package com.teamproject.safetycar.repository;

import com.teamproject.safetycar.models.Car;
import com.teamproject.safetycar.models.Policy;
import org.hibernate.query.criteria.internal.compile.CriteriaQueryTypeQueryAdapter;
import org.omg.PortableServer.LIFESPAN_POLICY_ID;
import org.omg.PortableServer.POA;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public interface PolicyRepository extends JpaRepository<Policy, Integer>, JpaSpecificationExecutor<Policy> {

    List<Policy> findAllByPendingTrueAndCanceledByUserFalse();

    List<Policy> findAllByApprovedTrue();

    //REJECTED
    List<Policy> findAllByApprovedFalseAndPendingFalse();

    //CANCELED BY USER

    List<Policy> findAllByCanceledByUserTrue();

    Policy findById(int id);

}
