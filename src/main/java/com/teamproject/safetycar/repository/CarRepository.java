package com.teamproject.safetycar.repository;

import com.teamproject.safetycar.models.Car;
import org.omg.PortableInterceptor.INACTIVE;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<Car, Integer> {

    Car getCarByModel (String model);
}
