package com.teamproject.safetycar.repository;

import com.teamproject.safetycar.models.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

public interface UserRepository extends JpaRepository<User, Integer> {

    boolean existsByEmail(String username);

    User findByEmail(String username);

    @Transactional
    void deleteByEmail(String username);

    User findById(int id);

}
