package com.teamproject.safetycar.service;

import com.teamproject.safetycar.exception.CustomException;
import com.teamproject.safetycar.models.Policy;
import com.teamproject.safetycar.models.Role;
import com.teamproject.safetycar.models.User;
import com.teamproject.safetycar.models.dto.UserUpdateAccountDTO;
import com.teamproject.safetycar.repository.PolicyRepository;
import com.teamproject.safetycar.repository.UserRepository;
import com.teamproject.safetycar.security.JwtTokenProvider;
import com.teamproject.safetycar.service.contracts.EmailService;
import com.teamproject.safetycar.service.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private PolicyRepository policyRepository;
    @Autowired
    private EmailService emailService;

    @Override
    public String signin(User user) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword()));
            return jwtTokenProvider.createToken(user.getEmail(), userRepository.findByEmail(user.getEmail()).getRoles());
        } catch (AuthenticationException e) {
            throw new CustomException("Invalid email/password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @Override
    public void signup(User user) {
        validateUser(user);
        String decodedPassword = user.getPassword();
        createUser(user);
        String JWToken = jwtTokenProvider.createToken(user.getEmail(), user.getRoles());
        String[] userEmail = {user.getEmail()};

        try {
            emailService.sendSSLMessage(userEmail, "accountCreated", "Activate Your Account at Safety Car", String.format(
                    "Hello %s %s,\n" +
                            "\n" +
                            "You are now registered at Safety Car.\n" +
                            "\n" +
                            "Your username: %s.\n" +
                            "Your password: %s\n" +
                            "\n" +
                            "To activate your account, please follow the link: http://localhost:8080/confirmemail?token=%s\n" +
                            "\n" + "Kind Regards,\n" +
                            "Safety Car Team", user.getFirstName(), user.getLastName(), user.getEmail(), decodedPassword, JWToken));
        } catch (MessagingException e) {
            throw new IllegalArgumentException("Something went wrong. Could not send email.");
        }

    }


    public void validateUser(User user) {
        if (user.getPassword().equals("")) {
            throw new CustomException("Password cannot be empty", HttpStatus.BAD_REQUEST);
        }
        if (!user.getPassword().matches("(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$")) {
            throw new CustomException(
                    "Password must be at least 6 characters. Must include at least one upper case, " +
                            "one lower case letter and at least one number.", HttpStatus.BAD_REQUEST);
        }
        if (userRepository.existsByEmail(user.getEmail())) {
            throw new CustomException("Email is already in use", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public void createUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        List<Role> userRoles = new ArrayList<>();
        userRoles.add(Role.ROLE_USER);
        user.setRoles(userRoles);
        user.setActive(false);
        userRepository.save(user);
    }


    @Override
    public User whoami(HttpServletRequest req) {
        return userRepository.findByEmail(jwtTokenProvider.getUsername(jwtTokenProvider.resolveToken(req)));
    }

    @Override
    public String refresh(String email) {
        return jwtTokenProvider.createToken(email, userRepository.findByEmail(email).getRoles());
    }

    @Override
    public User update(String email, UserUpdateAccountDTO newUser) {
        User userToUpdate = userRepository.findByEmail(newUser.getEmail());
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(newUser.getEmail(), newUser.getOldPassword()));
            userToUpdate.setFirstName(newUser.getFirstName());
            userToUpdate.setLastName(newUser.getLastName());
            userToUpdate.setPassword(passwordEncoder.encode(newUser.getNewPassword()));
        } catch (AuthenticationException e) {
            throw new IllegalArgumentException("The password you have entered is invalid.");
        }
        return userRepository.save(userToUpdate);
    }


    @Override
    public List<Policy> getUserPolicies(int id) {
        User user = userRepository.findById(id);
        List<Policy> userPolicies = user.getUserPolicies();
        if (userPolicies.isEmpty()) {
            throw new IllegalArgumentException(String.format("User %s has no policies!", user.getFirstName()));
        }
        return userPolicies;
    }

    @Override
    public List<Policy> getPendingPolicies(HttpServletRequest request) {
        User user = userRepository.findByEmail(jwtTokenProvider.getUsername(jwtTokenProvider.resolveToken(request)));
        return user.getUserPolicies().stream()
                .filter(Policy::isPending)
                .collect(Collectors.toList());

    }

    @Override
    public void cancelPolicyRequest(int policyId, HttpServletRequest req) {
        Policy policy = policyRepository.findById(policyId);
        User user = userRepository.findByEmail(jwtTokenProvider.getUsername(jwtTokenProvider.resolveToken(req)));
        if (policy.getUser().getEmail().equals(user.getEmail())) {
            policy.setPending(false);
            policy.setCanceledByUser(true);
            policyRepository.save(policy);
        } else {
            throw new IllegalArgumentException(String.format("User %s is not the owner of the policy.", user.getFirstName()));
        }
    }

    @Override
    public User createAgent(User user) {
        validateUser(user);

        String decodedPassword = user.getPassword();
        String[] userEmail = {user.getEmail()};

        createUser(user);
        user.getRoles().add(Role.ROLE_AGENT);

        try {
            emailService.sendSSLMessage(userEmail, "agentCreated", "Agent's Account at Safety Car", String.format(
                    "Hello %s %s,\n" +
                            "\n" +
                            "You are now an Agent at Safety Car.\n" +
                            "\n" +
                            "Your username: %s.\n" +
                            "Your password: %s\n" +
                            "\n" +
                            "In case you have any questions, please contact your supervisor.\n" +
                            "\n" + "Kind Regards,\n" +
                            "Safety Car Team", user.getFirstName(), user.getLastName(), user.getEmail(), decodedPassword));
        } catch (MessagingException e) {
            throw new IllegalArgumentException("Something went wrong. Could not send email.");
        }
        return user;

    }

    @Override
    public User search(String email) {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            throw new CustomException("The user doesn't exist", HttpStatus.NOT_FOUND);
        }
        return user;
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }



    @Override
    public void delete(String email) {
        userRepository.deleteByEmail(email);
    }


    @Override
    public void makeUserAdmin(int id) {
        User user = userRepository.findById(id);
        user.getRoles().add(Role.ROLE_ADMIN);
        userRepository.save(user);
    }


    @Override
    public void makeUserAgent(int id) {
        User user = userRepository.findById(id);
        user.getRoles().add(Role.ROLE_AGENT);
        userRepository.save(user);
    }


}
