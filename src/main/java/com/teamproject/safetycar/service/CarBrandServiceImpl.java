package com.teamproject.safetycar.service;

import com.teamproject.safetycar.models.Car;
import com.teamproject.safetycar.models.CarBrand;
import com.teamproject.safetycar.repository.CarBrandRepository;
import com.teamproject.safetycar.repository.CarRepository;
import com.teamproject.safetycar.service.contracts.CarBrandService;
import com.teamproject.safetycar.service.contracts.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarBrandServiceImpl implements CarBrandService {
    @Autowired
    private CarBrandRepository carBrandRepository;

    public CarBrandServiceImpl() {
    }

    @Override
    public List<String> getAll() {
        return carBrandRepository.findAll().stream()
                .map(CarBrand::getBrand)
                .collect(Collectors.toList());

    }

    @Override
    public CarBrand getByBrand(String brand) {
        return carBrandRepository.findByBrand(brand);
    }



}
