package com.teamproject.safetycar.service;

import com.teamproject.safetycar.criteria.PolicyRepoCustom;
import com.teamproject.safetycar.criteria.SearchCriteria;
import com.teamproject.safetycar.models.Policy;
import com.teamproject.safetycar.models.User;
import com.teamproject.safetycar.repository.*;
import com.teamproject.safetycar.security.JwtTokenProvider;
import com.teamproject.safetycar.service.contracts.EmailService;
import com.teamproject.safetycar.service.contracts.PolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Service
public class PolicyServiceImpl implements PolicyService {

    @Autowired
    private PolicyRepository repository;
    @Autowired
    private CoeffRepository coeffRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ReferenceRepository referenceRepository;
    @Autowired
    private JwtTokenProvider jwtTokenProvider;
    @Autowired
    private EmailService emailService;
    @Autowired
    private PolicyRepoCustom repoCustom;


    @Override
    public Policy create(Policy entity, HttpServletRequest request) {
        Double totalPrice = calculatePolicyPrice(entity);
        User user = userRepository.findByEmail(jwtTokenProvider.getUsername(jwtTokenProvider.resolveToken(request)));
        user.getUserPolicies().add(entity);
        entity.setUser(user);
        entity.setTotalPrice(totalPrice);

        repository.save(entity);
        userRepository.save(user);
        return entity;
    }

    public List<Policy> search(String criteria) {
        List<SearchCriteria> params = new ArrayList<>();
        if (criteria != null) {
            Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w.+?|\\d{4}-\\d{2}-\\d{2}$+?),");
            Matcher matcher = pattern
                    .matcher(criteria + ",");
            while (matcher.find()) {
                params.add(new SearchCriteria(matcher.group(1), matcher.group(2), matcher.group(3)));
            }
        }
        return repoCustom.searchPolicy(params);
    }


    @Override
    public List<Policy> getAll() {
        List<Policy> allPolicies = repository.findAll();
        if (allPolicies.isEmpty()) {
            throw new NullPointerException("There aren't any policies yet.");
        }
        return allPolicies;
    }

    @Override
    public Policy update(Policy updated) {
        try {
            return repository.save(updated);
        } catch (RuntimeException e) {
            throw new IllegalArgumentException();
        }
    }


    public Double calculatePolicyPrice(Policy policy) {
        double accidentCoeff = 1;
        double ageCoeff = 1;
        if (policy.getCarDetails().isAccidents()) {
            accidentCoeff = coeffRepository.getAccidentCoeff().get(0);
        }
        if (policy.getDriverDetails().getAge() < coeffRepository.getMinAge().get(0)) {
            ageCoeff = coeffRepository.getAgeCoeff().get(0);
        }
        double netPremium = referenceRepository.findBaseAmount(
                policy.getCarDetails().getCubic_capacity(), getCarAge(policy)) * accidentCoeff * ageCoeff;
        double taxAmount = coeffRepository.getTaxCoeff().get(0) * netPremium;
        return netPremium + taxAmount;
    }

    public Policy getPolicy(int id) {
        Policy policy = repository.findById(id);
        if (policy == null) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Policy not found");
        }
        return policy;
    }

    private int getCarAge(Policy offer) {
        Date regDate = offer.getCarDetails().getRegDate();
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Bulgaria"));
        calendar.setTime(regDate);
        int regYear = calendar.get(Calendar.YEAR);
        System.out.println(regYear);

        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        System.out.println(currentYear);
        System.out.println(currentYear - regYear);
        return currentYear - regYear;
    }


    @Override
    public Policy approvePolicy(int policyId) {
        Policy policy = repository.findById(policyId);
        policy.setApproved(true);
        policy.setPending(false);
        String[] userEmail = {policy.getDriverDetails().getEmail()};

        repository.save(policy);
        try {
            emailService.sendSSLMessage(userEmail, "approve", null, null);
        } catch (MessagingException e) {
            throw new IllegalArgumentException("Something went wrong. Could not send email.");
        }
        return policy;
    }

    @Override
    public Policy rejectPolicy(int policyId) {
        Policy policy = repository.findById(policyId);
        policy.setApproved(false);
        policy.setPending(false);
        repository.save(policy);
        String[] userEmail = {policy.getDriverDetails().getEmail()};
        try {
            emailService.sendSSLMessage(userEmail, "reject", null, null);
        } catch (MessagingException e) {
            throw new IllegalArgumentException("Something went wrong. Could not send email.");
        }
        return policy;
    }


    @Override
    public List<Policy> getPendingPolicies() {
        try {
            return repository.findAllByPendingTrueAndCanceledByUserFalse();
        } catch (RuntimeException e) {
            throw new IllegalArgumentException("No pending policies");
        }
    }

    @Override
    public List<Policy> getApprovedPolicies() {
        try {
            return repository.findAllByApprovedTrue();
        } catch (RuntimeException e) {
            throw new IllegalArgumentException("No approved policies");
        }
    }

    @Override
    public List<Policy> getRejectedPolicies() {
        try {
            return repository.findAllByApprovedFalseAndPendingFalse();
        } catch (RuntimeException e) {
            throw new IllegalArgumentException("No pending policies");
        }
    }

    @Override
    public List<Policy> getCanceledByUser() {
        try {
            return repository.findAllByCanceledByUserTrue();
        } catch (RuntimeException e) {
            throw new IllegalArgumentException("No canceled by user policies");
        }
    }

    @Override
    public String getPolicyPicture(int id) {
        Policy policy = repository.findById(id);
        return policy.getCarDetails().getImageRegCertificate();
    }


}
