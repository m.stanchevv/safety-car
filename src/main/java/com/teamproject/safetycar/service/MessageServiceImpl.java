package com.teamproject.safetycar.service;

import com.teamproject.safetycar.models.Car;
import com.teamproject.safetycar.models.Message;
import com.teamproject.safetycar.models.dto.MessageResponseDTO;
import com.teamproject.safetycar.repository.CarRepository;
import com.teamproject.safetycar.repository.MessagesRepository;
import com.teamproject.safetycar.service.contracts.CarService;
import com.teamproject.safetycar.service.contracts.EmailService;
import com.teamproject.safetycar.service.contracts.MessagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import javax.mail.MessagingException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MessageServiceImpl implements MessagesService {
    @Autowired
    private MessagesRepository messagesRepository;

    @Autowired
    private EmailService emailService;

    public MessageServiceImpl() {
    }


    @Override
    public List<Message> getAllWaitingResponse() {
        return messagesRepository.findByRespondedFalse();
    }

    @Override
    public List<Message> getAll() {
        return messagesRepository.findAll();
    }

    @Override
    public Message create(Message message) {
        return messagesRepository.save(message);
    }

    @Override
    public void respond(MessageResponseDTO message) {
        String[] userEmail = {message.getEmail()};
        Message initialMessage = getMessageById(message.getInitialMessageID());
//        System.out.println("RESPOND INITIAL MESSAGE SENT DATA:");
//        System.out.println(initialMessage.getEmail());
//        System.out.println(initialMessage.getName());
//        System.out.println(initialMessage.getId());
//        System.out.println(initialMessage.isResponded());
        initialMessage.setResponded(true);
        messagesRepository.save(initialMessage);
//        System.out.println(initialMessage.isResponded());
        try {
            emailService.sendSSLMessage(userEmail, "messageResponse", message.getSubject(), message.getText());
        } catch (MessagingException e) {
            throw new IllegalArgumentException("Something went wrong. Could not send email.");
        }
    }

    @Override
    public Message getMessageById(int id) {
        return messagesRepository.findById(id);
    }

}
