package com.teamproject.safetycar.service;

import com.teamproject.safetycar.models.ReferenceTable;
import com.teamproject.safetycar.repository.ReferenceRepository;
import com.teamproject.safetycar.service.contracts.ReferenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class ReferenceServiceImp implements ReferenceService {

    @Autowired
    private ReferenceRepository repository;

    @Override
    public List<ReferenceTable> findByDate(String date) {
        Date date1;
        int lastday = Integer.parseInt(date.substring(date.length()-1));
        lastday++;
        String d = date.substring(0,date.length()-1) + lastday;
        try {
            date1 = new SimpleDateFormat("yyyy-MM-dd").parse(d);
            return repository.findByDate(date1);
        }catch (Exception e ){
            throw new IllegalArgumentException("Wrong Date format");
        }
    }

    @Override
    public List<ReferenceTable> getActive() {
        return repository.findAllByActiveTrue();
    }

    @Override
    public List<Date> getDates() {
        return repository.getDates();
    }
}
