package com.teamproject.safetycar.service;

import com.teamproject.safetycar.models.Car;
import com.teamproject.safetycar.repository.CarBrandRepository;
import com.teamproject.safetycar.repository.CarRepository;
import com.teamproject.safetycar.service.contracts.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarServiceImpl implements CarService {
    @Autowired
    private CarRepository carRepository;


    public CarServiceImpl() {
    }

    @Override
    public List<Car> getAll() {
        return carRepository.findAll();

    }
    @Override
    public List<String> getAllModelsForBrand(String brand){
        return carRepository.findAll().stream()
                .filter(car -> car.getCarBrand().getBrand().equals(brand))
                .map(Car::getModel)
                .collect(Collectors.toList());
    }

}
