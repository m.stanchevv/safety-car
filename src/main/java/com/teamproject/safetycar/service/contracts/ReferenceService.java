package com.teamproject.safetycar.service.contracts;

import com.teamproject.safetycar.models.ReferenceTable;

import java.util.Date;
import java.util.List;

public interface ReferenceService {

    List<ReferenceTable> findByDate (String date);
    List<ReferenceTable> getActive ();
    List <Date> getDates();

}
