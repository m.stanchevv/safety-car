package com.teamproject.safetycar.service.contracts;

import com.teamproject.safetycar.models.Car;
import com.teamproject.safetycar.models.CarBrand;

import java.util.List;

public interface CarBrandService {
    List<String> getAll();

    CarBrand getByBrand(String brand);

}
