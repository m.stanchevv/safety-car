package com.teamproject.safetycar.service.contracts;

import javax.mail.MessagingException;

public interface EmailService {
    void sendSSLMessage(String[] recipients, String status, String subject, String message) throws MessagingException;

    String confirmEmail(String token);
}
