package com.teamproject.safetycar.service.contracts;

import com.teamproject.safetycar.models.Policy;
import org.springframework.data.repository.query.Param;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

public interface PolicyService {


    Double calculatePolicyPrice(Policy policy);

    Policy getPolicy(int id);

    List<Policy> getAll();

    Policy create(Policy entity, HttpServletRequest request);

    Policy update(Policy updated);

    Policy approvePolicy(int policyId);

    Policy rejectPolicy(int policyId);

    List<Policy> getPendingPolicies();

    List<Policy> getApprovedPolicies();

    List<Policy> getRejectedPolicies();

    List<Policy> getCanceledByUser();

    List<Policy> search(String criteria);

    String getPolicyPicture(int id);

}
