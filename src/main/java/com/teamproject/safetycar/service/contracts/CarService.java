package com.teamproject.safetycar.service.contracts;

import com.teamproject.safetycar.models.Car;

import java.io.Serializable;
import java.util.List;

public interface CarService{
    List<Car> getAll();
    List<String> getAllModelsForBrand(String brand);
}
