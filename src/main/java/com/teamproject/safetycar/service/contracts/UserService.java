package com.teamproject.safetycar.service.contracts;

import com.teamproject.safetycar.models.Policy;
import com.teamproject.safetycar.models.User;
import com.teamproject.safetycar.models.dto.UserUpdateAccountDTO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface UserService {

    String signin(User user);

    void signup(User user);

    User whoami(HttpServletRequest req);

    String refresh(String email);

    User update(String email, UserUpdateAccountDTO userUpdateAccountDTO);

    List<Policy> getUserPolicies(int id);

    void cancelPolicyRequest(int policyId, HttpServletRequest req);

    List<Policy> getPendingPolicies(HttpServletRequest request);

    void validateUser(User user);

    void createUser(User user);

    User createAgent(User user);

    User search(String email);

    void delete(String email);

    void makeUserAdmin(int id);

    void makeUserAgent(int id);

    List<User> getAllUsers();


}
