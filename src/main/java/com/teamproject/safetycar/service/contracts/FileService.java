package com.teamproject.safetycar.service.contracts;

import com.teamproject.safetycar.models.ReferenceTable;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface FileService {
    void readFile() throws Exception;

    File getCurrentCSV();

}
