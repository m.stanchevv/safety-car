package com.teamproject.safetycar.service.contracts;

import com.teamproject.safetycar.models.Car;
import com.teamproject.safetycar.models.Message;
import com.teamproject.safetycar.models.dto.MessageResponseDTO;

import java.util.List;

public interface MessagesService {
    List<Message> getAllWaitingResponse();

    List<Message> getAll();

    Message create(Message message);

    void respond(MessageResponseDTO message);

    Message getMessageById(int id);

}
