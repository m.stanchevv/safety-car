package com.teamproject.safetycar.service.contracts;

import com.teamproject.safetycar.models.Policy;

import java.util.List;

public interface Service<T> {
    T create(T entity);

    List<T> getAll();

    T update(T updated);

    T getbyID(int id);

    void delete(int id);
}
