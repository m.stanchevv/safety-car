package com.teamproject.safetycar.controllers;

import com.teamproject.safetycar.models.Policy;
import com.teamproject.safetycar.service.contracts.PolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/agent")
@PreAuthorize("hasRole('ROLE_AGENT') or hasRole('ROLE_ADMIN')")
public class AgentController {

    private PolicyService service;

    @Autowired
    public AgentController(PolicyService service) {
        this.service = service;
    }

    @PostMapping("approve/{policyId}")
    public Policy approvePolicy(@PathVariable int policyId) {
        try {
            return service.approvePolicy(policyId);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("reject/{policyId}")
    public Policy rejectPolicy(@PathVariable int policyId) {
        try {
            return service.rejectPolicy(policyId);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/pending")
    public List<Policy> getPendingPolicies() {
//        try {
            return service.getPendingPolicies();
//        } catch (IllegalArgumentException e) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
//        }
    }

    @GetMapping("/approved")
    public List<Policy> getApproved() {
        try {
            return service.getApprovedPolicies();
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/rejected")
    public List<Policy> getRejected() {
        try {
            return service.getRejectedPolicies();
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/canceledByUser")
    public List<Policy> getCanceles() {
        try {
            return service.getCanceledByUser();
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
}
