package com.teamproject.safetycar.controllers;

import com.teamproject.safetycar.models.Car;
import com.teamproject.safetycar.service.contracts.CarBrandService;
import com.teamproject.safetycar.service.contracts.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/cars")
public class CarController {

    private CarService carService;
    private CarBrandService carBrandService;

    @Autowired
    public CarController(CarService carService, CarBrandService carBrandService) {
        this.carService = carService;
        this.carBrandService = carBrandService;
    }

    @GetMapping
    public List<Car> getAll (){
        return carService.getAll();
    }

//    @GetMapping("/{model}")
//    public Car getByModel (@PathVariable String model){
//        return carService.getByModel(model);
//    }

    @GetMapping("/{brand}")
    public List<String> getAllModelsForBrand(@PathVariable String brand){
//        System.out.println(carService.getAllModelsForBrand(brand));
       return carService.getAllModelsForBrand(brand);
    }
    @GetMapping("/brands")
    public List<String> getAllCarBrands (){
        System.out.println("Brands requested");
        return carBrandService.getAll();
    }

//    @GetMapping("/brands/{brand}")
//    public Car getByCarBrand (@PathVariable String brand){
//        return carBrandService.getByBrand(brand);
//    }

}
