package com.teamproject.safetycar.controllers;

import com.teamproject.safetycar.models.Message;
import com.teamproject.safetycar.models.Policy;
import com.teamproject.safetycar.models.User;
import com.teamproject.safetycar.models.dto.MessageResponseDTO;
import com.teamproject.safetycar.models.dto.UserFullDTO;
import com.teamproject.safetycar.service.contracts.MessagesService;
import com.teamproject.safetycar.service.contracts.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/messages")
public class MessageController {

    private MessagesService messagesService;

    @Autowired
    public MessageController(MessagesService messagesService) {
        this.messagesService = messagesService;
    }


    @PostMapping("/new")
    public Message sendMessage(@RequestBody Message message) {
        try {
            return messagesService.create(message);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    e.getMessage());
        }
    }


    @GetMapping("/pending")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<Message> getWaitingResponse() {
        try {
            return messagesService.getAllWaitingResponse();
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }


    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<Message> getALL() {
        try {
            return messagesService.getAll();
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
    @GetMapping("/{id}")
//    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Message getMessageById(@PathVariable int id) {
        try {
            return messagesService.getMessageById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/respond")
    public void senResponse(@RequestBody MessageResponseDTO message) {
        try {
            messagesService.respond(message);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    e.getMessage());
        }
    }


}
