package com.teamproject.safetycar.controllers;

import com.teamproject.safetycar.models.Policy;
import com.teamproject.safetycar.models.dto.ModdelMapperCustom;
import com.teamproject.safetycar.models.dto.PolicyFullDTO;
import com.teamproject.safetycar.models.dto.PolicyOfferDTO;
import com.teamproject.safetycar.repository.CarRepository;
import com.teamproject.safetycar.service.contracts.PolicyService;
import com.teamproject.safetycar.storage.StorageFileNotFoundException;
import com.teamproject.safetycar.storage.PicturesStorageService;
import com.teamproject.safetycar.storage.UploadFileResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/policies")
public class PolicyController {

    private final PicturesStorageService storageService;

    private CarRepository carRepository;

    private PolicyService service;

//    private EmailService emailService;

    @Autowired
    public PolicyController(PolicyService service, PicturesStorageService storageService, CarRepository carRepository) {
        this.service = service;
        this.storageService = storageService;
        this.carRepository = carRepository;
//        this.emailService = emailService;
    }


    @GetMapping("/search")
    public List<Policy> search(@RequestParam String criteria) {
        return service.search(criteria);
    }


    @GetMapping
//    @PreAuthorize("hasRole('ROLE_AGENT') or hasAnyRole('ROLE_ADMIN')")
    public List<Policy> getAll() {
        return service.getAll();
    }


    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_AGENT') or hasAnyRole('ROLE_ADMIN')")
    public Policy getById(@PathVariable int id) {
        return service.getPolicy(id);
    }

//    @PostMapping("/sendmail")
//    public void sendmail (){
//        emailService.sendSimpleMessage("","","");
//
//    }

    @PostMapping("/new")
    public Policy createPolicy(@RequestBody @Valid PolicyFullDTO policyFullDTO, HttpServletRequest request) {
        try {
            ModdelMapperCustom mapperCustom = new ModdelMapperCustom(carRepository);
            Policy policy = mapperCustom.mapPolicy(policyFullDTO);
            service.create(policy, request);
            return policy;
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    e.getMessage());
        }
    }

    @PutMapping("/totalprice")
    public Double getTotalPrice(@RequestBody @Valid PolicyOfferDTO policyOfferDTO) {
        try {
            ModdelMapperCustom mapperCustom = new ModdelMapperCustom(carRepository);
            Policy policy = mapperCustom.mapOffer(policyOfferDTO);
            return service.calculatePolicyPrice(policy);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


    @PostMapping("/uploadPicture")
    public UploadFileResponse uploadPicture(@RequestParam("file") MultipartFile file) {
        try {
            String pictureName = storageService.storePicture(file);
            String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/picturesUploaded/")
                    .path(pictureName)
                    .toUriString();
            return new UploadFileResponse(pictureName, fileDownloadUri,
                    file.getContentType(), file.getSize());
        } catch (RuntimeException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }

    @GetMapping("/{id}/policypicture")
    public String getPolicyPicture(@PathVariable int id) {
        try {
            return service.getPolicyPicture(id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


}
