package com.teamproject.safetycar.controllers;

import com.teamproject.safetycar.models.ReferenceTable;
import com.teamproject.safetycar.service.contracts.FileService;
import com.teamproject.safetycar.service.contracts.ReferenceService;
import com.teamproject.safetycar.storage.FileStorageService;
import com.teamproject.safetycar.storage.StorageFileNotFoundException;
import com.teamproject.safetycar.storage.UploadFileResponse;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/table")
//@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_AGENT')")
public class ReferenceTableController {
    private final FileStorageService storageService;
    private FileService reader;
    private ReferenceService service;

    @Autowired
    public ReferenceTableController(FileStorageService storageService, FileService reader, ReferenceService service) {
        this.storageService = storageService;
        this.reader = reader;
        this.service = service;
    }

    @PostMapping("/uploadFile")
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file) {
        String fileName = storageService.storeFile(file);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/filesUploaded/")
                .path(fileName)
                .toUriString();

        return new UploadFileResponse(fileName, fileDownloadUri,
                file.getContentType(), file.getSize());
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/update")
    public String update() throws Exception {
        reader.readFile();
        return "mina";
    }


    @GetMapping()
    public List<ReferenceTable> findByDate(@RequestParam String date){
        System.out.println(service.findByDate(date));
        try {
           return service.findByDate(date);

        }catch (IllegalArgumentException e ){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
        }
    }
    @GetMapping("/active")
    public List<ReferenceTable> getActive(){
        try {
            return service.getActive();
        }catch (IllegalArgumentException e ){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
        }
    }

    @GetMapping("/dates")
    public List<Date> getDates(){
        try {
            return service.getDates();

        }catch (IllegalArgumentException e ){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
        }
    }



}

