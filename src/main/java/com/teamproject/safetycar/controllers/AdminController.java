package com.teamproject.safetycar.controllers;

import com.teamproject.safetycar.models.User;
import com.teamproject.safetycar.models.dto.UserFullDTO;
import com.teamproject.safetycar.models.dto.UserResponseDTO;
import com.teamproject.safetycar.service.contracts.AdminService;
import com.teamproject.safetycar.service.contracts.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/admin")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class AdminController {

    private UserService userService;
    private ModelMapper modelMapper;

    @Autowired
    public AdminController(UserService userService, ModelMapper modelMapper) {
        this.userService = userService;
        this.modelMapper = modelMapper;

    }

    @PutMapping("/makeadmin/{userId}")
    public void makeUserAdmin(@PathVariable int userId) {
        try {
            userService.makeUserAdmin(userId);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage()
            );
        }
    }

    @PutMapping("/makeagent/{userId}")
    public void makeUserAgent(@PathVariable int userId) {
        try {
            userService.makeUserAgent(userId);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage()
            );
        }
    }

    @DeleteMapping(value = "/{username}")
    public String delete(@PathVariable String username) {
        userService.delete(username);
        return username;
    }

    @PostMapping("/createagent")
    public User createAgent(@RequestBody UserFullDTO user) {
        try {
            return userService.createAgent(modelMapper.map(user, User.class));
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    e.getMessage());
        }
    }

    @GetMapping(value = "/{email}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_AGENT')")
    public UserResponseDTO search(@PathVariable String email) {
        return modelMapper.map(userService.search(email), UserResponseDTO.class);
    }


    @GetMapping("/users")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<User> getAllUsers() {
        try {
            return userService.getAllUsers();
        } catch (RuntimeException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    e.getMessage()
            );
        }
    }
}
