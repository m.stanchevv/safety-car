package com.teamproject.safetycar.controllers;

import com.teamproject.safetycar.exception.CustomException;
import com.teamproject.safetycar.models.Policy;
import com.teamproject.safetycar.models.Role;
import com.teamproject.safetycar.models.User;
import com.teamproject.safetycar.models.dto.*;
import com.teamproject.safetycar.service.contracts.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping("/signin")
    public String login(@RequestBody UserSignInDTO userSignInDTO) {

        return userService.signin(modelMapper.map(userSignInDTO, User.class));
    }

    @PostMapping("/signup")
    public void signup(@RequestBody UserFullDTO user) {
        try {
            userService.signup(modelMapper.map(user, User.class));
        } catch (CustomException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    e.getMessage());
        }
    }

    @GetMapping(value = "/me")
    public UserResponseDTO whoami(HttpServletRequest req) {
        return modelMapper.map(userService.whoami(req), UserResponseDTO.class);
    }

    @GetMapping("/refresh")
    public String refresh(HttpServletRequest req) {
        return userService.refresh(req.getRemoteUser());
    }

    @PostMapping("cancel/{policyId}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public void cancelRequest(@PathVariable int policyId, HttpServletRequest req) {
        try {
            userService.cancelPolicyRequest(policyId, req);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/editaccount")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER') or hasRole('ROLE_AGENT')")
    public User update(@RequestBody @Valid UserUpdateAccountDTO userUpdateAccountDTO) {
        try {
            return userService.update(userUpdateAccountDTO.getEmail(), userUpdateAccountDTO);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    e.getMessage()
            );
        }
    }

    @GetMapping("/pendingpolicies")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER') or hasRole('ROLE_AGENT')")
    public List<Policy> getPendingPolicies(HttpServletRequest request) {
        try {
            return userService.getPendingPolicies(request);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    e.getMessage()
            );
        }
    }


    @GetMapping("/history/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER') or hasRole('ROLE_AGENT')")
    public List<Policy> getPolicyHistory(@PathVariable int id) {
        try {
            return userService.getUserPolicies(id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    e.getMessage()
            );
        }
    }



//    @GetMapping(value = "/{username}")
//    public UserResponseDTO getUser(@PathVariable String username) {
//        User user = userService.search(username);
//        List<Role> roles = new ArrayList<>();
//        roles.add(Role.ROLE_USER);
//        roles.add(Role.ROLE_ADMIN);
//        user.setRoles(roles);
//        return modelMapper.map(userService.search(username), UserResponseDTO.class);
//    }


}

