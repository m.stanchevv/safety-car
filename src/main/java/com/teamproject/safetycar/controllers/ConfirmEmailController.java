package com.teamproject.safetycar.controllers;

import com.teamproject.safetycar.service.contracts.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ConfirmEmailController {

    @Autowired
    private EmailService emailService;

    @RequestMapping(value = "/confirmemail", method = RequestMethod.GET)
    public String confirmEmail(@RequestParam String token) {
        emailService.confirmEmail(token);
        return "emailConfirmationPage";
    }
}
