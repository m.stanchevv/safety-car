package com.teamproject.safetycar.controllers;

import com.teamproject.safetycar.models.Policy;
import com.teamproject.safetycar.models.dto.ModdelMapperCustom;
import com.teamproject.safetycar.models.dto.PolicyOfferDTO;
import com.teamproject.safetycar.service.contracts.PolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/offer")
public class OfferController {

    @Autowired
    private PolicyService service;

    @PostMapping
    public double getOffer(@RequestBody PolicyOfferDTO offer) {
        try {
            ModdelMapperCustom mapperCustom = new ModdelMapperCustom();
            Policy policy = mapperCustom.mapOffer(offer);
            return service.calculatePolicyPrice(policy);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }


}
