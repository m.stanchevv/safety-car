package com.teamproject.safetycar.criteria;

import com.teamproject.safetycar.models.Policy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Component
public class PolicyRepoCustomImp implements PolicyRepoCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Policy> searchPolicy(List<SearchCriteria> params) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Policy> query = builder.createQuery(Policy.class);
        Root root = query.from(Policy.class);

        Predicate predicate = builder.conjunction();

        PolicySearchQueryCriteriaConsumer searchConsumer =
                new PolicySearchQueryCriteriaConsumer(predicate, builder, root);
        params.forEach(searchConsumer);
        predicate = searchConsumer.getPredicate();
        query.where(predicate);

        List<Policy> result = entityManager.createQuery(query).getResultList();
        return result;
    }
}
