package com.teamproject.safetycar.criteria;

import com.teamproject.safetycar.models.*;

import javax.persistence.criteria.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

public class PolicySearchQueryCriteriaConsumer implements Consumer<SearchCriteria> {

    private static final List<String> carDetailsFields = new ArrayList<>(Arrays.asList("cubic_capacity", "regDate", "accidents"));
    private static final List<String> carFields = new ArrayList<>(Arrays.asList("model"));
    private static final List<String> carBrandFields = new ArrayList<>(Arrays.asList("brand"));
    private static final List<String> userFields = new ArrayList<>(Arrays.asList("firstName", "lastName"));
    private static final List<String> driverDetailsFields = new ArrayList<>(Arrays.asList("age", "phone", "email"));

    private Predicate predicate;
    private CriteriaBuilder builder;
    private Root<Policy> root;

    PolicySearchQueryCriteriaConsumer(Predicate predicate, CriteriaBuilder builder,
                                      Root<Policy> r) {
        this.predicate = predicate;
        this.builder = builder;
        this.root = r;
    }

    Predicate getPredicate() {
        return predicate;
    }

    public void setPredicate(Predicate predicate) {
        this.predicate = predicate;
    }

    public CriteriaBuilder getBuilder() {
        return builder;
    }

    public void setBuilder(CriteriaBuilder builder) {
        this.builder = builder;
    }

    public Root getRoot() {
        return root;
    }

    public void setRoot(Root root) {
        this.root = root;
    }


    @Override
    public void accept(SearchCriteria param) {
        if (carDetailsFields.contains(param.getKey())) {
            Join<Policy, CarDetails> carDetails = root.join("carDetails");
            build(param, carDetails);
        } else if (carFields.contains(param.getKey())) {
            Join<Policy, CarDetails> carDetails = root.join("carDetails");
            Join<CarDetails, Car> car = carDetails.join("car");
            build(param, car);
        } else if (carBrandFields.contains(param.getKey())) {
            Join<Policy, CarDetails> carDetails = root.join("carDetails");
            Join<CarDetails, Car> car = carDetails.join("car");
            Join<Car, CarBrand> carBrand = car.join("carBrand");
            build(param, carBrand);
        } else if (driverDetailsFields.contains(param.getKey())) {
            Join<Policy, DriverDetails> driverDetails = root.join("driverDetails");
            build(param, driverDetails);
        } else if (userFields.contains(param.getKey())) {
            Join<Policy, User> user = root.join("user");
            build(param, user);
        } else {
            build(param, root);
        }
    }

    private void build(SearchCriteria param, From from) {
        if (param.getOperation().equalsIgnoreCase(">")) {
            if (from.get(param.getKey()).getJavaType() == Date.class) {
                doDate(param, from);
            } else {
                predicate = builder.and(predicate, builder
                        .greaterThanOrEqualTo(from.get(param.getKey()), param.getValue().toString()));
            }
        } else if (param.getOperation().equalsIgnoreCase("<")) {
            if (from.get(param.getKey()).getJavaType() == Date.class) {
                doDate(param, from);
            } else {
                predicate = builder.and(predicate, builder.lessThanOrEqualTo(
                        from.get(param.getKey()), param.getValue().toString()));
            }
        } else if (param.getOperation().equalsIgnoreCase(":")) {
            if (from.get(param.getKey()).getJavaType() == String.class) {
                predicate = builder.and(predicate, builder.like(
                        from.get(param.getKey()), "%" + param.getValue() + "%"));
            } else if (from.get(param.getKey()).getJavaType() == boolean.class) {
                if (param.getValue().toString().equals("true")) {
                    predicate = builder.and(predicate, builder.isTrue(from.get(param.getKey())));
                } else if (param.getValue().toString().equals("false")) {
                    predicate = builder.and(predicate, builder.isFalse(from.get(param.getKey())));
                }
            } else if (from.get(param.getKey()).getJavaType() == Date.class) {
                doDate(param, from);
            } else {
                predicate = builder.and(predicate, builder.equal(from.get(param.getKey()), param.getValue()));
            }
        }
    }

    private void doDate(SearchCriteria param, From from) {
        Date date = parseDate(param.getValue().toString());
        switch (param.getOperation()) {
            case ">":
                predicate = builder.and(predicate, builder.greaterThanOrEqualTo(from.get(param.getKey()), date));
                return;
            case ":":
                predicate = builder.and(predicate, builder.equal(from.get(param.getKey()), date));
                return;
            case "<":
                predicate = builder.and(predicate, builder.lessThanOrEqualTo(from.get(param.getKey()), date));
                return;
        }
    }

    private Date parseDate(String date) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (Exception e) {
            throw new IllegalArgumentException("Wrong date format!");
        }
    }
}
