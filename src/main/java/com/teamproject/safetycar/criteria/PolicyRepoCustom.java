package com.teamproject.safetycar.criteria;

import com.teamproject.safetycar.models.Policy;

import java.util.List;

public interface PolicyRepoCustom {

    List<Policy> searchPolicy(List<SearchCriteria> params);
}
