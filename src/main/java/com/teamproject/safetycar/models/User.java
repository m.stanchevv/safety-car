package com.teamproject.safetycar.models;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "first_name")
    @Size(min = 2, max = 30, message = "First name must be between 2 and 30 characters.")
    private String firstName;


    @Column(name = "last_name")
    @Size(min = 2, max = 30, message = "Last name must be between 2 and 30 characters.")
    private String lastName;


    @Column(name = "password")
    @Pattern(regexp = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$",
            message = "Password must be at least 6 characters. Must include at least one upper case, one lower case letter and at least one number.")
    private String password;

    @Column(name = "email")
    @Email(message = "Email should be valid")
    private String email;

    @ElementCollection(fetch = FetchType.EAGER)
    private List<Role> roles;

    @Column(name = "active")
    private boolean isActive;

    @OneToMany(mappedBy = "user")
    private List<Policy> userPolicies = new ArrayList<>();

    public User() {
    }

    public User(@Pattern(regexp = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$",
            message = "Password must be at least 6 characters. Must include at least one upper case," +
                    "one lower case letter and at least one number.") String password,
                @Email(message = "Email should be valid") String email) {
        this.password = password;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public List<Policy> getUserPolicies() {
        return new ArrayList<>(userPolicies);
    }

    public void setUserPolicies(List<Policy> userPolicies) {
        this.userPolicies = new ArrayList<>(userPolicies);
    }
}

