package com.teamproject.safetycar.models.dto;

import com.teamproject.safetycar.models.*;
import com.teamproject.safetycar.repository.CarBrandRepository;
import com.teamproject.safetycar.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class ModdelMapperCustom {

    private CarRepository carRepository;

    @Autowired
    public ModdelMapperCustom(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public ModdelMapperCustom() {
    }

    public Policy mapOffer(PolicyOfferDTO offerDTO) {
        DriverDetails driverDetails = new DriverDetails();
        System.out.println(offerDTO.getDriverAge());
        driverDetails.setAge(offerDTO.getDriverAge());

        Car car = new Car();
        CarBrand carBrand = new CarBrand();
        carBrand.setBrand(offerDTO.getCarBrand());
        car.setCarBrand(carBrand);
        car.setModel(offerDTO.getCarModel());

        CarDetails details = new CarDetails();
        details.setAccidents(offerDTO.isHasAccidents());
        details.setCubic_capacity(offerDTO.getCc());
        System.out.println(offerDTO.getFirstRegDate());
//        validateDate(offerDTO.getFirstRegDate());

        details.setRegDate(offerDTO.getFirstRegDate());
        details.setCar(car);

        Policy policy = new Policy();
        policy.setCarDetails(details);
        policy.setDriverDetails(driverDetails);
        return policy;
    }


    public Policy mapPolicy(PolicyFullDTO policyFullDTO) {
        Address address = new Address();
        address.setText(policyFullDTO.getAddressText());
        address.setNo(policyFullDTO.getAddressNumber());
        address.setPostalCode(policyFullDTO.getAddressPostCode());
        address.setCity(policyFullDTO.getAddressCity());

        DriverDetails driverDetails = new DriverDetails();
        driverDetails.setAge(policyFullDTO.getDriversAge());
        driverDetails.setEmail(policyFullDTO.getEmail());
        driverDetails.setPhone(policyFullDTO.getPhoneNumber());
        driverDetails.setAddress(address);


        //TODO make it better!!
        List<Car> car = carRepository.findAll().stream()
                .filter(car1 -> car1.getModel().equals(policyFullDTO.getCarModel())
                        && car1.getCarBrand().getBrand().equals(policyFullDTO.getCarBrand()))
                .collect(Collectors.toList());

        CarDetails carDetails = new CarDetails();
        carDetails.setAccidents(policyFullDTO.isHasAccidents());
        carDetails.setCubic_capacity(policyFullDTO.getCc());

//        validateDate(policyFullDTO.getFirstRegDate());

        carDetails.setRegDate(policyFullDTO.getFirstRegDate());
        carDetails.setImageRegCertificate(policyFullDTO.getImageRegCertificate());
        try {
            carDetails.setCar(car.get(0));
        }catch (IndexOutOfBoundsException e){
            throw new IllegalArgumentException("Car model or car brand is empty");
        }

        Policy policy = new Policy();
        policy.setCarDetails(carDetails);

        policy.setDriverDetails(driverDetails);
        policy.setPending(true);
        policy.setApproved(false);

//        validateDate(policyFullDTO.getActivationDate());

        policy.setStatDate(policyFullDTO.getActivationDate());
        policy.setCanceledByUser(false);

        policy.setOrderDate(new Date());
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//        String dateOrder = sdf.format(new Date());
//        policy.setOrderDate(dateOrder);
        return policy;
    }


//    public Policy mapOffer (PolicyOfferDTO offerDTO){
//        Policy policy = new Policy();
//        DriverDetails driverDetails = new DriverDetails();
//        driverDetails.setAge(offerDTO.getDriverAge());
//        Car car = new Car();
//        CarBrand brand = new CarBrand();
//        brand.setBrand(offerDTO.getCarBrand());
//        car.setCarBrand(brand);
//        car.setModel(offerDTO.getCarModel());
//        CarDetails carDetails = new CarDetails();
//        carDetails.setCar(car);
//        carDetails.setAccidents(offerDTO.hasAccidents());
//        carDetails.setRegYear(offerDTO.getCarAge());
//        policy.setCarDetails(carDetails);
//        policy.setDriverDetails(driverDetails);
//        return policy;
//    }


//    private static void validateDate(String intputDate) {
//        Date date = null;
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
//            System.out.println(intputDate);
//            date = sdf.parse(intputDate);
//            if (!intputDate.equals(sdf.format(date))) {
//                date = null;
//            }
//        } catch (ParseException ex) {
//            ex.printStackTrace();
//        }
//        if (date == null) {
//            throw new IllegalArgumentException("Invalid date format.");
//        }
//    }
}
