package com.teamproject.safetycar.models.dto;

import org.springframework.format.annotation.NumberFormat;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.*;
import java.util.Date;

public class PolicyFullDTO {

    private String carBrand;
    private String carModel;

    @Min(value = 0, message = "Cubic capacity must be a positive number")
    @Max(value = 999999, message = "Cubic capacity can't be more than 999999")
    private int cc;

    @Temporal(TemporalType.DATE)
//    @Pattern(regexp = "(0[1-9]|[12][0-9]|3[01])[- \\/.](0[1-9]|1[012])[-\\/.](19|20)\\d\\d", message = "Please enter a valid registration date.")
    private Date firstRegDate;

    @Min(value = 18,message = "Driver must be older than 18.")
    @Max(value = 99,message = "Please enter a valid age.")
    private int driversAge;

    private boolean hasAccidents;
    @Temporal(TemporalType.DATE)
//    @Pattern(regexp = "(0[1-9]|[12][0-9]|3[01])[- \\/.](0[1-9]|1[012])[-\\/.](19|20)\\d\\d", message = "Please enter a valid activation date.")
    private Date activationDate;

    private String imageRegCertificate;

    @Email(message = "Email should be valid")
    private String email;

    @Pattern(regexp = "(\\+)?(359|0)8[789]\\d{1}(|-| )\\d{3}(|-| )\\d{3}", message = "Please enter a valid mobile number.")
    private String phoneNumber;

    private String addressText;
    private String addressCity;

    @NumberFormat(style = NumberFormat.Style.NUMBER)
    @Min(value = 0,message = "Please enter a positive number")
    private int addressNumber;

    @Min(value = 1000, message = "Please enter a valid post code for Bulgaria")
    @Max(value = 9999, message = "Please enter a valid post code for Bulgaria")
    private int addressPostCode;

    public PolicyFullDTO() {
    }

    String getCarBrand() {
        return carBrand;
    }

    private void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    String getCarModel() {
        return carModel;
    }

    private void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    int getCc() {
        return cc;
    }

    private void setCc(int cc) {
        this.cc = cc;
    }

    public Date getFirstRegDate() {
        return firstRegDate;
    }

    public void setFirstRegDate(Date firstRegDate) {
        this.firstRegDate = firstRegDate;
    }

    public int getDriversAge() {
        return driversAge;
    }

    public void setDriversAge(int driversAge) {
        this.driversAge = driversAge;
    }

    public boolean isHasAccidents() {
        return hasAccidents;
    }

    public void setHasAccidents(boolean hasAccidents) {
        this.hasAccidents = hasAccidents;
    }

    public Date getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

    String getImageRegCertificate() {
        return imageRegCertificate;
    }

    private void setImageRegCertificate(String imageRegCertificate) {
        this.imageRegCertificate = imageRegCertificate;
    }

    String getEmail() {
        return email;
    }

    private void setEmail(String email) {
        this.email = email;
    }

    String getPhoneNumber() {
        return phoneNumber;
    }

    private void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    String getAddressText() {
        return addressText;
    }

    private void setAddressText(String addressText) {
        this.addressText = addressText;
    }

    int getAddressNumber() {
        return addressNumber;
    }

    private void setAddressNumber(int addressNumber) {
        this.addressNumber = addressNumber;
    }

    int getAddressPostCode() {
        return addressPostCode;
    }

    private void setAddressPostCode(int addressPostCode) {
        this.addressPostCode = addressPostCode;
    }

    String getAddressCity() {
        return addressCity;
    }

    private void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }
}
