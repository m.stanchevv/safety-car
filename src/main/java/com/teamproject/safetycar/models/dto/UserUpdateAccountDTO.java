package com.teamproject.safetycar.models.dto;


import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserUpdateAccountDTO {

    @Size(min = 3, max = 30, message = "First name must be between 3 and 30 characters.")
    private String firstName;
    @Size(min = 3, max = 30, message = "Last name must be between 3 and 30 characters.")
    private String lastName;
    @Email(message = "Email should be valid")
    private String email;
    private String oldPassword;
    @Pattern(regexp = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$",
            message = "Password must be at least 6 characters. Must include at least one upper case, one lower case letter and at least one number.")
    private String newPassword;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
