package com.teamproject.safetycar.models.dto;

public class MessageResponseDTO {
    private Integer initialMessageID;
    private String subject;
    private String text;
    private String email;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getInitialMessageID() {
        return initialMessageID;
    }

    public void setInitialMessageID(Integer initialMessageID) {
        this.initialMessageID = initialMessageID;
    }
}
