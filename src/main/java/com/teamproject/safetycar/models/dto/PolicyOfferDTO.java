package com.teamproject.safetycar.models.dto;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import java.util.Date;

public class PolicyOfferDTO {

    private String carModel;
    private String carBrand;
    private boolean hasAccidents;

    @Min(value = 18, message = "Driver must be older than 18.")
    @Max(value = 99, message = "Please enter a valid age.")
    private int driverAge;

    @Min(value = 0, message = "Cubic capacity must be a positive number")
    @Max(value = 999999, message = "Cubic capacity can't be more than 999999")
    private int cc;

    @Temporal(TemporalType.DATE)
//    @Pattern(regexp = "(0[1-9]|[12][0-9]|3[01])[- \\/.](0[1-9]|1[012])[-\\/.](19|20)\\d\\d", message = "Please enter a valid registration date.")
    private Date firstRegDate;


    public PolicyOfferDTO() {
    }

    public Date getFirstRegDate() {
        return firstRegDate;
    }

    public void setFirstRegDate(Date firstRegDate) {
        this.firstRegDate = firstRegDate;
    }

    boolean hasAccidents() {
        return hasAccidents;
    }

    private void setHasAccidents(boolean hasAccidents) {
        this.hasAccidents = hasAccidents;
    }

    int getDriverAge() {
        return driverAge;
    }

    private void setDriverAge(int driverAge) {
        this.driverAge = driverAge;
    }

    int getCc() {
        return cc;
    }

    private void setCc(int cc) {
        this.cc = cc;
    }

    String getCarModel() {
        return carModel;
    }

    private void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    String getCarBrand() {
        return carBrand;
    }

    private void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    boolean isHasAccidents() {
        return hasAccidents;
    }
}
