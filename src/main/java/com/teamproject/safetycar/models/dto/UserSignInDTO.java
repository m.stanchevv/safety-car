package com.teamproject.safetycar.models.dto;

import javax.validation.constraints.Email;

public class UserSignInDTO {

    @Email(message = "Email should be valid")
    private String email;
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
