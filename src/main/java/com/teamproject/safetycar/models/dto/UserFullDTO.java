package com.teamproject.safetycar.models.dto;


import com.teamproject.safetycar.models.Role;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

public class UserFullDTO {

    @Size(min = 2, max = 30, message = "First name must be between 2 and 30 characters.")
    private String firstName;
    @Size(min = 2, max = 30, message = "Last name must be between 2 and 30 characters.")
    private String lastName;
    @Email(message = "Email should be valid")
    private String email;
    @Pattern(regexp = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$",
            message = "Password must be at least 6 characters. Must include at least one upper case, one lower case letter and at least one number.")
    private String password;


    public String getEmail() {
        return email;
    }

    private void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    private void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    private void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    private void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
