package com.teamproject.safetycar.models;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    ROLE_USER, ROLE_AGENT, ROLE_ADMIN;

    public String getAuthority() {
        return name();
    }

}
