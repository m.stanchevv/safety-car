package com.teamproject.safetycar.models;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

@Entity
@Table(name = "car_details")
public class CarDetails {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "car_id")
    @NotNull
    private Car car;

    @Column(name = "cubic_capacity")
    @Min(value = 0, message = "Cubic capacity must be a positive number")
    @Max(value = 999999, message = "Cubic capacity can't be more than 999999")
    private int cubic_capacity;

    @Column(name = "accidents")
    private boolean accidents;

    @Column(name = "reg_certificate")
    @NotNull
    private String imageRegCertificate;

    @Column(name = "first_reg_date")
    @Temporal(TemporalType.DATE)
//    @Pattern(regexp = "(0[1-9]|[12][0-9]|3[01])[- \\/.](0[1-9]|1[012])[-\\/.](19|20)\\d\\d", message = "Please enter a valid registration date.")
    private Date regDate;

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public int getCubic_capacity() {
        return cubic_capacity;
    }

    public void setCubic_capacity(int cubic_capacity) {
        this.cubic_capacity = cubic_capacity;
    }

    public boolean isAccidents() {
        return accidents;
    }

    public void setAccidents(boolean accidents) {
        this.accidents = accidents;
    }

    public String getImageRegCertificate() {
        return imageRegCertificate;
    }

    public void setImageRegCertificate(String imageRegCertificate) {
        this.imageRegCertificate = imageRegCertificate;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
