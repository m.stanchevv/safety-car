package com.teamproject.safetycar.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "reference")
public class ReferenceTable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @JsonIgnore
    private int id;

    @Column(name = "cc_min")
    private Integer ccMin;
    @Column(name = "cc_max")
    private Integer ccMax;

    @Column(name = "age_min")
    private Integer ageMin;
    @Column(name = "age_max")
    private Integer ageMax;

    @Column(name = "base_amount")
    private Double baseAmount;

    @Column (name = "active")
    private boolean active;

    @Column (name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCcMin() {
        return ccMin;
    }

    public void setCcMin(Integer ccMin) {
        this.ccMin = ccMin;
    }

    public int getCcMax() {
        return ccMax;
    }

    public void setCcMax(Integer ccMax) {
        this.ccMax = ccMax;
    }

    public int getAgeMin() {
        return ageMin;
    }

    public void setAgeMin(Integer ageMin) {
        this.ageMin = ageMin;
    }

    public int getAgeMax() {
        return ageMax;
    }

    public void setAgeMax(Integer ageMax) {
        this.ageMax = ageMax;
    }

    public Double getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(Double baseAmount) {
        this.baseAmount = baseAmount;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
