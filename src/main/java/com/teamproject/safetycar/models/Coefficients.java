package com.teamproject.safetycar.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "coefficients")
public class Coefficients {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "age_coeff")
    private Double ageCoeff;

    @Column(name = "accident_coeff")
    private Double accidentCoeff;

    @Column(name = "tax_coeff")
    private Double taxCoeff;

    @Column(name = "user_age_min")
    private int userMinAge;
    @Column(name = "user_age_max")
    private int userMaxAge;

    @Column (name = "active")
    private boolean active;

    @Column (name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getTaxCoeff() {
        return taxCoeff;
    }

    public void setTaxCoeff(Double taxCoeff) {
        this.taxCoeff = taxCoeff;
    }

    public int getUserMinAge() {

        return userMinAge;
    }

    public void setUserMinAge(int userMinAge) {
        this.userMinAge = userMinAge;
    }

    public int getUserMaxAge() {
        return userMaxAge;
    }

    public void setUserMaxAge(int userMaxAge) {
        this.userMaxAge = userMaxAge;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getAgeCoeff() {
        return ageCoeff;
    }

    public void setAgeCoeff(Double ageCoeff) {
        this.ageCoeff = ageCoeff;
    }

    public Double getAccidentCoeff() {
        return accidentCoeff;
    }

    public void setAccidentCoeff(Double accidentCoeff) {
        this.accidentCoeff = accidentCoeff;
    }
}
