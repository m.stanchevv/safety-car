package com.teamproject.safetycar;

import com.teamproject.safetycar.storage.StorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({
        StorageProperties.class
})
public class SafetycarApplication {
    public static void main(String[] args) {
        SpringApplication.run(SafetycarApplication.class, args);



    }


}
