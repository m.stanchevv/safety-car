package com.teamproject.safetycar.utilities;

import com.teamproject.safetycar.exception.CustomException;
import com.teamproject.safetycar.models.User;
import com.teamproject.safetycar.repository.UserRepository;
import com.teamproject.safetycar.security.JwtTokenProvider;
import com.teamproject.safetycar.service.contracts.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Service
public class EmailServiceImp implements EmailService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    private static final String SMTP_HOST_NAME = "smtp.gmail.com";
    private static final String SMTP_PORT = "465";
    private static final String emailMsgTxtAPPROVE = "Congrads! Your policy has been approved";
    private static final String emailMsgTxtREJECT = "Your policy has been Rejected :(";
    private static final String emailFromAddress = "safetycar.dev@gmail.com";
    private static final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";


    @Override
    public void sendSSLMessage(String[] recipients, String status, String subject, String message) throws MessagingException {
        boolean debug = true;

        Properties props = new Properties();
        props.put("mail.smtp.host", SMTP_HOST_NAME);

        props.put("mail.smtp.auth", "true");
        props.put("mail.debug", "true");
        props.put("mail.smtp.port", SMTP_PORT);
        props.put("mail.smtp.socketFactory.port", SMTP_PORT);
        props.put("mail.smtp.socketFactory.class", SSL_FACTORY);
        props.put("mail.smtp.socketFactory.fallback", "false");
        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {

                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(emailFromAddress, "safetycar3776");
                    }
                });

        session.setDebug(debug);

        Message msg = new MimeMessage(session);
        InternetAddress addressFrom = new InternetAddress(emailFromAddress);
        msg.setFrom(addressFrom);

        InternetAddress[] addressTo = new InternetAddress[recipients.length];
        for (int i = 0; i < recipients.length; i++) {
            addressTo[i] = new InternetAddress(recipients[i]);
        }
        msg.setRecipients(Message.RecipientType.TO, addressTo);


        User user = userRepository.findByEmail(recipients[0]);
        switch (status) {
            case "approve":
                msg.setSubject("Policy Request");
                msg.setContent(emailMsgTxtAPPROVE, "text/plain");
                break;
            case "reject":
                msg.setSubject("Policy Request");
                msg.setContent(emailMsgTxtREJECT, "text/plain");
                break;
            default:
                msg.setSubject(subject);
                msg.setContent(message, "text/plain");
                break;
        }
        Transport.send(msg);
    }


    @Override
    public String confirmEmail(String token) {
        if (jwtTokenProvider.validateToken(token)) {
            User user = userRepository.findByEmail(jwtTokenProvider.getUsername(token));
            user.setActive(true);
            userRepository.save(user);
            return "Email confirmed!!";
        } else {
            throw new CustomException("Email validation token expired", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
}
