package com.teamproject.safetycar.utilities;

import com.teamproject.safetycar.models.Coefficients;
import com.teamproject.safetycar.models.ReferenceTable;
import com.teamproject.safetycar.repository.CoeffRepository;
import com.teamproject.safetycar.repository.ReferenceRepository;
import com.teamproject.safetycar.service.contracts.FileService;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class FileServiceImp implements FileService {

    @Autowired
    private ReferenceRepository referenceRepository;

    @Autowired
    private CoeffRepository coeffRepository;

    public void readFile() throws Exception {



        Workbook workbook = WorkbookFactory.create(new File("C:\\Users\\user\\Desktop\\New folder (2)\\safety-car\\src\\main\\resources\\static\\storage\\filesUploaded\\reftable.xlsx"));
//        Workbook workbook = WorkbookFactory.create(new File("/Users/venetagergova/fresh-projects/safety-car/src/main/resources/static/storage/filesUploaded/reftable.xlsx"));

        // Getting the Sheet at index zero
        Sheet refSheet = workbook.getSheetAt(0);
        // Create a DataFormatter to format and get each cell's value as String
        DataFormatter dataFormatter = new DataFormatter();

        String[][] validated= validateExcell(refSheet, dataFormatter);

        List<ReferenceTable> active = referenceRepository.findAllByActiveTrue();
        active.forEach(r -> r.setActive(false));
        active.forEach(r -> referenceRepository.save(r));

        for (int i = 0; i < 14; i++) {
            ReferenceTable referenceTable = new ReferenceTable();
                referenceTable.setCcMin(Integer.parseInt(validated[i][0]));
                referenceTable.setCcMax(Integer.parseInt(validated[i][1]));
                referenceTable.setAgeMin(Integer.parseInt(validated[i][2]));
                referenceTable.setAgeMax(Integer.parseInt(validated[i][3]));
                referenceTable.setBaseAmount(Double.parseDouble(validated[i][4]));
                referenceTable.setActive(true);
                referenceTable.setDate(new Date());
                referenceRepository.save(referenceTable);
        }

        Sheet coefSheet = workbook.getSheetAt(1);

        Coefficients old = coeffRepository.findFirstByActiveTrue();

        for (Row row : coefSheet) {
            Coefficients newCoeffs = new Coefficients();
            List<String> list = new ArrayList<>();
            for (Cell cell : row) {

                String cellValue = dataFormatter.formatCellValue(cell);
                list.add(cellValue);
            }
            if (row.getRowNum() != 0) {
                newCoeffs.setAgeCoeff(Double.parseDouble(list.get(0)));
                newCoeffs.setTaxCoeff(Double.parseDouble(list.get(1)));
                newCoeffs.setAccidentCoeff(Double.parseDouble(list.get(2)));
                newCoeffs.setUserMinAge(Integer.parseInt(list.get(3)));
                newCoeffs.setUserMaxAge(Integer.parseInt(list.get(4)));
                newCoeffs.setActive(true);
                newCoeffs.setDate(new Date());

                if (old != null) {
                    if (old.getUserMinAge() != newCoeffs.getUserMinAge() ||
                            old.getUserMaxAge() != newCoeffs.getUserMaxAge() ||
                            !old.getTaxCoeff().equals(newCoeffs.getTaxCoeff()) ||
                            !old.getAccidentCoeff().equals(newCoeffs.getAccidentCoeff()) ||
                            !old.getAgeCoeff().equals(newCoeffs.getAgeCoeff())) {

                        old.setActive(false);
                        coeffRepository.save(old);
                        coeffRepository.save(newCoeffs);
                    }
                } else {
                    coeffRepository.save(newCoeffs);
                }
            }
        }
        workbook.close();

        getCurrentCSV();
    }

    private String[][] validateExcell(Sheet refSheet, DataFormatter dataFormatter) {
        String[][] matrix = new String[14][5];
        for (int i = 0; i < 14; i++) {
            Row row = refSheet.getRow(i + 1);
            for (int j = 0; j < 5; j++) {
                Cell cell = row.getCell(j);
                String cellValue = dataFormatter.formatCellValue(cell);
                matrix[i][j] = cellValue;
                if (matrix[i][j] == null || matrix[i][j].equals("")) {
                    throw new IllegalArgumentException(String.format("Missing value at row %d column %d", i, j));

                }
                try {
                    double parse = Double.parseDouble(matrix[i][j]);
                } catch (NumberFormatException e) {
                    throw new IllegalArgumentException(String.format("Value at row %d column %d is not a number!" +
                            "All cell table values must be numbers!", i, j));

                }
            }
                if (i > 0) {
                    if (i % 2 == 0) {
                        if (!matrix[i][0].equals(matrix[i - 1][1])) {
                            throw new IllegalArgumentException(String.format("Value at row %d column %d does" +
                                    " not match value in row %d column %d", i, 0, i - 1, 1));

                        }
                        if (Integer.parseInt(matrix[i - 1][2]) - (Integer.parseInt(matrix[i][3])) != 1) {
                            throw new IllegalArgumentException(String.format("Value at row %d column %d does" +
                                    " not match value in row %d column %d", i - 1, 2, i, 3));

                        }
                    }
                    if (i % 2 == 1) {
                        if (!matrix[i][0].equals(matrix[i - 1][0])) {
                            throw new IllegalArgumentException(String.format("Value at row %d column %d does" +
                                    " not match value in row %d column %d", i, 0, i - 1, 0));

                        }
                        if (!matrix[i][1].equals(matrix[i - 1][1])) {
                            throw new IllegalArgumentException(String.format("Value at row %d column %d does" +
                                    " not match value in row %d column %d", i, 1, i - 1, 1));

                        }
                    }
                }
            }
        return matrix;
    }

    @Override
    public File getCurrentCSV() {
        File csvOutputFile = new File("C:\\Users\\user\\Desktop\\New folder (2)\\safety-car\\src\\main\\resources\\static\\storage\\csvTables\\csvActiveTable");
//        File csvOutputFile = new File("/Users/venetagergova/fresh-projects/safety-car/src/main/resources/static/storage/csvTables/csvActiveTable");
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(csvOutputFile, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fileWriter.write("ccMin,ccMax,ageMin,ageMax,baseAmount\n");
        } catch (IOException e) {
            e.printStackTrace();
        }


        for (ReferenceTable mapping : referenceRepository.findAllByActiveTrue()) {
                try {
                    fileWriter.write(mapping.getCcMin() + "," + mapping.getCcMax() + "," + mapping.getAgeMin() +
                            "," + mapping.getAgeMax() + "," + mapping.getBaseAmount() + "\n");
                } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return csvOutputFile;
    }

}

