package com.teamproject.safetycar.storage;


import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("storage")
public class StorageProperties {

    /**
     * Folder location for storing files
     */
//    private String filesLocation = "C:\\Users\\user\\Desktop\\New folder (2)\\safety-car\\src\\main\\resources\\static\\storage\\filesUploaded";
    private String filesLocation = "/Users/venetagergova/fresh-projects/safety-car/src/main/resources/static/storage/filesUploaded";
//
    private String picturesLocation = "/Users/venetagergova/fresh-projects/safety-car/src/main/resources/static/picturesUploaded";
//    private String picturesLocation = "C:\\Users\\user\\Desktop\\New folder (2)\\safety-car\\src\\main\\resources\\static\\storage\\picturesUploaded";

    public String getFilesLocation() {
        return filesLocation;
    }

    public void setFilesLocation(String filesLocation) {
        this.filesLocation = filesLocation;
    }

    public String getPicturesLocation() {
        return picturesLocation;
    }

    public void setPicturesLocation(String picturesLocation) {
        this.picturesLocation = picturesLocation;
    }
}
