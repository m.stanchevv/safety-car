package com.teamproject.safetycar.storage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class PicturesSystemStorageService implements PicturesStorageService {


    private final Path pictureStorageLocation;


    @Autowired
    public PicturesSystemStorageService(StorageProperties storageProperties) {
        this.pictureStorageLocation = Paths.get(storageProperties.getPicturesLocation())
                .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.pictureStorageLocation);
        } catch (Exception ex) {
            throw new StorageException("Could not create the directory where the uploaded pictures will be stored.", ex);
        }
    }

    public String storePicture(MultipartFile picture)  {
        // Normalize picture name
        String pictureName = Math.random() + "__" + StringUtils.cleanPath(picture.getOriginalFilename());

        try {
            // Check if the picture's name contains invalid characters
            if (pictureName.contains("..")) {
                throw new StorageException("Sorry! Picture name contains invalid path sequence " + pictureName);
            }

            // Copy picture to the target location (Replacing existing picture with the same name)
            Path targetLocation = this.pictureStorageLocation.resolve(pictureName);
            Files.copy(picture.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return pictureName;
        } catch (IOException ex) {
            throw new StorageException("Could not store picture " + pictureName + ". Please try again!", ex);
        }
    }

    public Resource loadPictureAsResource(String pictureName) {
        try {
            Path picturePath = this.pictureStorageLocation.resolve(pictureName).normalize();
            Resource resource = new UrlResource(picturePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new StorageFileNotFoundException("Picture not found " + pictureName);
            }
        } catch (MalformedURLException ex) {
            throw new StorageFileNotFoundException("Picture not found " + pictureName, ex);
        }
    }
}
