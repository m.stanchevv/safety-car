package com.teamproject.safetycar.storage;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


public interface PicturesStorageService {


    String storePicture(MultipartFile picture);

    Resource loadPictureAsResource(String pictureName);

}
