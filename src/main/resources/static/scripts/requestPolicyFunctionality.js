let brand;
let model;
let cc;
let regDate;
let driverAge;
let activationDate;
let hasAccidents;

let email;
let phoneNumber;
let addressText;
let addressNo;
let addressCity;
let addressPostCode;

let imageUploadedName;

let imageVisualize;

let totalPrice;


function requestPolicyStep1() {

    clearALLContainers();
    emptyNav();
    $('#filters').collapse('hide');
    $('#userAccountContainer').append(`    
  <h1 >Request Policy</h1>
<br>
<br>
<div id="formOffer">
        
<form id="policyFormStep1">
     <div class="form-row">
        <div class="col-md-5">
                           
            <label  for="inputCarBrandBrand">Car Brand</label>
             <select onchange="addCarModelToForm()" id="inputCarBrandBrand1" class="form-control is-invalid" required>
                                <option id="optionSelectedCarBrand" value="">Choose...</option>

                            </select>
        </div>
        <div class="form-group col-md-5">
            <label for="inputCarModel">Car Model</label>
              <select id="inputCarModel1" class="form-control is-invalid" required>
                                <option id="optionCarModel" value="">Choose...</option>

                            </select>
        </div>
    </div>
   <div class="form-row">
                <div class="col-md-5">
            <label for="inputCubicCapacity4">Cubic Capacity</label>
            <textarea oninput="validateCC(this)"  type="text" class="form-control is-invalid" required id="inputCubicCapacity4" rows="1"></textarea>
        </div>
        <div class="form-group col-md-5">
            <label for="inputRegDate">First Registration Date</label>
                <input type='text' class="form-control" id='datetimepicker' required/>
<!--            <textarea oninput="validateDate(this)" type="text" class="form-control is-invalid" required id="inputRegDate" rows="1" placeholder="DD/MM/YYYY"></textarea>-->
        </div>
      
    </div>
      <div class="form-row">
        <div class="form-group col-md-5">
            <label for="DriverAge">Driver's age</label>
            <textarea oninput="validateAge(this)" type="text" class="form-control" id="DriverAge" rows="1"></textarea>
        </div>
        <div class="form-group col-md-5">
                   <div class="form-check" style="margin-top: 30px">
           <input class="form-check-input" type="checkbox" id="gridCheck">
            <label class="form-check-label" for="gridCheck">
                Accidents in previous year
            </label>
            </div>
        </div>
    </div>
   <div class="form-row">
        <div class="col-md-5">
            <label for="inputDate">To be active from:</label>
            <input type='text' class="form-control" id='datetimepicker1' required/>
<!--            <textarea oninput="validateDate(this)" type="text" class="form-control" id="inputDate" required placeholder="DD/MM/YYYY" rows="1"></textarea>-->
        </div> 
                <div class="col-md-5">
            <span style="margin-right: 10px">(1/3)</span> 
            <button  type="submit" class="btn btn-primary">Next</button>        
</div>
</div>
        </form>
        </div>
        
`);
    $('#datetimepicker').datetimepicker({
        viewMode: 'years',
        format: 'YYYY-MM-DD'
    });

    $('#datetimepicker1').datetimepicker({
        format: 'YYYY-MM-DD'
    });


    $('#inputCubicCapacity4').append(localStorage.getItem("cc"));
    // $('#inputRegDate').attr(localStorage.getItem("regDate"));
    $('#DriverAge').append(localStorage.getItem("age"));

    addCarBrandToForm();
    addCarModelToForm();

    $('#policyFormStep1').submit(function (event) {

        brand = $("#inputCarBrandBrand1 option:selected").text();
        model = $('#inputCarModel1 option:selected').text();
        driverAge = $('#DriverAge').val();
        cc = $('#inputCubicCapacity4').val();
        regDate = $('#datetimepicker').val();
        activationDate = $('#datetimepicker1').val();

        if ($('#gridCheck').is(":checked")) {
            hasAccidents = true;
        } else {
            hasAccidents = false;
        }
        requestPolicyStep2();
        getTotalPrice();
    });
}


function getTotalPrice() {
    $.ajax
    ({
        type: "PUT",
        url: `http://localhost:8080/api/policies/totalprice`,
        "headers": {
            "Authorization": "Bearer " + localStorage.getItem('token'),
        },
        // dataType: 'json',
        contentType: 'application/json',
        //json object to sent to the authentication url
        data: JSON.stringify({
            "carBrand": brand,
            "carModel": model,
            "cc": cc,
            "firstRegDate": regDate,
            "driverAge": driverAge,
            "hasAccidents": hasAccidents
        }),
        success: function (data) {
            let price = data;
            totalPrice = price.toFixed(2);
        }

    })

}


function requestPolicyStep2() {
    clearALLContainers();
    emptyNav();
    $('#filters').collapse('hide');
    $('#idEmail').append(localStorage.getItem("email"));


    $('#userAccountContainer').append(`<form id="policyFormStep2">
            <h4>Communication details:</h4>
<br>
    <div class="form-row">
        <div class="form-group col-md-6">
            
            <label for="idEmail">Email:</label><br>
            <input oninput="validateEmail(this)" type="text" class="form-control is-invalid" id="idEmail" required>
        </div>
        <div class="form-group col-md-6">
            <label for="inputPhone">Phone number:</label>
            <input oninput="validatePhone(this)" type="text" class="form-control is-invalid" id="inputPhone" placeholder="Phone number"required>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">

            <label for="inputAddress">Address:</label><br>
            <input type="text" class="form-control is-invalid" id="inputAddress" placeholder="Address:"required>

        </div>

        <div class="form-group col-md-6">

            <label for="inputNo">No:</label><br>
            <input oninput="validateNum(this)" type="text" class="form-control is-invalid" id="inputNo" placeholder="No"required>
        </div>

    </div>
  

    <div class="form-row">
        <div class="form-group col-md-6">

            <label for="inputCity">City:</label><br>
            <input type="text" class="form-control is-invalid" id="inputCity" placeholder="City"required>
        </div>
        <div class="form-group col-md-6">
            <label for="inputPostCode">Post Code:</label><br>
            <input oninput="validatePostCode(this)" type="text" class="form-control is-invalid" id="inputPostCode" placeholder="Post Code"required>
        </div>

    </div>
    <div class="form-row">
    
               <h8 style="margin-right: 10px">(2/3)</h8> 
               <button  style="margin-right: 10px" type="button" onclick="requestPolicyStep1()" class="btn btn-primary">Back</button>        
               <button type="submit" class="btn btn-primary">Next</button>
               </div>

</form>`);

    $('#policyFormStep2').submit(function (event) {

        email = $('#idEmail').val();
        phoneNumber = $('#inputPhone').val();
        addressText = $('#inputAddress').val();
        addressNo = $('#inputNo').val();
        addressCity = $('#inputCity').val();
        addressPostCode = $('#inputPostCode').val();
        requestPolicyStep3();

    });
}


function requestPolicyStep3() {
    clearALLContainers();
    emptyNav();
    $('#filters').collapse('hide');
    $('#userAccountContainer').append(`
<h3>Upload a picture of your registration plate</h3>

<div style="display: flex">
<div class="upload-container">
    <div class="upload-content">
        <div class="single-upload">
            <form action="/" method="post" enctype="multipart/form-data" id="singleUploadForm" name="singleUploadForm">
                <label for="singleFileUploadInput">Picture size must be up to 2MB</label>
                <input id="singleFileUploadInput" type="file" name="file" class="file-input" onchange="readURL(this)"
                       required/>
                    <br><br><br><br><br>
                <div id="buttonDivContainer">
                <h8 style="margin-right: 10px">(3/3)</h8>
                <button style="margin-right: 10px" type="button" onclick="requestPolicyStep2()" class="btn btn-primary">Back</button>        
                <button type="submit" id="submitPhotoBtn" class="btn btn-primary">Submit</button>
                </div>

            </form>
            <div class="upload-response">
                <div id="singleFileUploadError"></div>
                <div id="singleFileUploadSuccess"></div>
            </div>
        </div>

    </div>
</div>


 
<div class="form-group col-md-6">

    <img id="blah" src="../img/placeholder-small.png" alt="your image"/>

</div>
</div>`);


    $('#singleUploadForm').submit(function (event) {
        let formData = new FormData($("#singleUploadForm")[0]);
        $('#singleFileUploadError').html(' ');
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "http://localhost:8080/api/policies/uploadPicture",
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                imageUploadedName = response.fileName;
                console.log(imageUploadedName);
                $('#singleFileUploadSuccess').append(`<h6 style="color: green">Image Uploaded!</h6>`);
                $('#submitPhotoBtn').hide();
                $('#buttonDivContainer').append(`
                    <button type="button" onclick="prviewRequest()" class="btn btn-success">Preview Request</button>`)
                // process response
            },
            error: function (xhr, status, error) {
                let err = JSON.parse(xhr.responseText);
                $('#singleFileUploadError').html(' ');
                $('#singleFileUploadError').append(`<h4 style="color: red">${err.message}</h4>`);
            }
        });
        event.preventDefault();

    });

}

function prviewRequest() {
    visualizePreview();
    clearALLContainers();
    emptyNav();
    $('#filters').collapse('hide');
    $('#userAccountContainer').append(`<div class="line"></div>
    <div  class="card">
        <div class="container" >
        <div class="card-body">
            <h1>Policy Details:<a> </a> </h1> 
                <div class="row">
                    <div class="col-sm-4">
                        <h5 style="color: rgb(96,192,220);">Brand: </h5><h5>${brand}</h5>
                        <h5 style="color: rgb(96,192,220);">Model: </h5><h5>${model} </h5>
                        <h5 style="color: rgb(96,192,220);">Cubic capacity: </h5><h5> ${cc}</h5>
                    </div>
                    <div class="col-sm-4">
                         <h5 style="color: rgb(96,192,220);">First registration date: </h5><h5>${regDate} </h5>
                        <h5 style="color: #60c0dc;"> Driver age: </h5><h5>${driverAge} </h5>
                        <h5 style="color: #60c0dc;">Accidents in previous year: </h5><h5>${hasAccidents} </h5>
                        <h5 style="color: #60c0dc;">Policy activation date: </h5> <h5>${activationDate}</h5>
                    </div>
                    <div class="col-sm-3">
                        <img id="imagePreview" style="max-height: 120px; margin-left: 15px"  src="" class="img-thumbnail" alt="img"/>
                    </div>
                </div>
       
        <h3>Contact Information: </h3> 
            <div class="row">
                <div class="col-sm-4">
                    <h5 style="color: rgb(96,192,220);">Email: </h5><h5>  ${email} </h5>
                  <h5 style="color: rgb(96,192,220);">Phone: </h5><h5>  ${phoneNumber}<a> </a> </h5>
                    <h5 style="color: rgb(96,192,220);">Address: </h5><h5> ${addressText}</h5>
                    <h5 style="color: rgb(96,192,220);">No: </h5><h5> ${addressNo} </h5>
                </div>
                  <div class="col-sm-4">
                    <h5 style="color: rgb(96,192,220);">City: </h5><h5> ${addressCity}</h5>
                   <h5 style="color: rgb(96,192,220);">Post Code: </h5><h5> ${addressPostCode} </h5>
                </div>
              
                <div  id="price" class="col-sm-4">                    
                    <h3>Total Price: </h3><h3> ${totalPrice}</h3>

                </div>
                </div>
                <br>
               
                <div style="display: flex;">
                <div  id="BackBtn" >
                    <button onclick="requestPolicyStep3()" type="button" class="btn btn-primary">Back</button>
                </div>
                 <div  id="ConfirmRequestBtn" >
                    <button style="margin-left: 15px" onclick="submitRequest()" type="button" class="btn btn-primary">Submit Request</button>
                </div>
               <div style="margin-left: 15px" id="errorMessageRequest"></div>
               </div>

            </div>
        </div>
        </div>
    </div>
</div>`);

    // $('#imagePreview').attr('src', `data:image/png;base64,${imageUploadedByte}`);

    let modal = document.getElementById("myModalImg");

    let img = document.getElementById("imagePreview");
    let modalImg = document.getElementById("img01");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
    };

    let span = document.getElementsByClassName("img-close")[0];

    span.onclick = function () {
        modal.style.display = "none";
    };
}

function submitRequest() {
    $.ajax
    ({
        type: "POST",
        url: `http://localhost:8080/api/policies/new?email=${localStorage.getItem("email")}`,
        "headers": {
            "Authorization": "Bearer " + localStorage.getItem('token'),
        },
        dataType: 'json',
        contentType: 'application/json',
        //json object to sent to the authentication url
        data: JSON.stringify({
            "carBrand": brand,
            "carModel": model,
            "cc": cc,
            "firstRegDate": regDate,
            "driversAge": driverAge,
            "hasAccidents": hasAccidents,
            "activationDate": activationDate,
            "imageRegCertificate": imageUploadedName,
            "email": email,
            "phoneNumber": phoneNumber,
            "addressText": addressText,
            "addressNumber": addressNo,
            "addressPostCode": addressPostCode,
            "addressCity": addressCity,
        }),
        success: function () {
            clearALLContainers();
            $('#userAccountContainer').append(`<h1>Your Request is Sent!</h1>
                <h3>You will recieve an email confirmation when your request has been approved.</h3>
                <button onclick="getPendingPolicies()" type="button" class="btn btn-primary">View Pending Requests</button>
                `);
        },
        error: function (xhr, status, error) {
            $('#errorMessageRequest').html(' ');

            let xhrResponse = JSON.parse(xhr.responseText);
            let arrayErrors = xhrResponse.errors;

            if (typeof arrayErrors === "undefined") {
                $('#errorMessageRequest').append(`<h5 style="color: red">${xhrResponse.message}</h5>`);

            } else {
                $.each(arrayErrors, function (i) {
                    $('#errorMessageRequest').append(`<h5 style="color: red">${arrayErrors[i].defaultMessage}</h5>`);
                })
            }
        }

    })

}

function readURL(input) {
    imageVisualize = input;
    if (input.files && input.files[0]) {
        let reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result)
                .width(300)
                .height(200);
        };


        reader.readAsDataURL(input.files[0]);
    }
}


function visualizePreview() {
    if (imageVisualize.files && imageVisualize.files[0]) {
        let reader = new FileReader();
        reader.onload = function (e) {
            $('#imagePreview')
                .attr('src', e.target.result)
                .width(300)
                .height(200);
        };
        reader.readAsDataURL(imageVisualize.files[0]);
    }
}


function addCarBrandToForm() {
    $('#inputCarBrandBrand').html('');
    $('#inputCarBrandBrand').append(`<option selected>Choose...</option>`);
    $('#inputCarBrandBrand1').html('');
    $('#inputCarBrandBrand1').append(`<option selected>Choose...</option>`);
    if (localStorage.getItem('carBrand') !== null) {
        $('#inputCarBrandBrand').append(`<option selected>Choose...</option>`);
        $('#inputCarBrandBrand1').append(`<option selected>Choose...</option>`);
    }
    $.ajax({
        url: `http://localhost:8080/api/cars/brands`,
        success: function (response) {
            $.each(response, function (i) {
                $('#inputCarBrandBrand').append(`<option>${response[i]}</option>`);
                $('#inputCarBrandBrand1').append(`<option>${response[i]}</option>`);
            })
        }
    });
}

function addCarModelToForm() {
    let brand = $("#inputCarBrandBrand option:selected").text();
    let brand1 = $("#inputCarBrandBrand1 option:selected").text();
    $('#inputCarModel').html('');
    $('#inputCarModel').append(`<option selected>Choose...</option>`);
    $('#inputCarModel1').html('');
    $('#inputCarModel1').append(`<option selected>Choose...</option>`);
    $.ajax({
        url: `http://localhost:8080/api/cars/${brand}`,
        success: function (response) {
            $.each(response, function (i) {
                $('#inputCarModel').append(`<option>${response[i]}</option>`);

            })
        }
    });
    $.ajax({
        url: `http://localhost:8080/api/cars/${brand1}`,
        success: function (response) {
            $.each(response, function (i) {
                $('#inputCarModel1').append(`<option>${response[i]}</option>`);

            })
        }
    });
}
