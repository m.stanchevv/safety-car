function validateCC(input) {
    let ccInput = input.value;
    if (/\D/.test(ccInput)) {
        input.setCustomValidity('Please enter a valid number!')
    } else if (ccInput <= 0 || ccInput > 999999) {
        input.setCustomValidity('Cubic capacity must be between 0 and 999999!')
    } else {
        input.setCustomValidity('')
    }
}

function validateAge(input) {
    let age = input.value;
    if (/\D/.test(age)) {
        input.setCustomValidity('Please enter a valid age!')
    } else if (age <= 18 || age > 99) {
        input.setCustomValidity('Driver must be over 18!')
    } else {
        input.setCustomValidity('')
    }

}

function validateDate(input) {
    let frdInput = input.value;
    let reg = /(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/;
    if (String(frdInput).match(reg)) {
        input.setCustomValidity('');
    } else {
        input.setCustomValidity("Please enter a valid date in the format: DD/MM/YYYY");
    }

}

function validateEmail(input) {
    let email = input.value;
    let reg = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!reg.test(email)) {
        input.setCustomValidity("Please enter a valid email address.");
    } else {
        input.setCustomValidity('');
    }
}

function validatePassword(input) {
    let pass = input.value;
    let reg = /(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\S+$).{6,}$/;
    if (!reg.test(pass)) {
        input.setCustomValidity("Password must be at least 6 characters.\n" +
            "Must include at least one upper case letter, one lower case letter and at least one number.");
    } else {
        input.setCustomValidity('');
    }
}

function validatePhone(input) {
    let phone = input.value;
    let reg = /(\+)?(359|0)8[789]\d{1}(|-| )\d{3}(|-| )\d{3}/;
    if (!reg.test(phone)) {
        input.setCustomValidity("Please enter a valid Bulgarian phone number.");
    } else {
        input.setCustomValidity('');
    }
}

function validatePostCode(input) {
    let ccInput = input.value;
    if (/\D/.test(ccInput)) {
        input.setCustomValidity('Please enter a valid number!')
    } else if (ccInput < 1000 || ccInput > 9999) {
        input.setCustomValidity('Please enter a valid Bulgarian post code.')
    } else {
        input.setCustomValidity('')
    }

}

function validateNum(input) {
    let noinput = input.value;
    if (/\D/.test(noinput)) {
        input.setCustomValidity('Please enter a valid number!')
    } else {
        input.setCustomValidity('')
    }
}

function validateName(input) {
    let nameInput = input.value;
    if (nameInput.length < 3) {
        input.setCustomValidity('Name must be more than 3 characters long')
    } else {
        input.setCustomValidity('');
    }
}

function validateText(input) {
    let message = input.value;
    if (message.length < 20 || message.length > 500) {
        input.setCustomValidity('Message should b between 20 and 500 characters long')
    } else {
        input.setCustomValidity('');
    }
}