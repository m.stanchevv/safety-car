function showUploadReferenceTable() {
    clearALLContainers();

    $('#refContainer').html('');
    $('#cardContainerAgent').html('');
    $('#adminAccountContainer').html('');

    $('#cardContainerAgent').append(`
<h3>Upload new Reference table</h3>

<div style="display: flex">
<div class="upload-container">
    <div class="upload-content">
        <div class="single-upload">
            <form action="/" method="post" enctype="multipart/form-data" id="singleUploadForm" name="singleUploadForm">
                <label for="singleFileUploadInput">Only Excel format accepted.<br/>File size must be up to 2MB</label>
                <input id="singleFileUploadInput" type="file" name="file" class="file-input" required/>
                    <br><br><br><br><br>
                <div id="buttonDivContainer">      
                <button type="submit" id="submitPhotoBtn" class="btn btn-primary">Submit</button>
                </div>

            </form>
            <div class="upload-response">
                <div id="singleFileUploadError"></div>
                <div id="singleFileUploadSuccess"></div>
            </div>
        </div>

    </div>
</div>

</div>`);


    $('#singleUploadForm').submit(function (event) {
        let formData = new FormData($("#singleUploadForm")[0]);

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "http://localhost:8080/api/table/uploadFile",
            "headers": {
                "Authorization": "Bearer " + localStorage.getItem('token'),
            },
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                $('#cardContainerAgent').html('');
                $('#cardContainerAgent').append(`<h3>File successfully uploaded!</h3>
    <button onclick="getReferenceTable()" class="btn btn-primary">See Reference Table</button>
`);

            },
            error: function (xhr, status, error) {
                let err = JSON.parse(xhr.responseText);
                $('#singleFileUploadError').html('');
                $('#singleFileUploadError').append(`<h6 style="color: red">${err.message}</h6>`);
            }
        });

        event.preventDefault();
    });
}

// getPendingPolicies();
function addDates() {
    $.ajax({
        url: `http://localhost:8080/api/table/dates`,
        success: function (response) {
            $.each(response, function (i) {
                $('#dates').append(`<option>${response[i]}</option>`);
            })
        }
    });
}
function showActiveTable() {
    $('#refBody').html('');
    $.ajax({
        url: `http://localhost:8080/api/table/active`,
        success: function (response) {
            $.each(response, function (i) {
                $('#refBody').append(`<tr>
                    <th scope="row">${i+1}</th>
                   
                    <td>${response[i].ccMin}</td>                   
                    <td>${response[i].ccMax}</td>                   
                    <td>${response[i].ageMin}</td>                   
                    <td>${response[i].ageMax}</td>                   
                    <td>${response[i].baseAmount}</td>                   
                    </tr>`)
            })
        }
    });
}
function getReferenceTable() {
emptyNav();
    $('#filters').collapse('hide');
    clearALLContainers();

    $('#cardContainerAgent').append(`<h1>Reference Table</h1>
<br><br>`)
    emptyNav();
    $('#refContainer').html('');
    $('#adminAccountContainer').html('');
    $('#filters').collapse('hide');
    $('#cardContainerAgent').html('');
    $('#refContainer').append(`
  <form id="refForm">
  <div class="row">
  <div class="col-md-4">
                 <label  for="refDates">From When</label>
             <select onload="addDates()" id="dates" class="form-control is-invalid">
                                <option id="optionSelectedDate" value=""></option>
                            </select>
                            </div>
                            <br/>
                            <div class="col-lg-4">
          <button style="background-color: #3d8394"  type="submit" class="btn btn-info navbar-btn">
                    <i class="glyphicon glyphicon-search"></i><span> Search</span></button>
                      <button style="background-color: #3d8394" onclick="showActiveTable()" type="button" class="btn btn-info navbar-btn">
                    <span> Show Active</span></button>
                    </div>
                    </form>
                    </div>
                    <br/>
<table class="table table-bordered"><thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">ccMin</th>
      <th scope="col">ccMax</th>
      <th scope="col">ageMin</th>
      <th scope="col">ageMax</th>
      <th scope="col">BaseAmount</th>
    </tr>
  </thead><tbody id="refBody"></tbody></table>`);

    $('#refForm').submit(function (event) {
        event.preventDefault();
        let date = $("#dates option:selected").text();

        $('#refBody').html('');
        $.ajax({
            url: `http://localhost:8080/api/table?date=${date}`,
            success: function (response) {
                console.log(date);
                $.each(response, function (i) {
                    console.log(date);
                    $('#refBody').append(`<tr>
                    <th scope="row">${i+1}</th>
                    <td>${response[i].ccMin}</td>                   
                    <td>${response[i].ccMax}</td>                   
                    <td>${response[i].ageMin}</td>                   
                    <td>${response[i].ageMax}</td>                   
                    <td>${response[i].baseAmount}</td>                   
                    </tr>`)
                })
            }
        });
    });



    addDates();

    showActiveTable();



    $('#refContainer').append(`<br>
 <button onclick="showUploadReferenceTable()" type="button" class="btn btn-primary">Upload new table</button>`);

}

