let messageFromId;


$('form.contactForm').submit(function (event) {
    event.preventDefault();

    let name = $('#name').val();
    let email = $('#email').val();
    let subject = $('#subject').val();
    let text = $('#messageText').val();

    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = today.getFullYear();

    today = yyyy + '-' + mm + '-' + dd;

    // let reg = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    // if (!reg.test(email)) {
    //     $('#errormessageContactForm').append(`<h6 style="color: red">You must enter a valid email address.</h6>`);
    // }
    $.ajax
    ({
        method: "POST",
        url: `http://localhost:8080/api/messages/new`,
        dataType: 'json',
        contentType: 'application/json',
        //json object to sent to the authentication url
        data: JSON.stringify({
            "name": name,
            "email": email,
            "subject": subject,
            "text": text,
            "responded": false,
            "dateSent": today
        }),
        success: function (data) {
            $('#errormessageContactForm').html('');
            $('#sendmessageContactForm').html('');
            $('#sendmessageContactForm').append(`<h5 style="color: green">Successfully sent!</h5>`)
        },
        error: function (xhr, status, error) {
            let err = JSON.parse(xhr.responseText);
            console.log(err.status);

            // } else if (err.status === 500) {
            //     $('#errormessage').append(`<h6 style="color: red">First name and Last name must be between 3 and 30 characters.</h6>`);
            // }
        }
    });
});

function getMessageCard(response, i) {
    $('#adminAccountContainer').append(`
<div class="line"></div>
    <div  class="card">
        <div class="container" >
        <div class="card-body">
          <div style="display: flex"> <div><h3 >From: </h3></div><div> <h4 style="margin-top: 20px; margin-left: 10px">   ${response[i].email}, ${response[i].name}</h4> </div></div>
         
                <div class="row">
                    <div class="col-sm-8">
                 <div style="display: flex"> <div><h3 >Subject: </h3></div><div> <h4 style="margin-top: 20px; margin-left: 10px">   ${response[i].subject}</h4> </div></div>

                 <h3>Message: </h3>
<!--                        <h5 style="color: rgb(96,192,220);">Text: </h5>-->
                        <h5>${response[i].text} </h5>
                        <br>
                    </div>
                    <div class="col-sm-2">
                         <h5 style="color: rgb(96,192,220);">Date sent: </h5><h5>${response[i].dateSent} </h5>
                       </div>
                </div>
                </div>
           
               <br>
                <div style="display: flex;" >
                   <div id="Answered_${response[i].id}">
                   <h3>Answered</h3>               
                   </div>
                    </div> <div id="RespondBtn_${response[i].id}">
                    <button onclick="showResponseModal(${response[i].id})" type="button" class="btn btn-primary">Respond</button>
                </div>
               <div style="margin-left: 15px" id="errorMessageRequest"></div>
               </div>

            </div>
        </div>
        </div>
    </div>
</div>



`);
}

function getAllMessages() {
    clearALLContainers();


    $('#bs-example-navbar-collapse-1').html('');
    $('#bs-example-navbar-collapse-1').append(`<ul class="nav navbar-nav navbar-right">
                    <li><a style="color: gray" id="msg1" onclick="getAllMessages()">Show All</a></li>
                    <li><a style="color: gray" id="msg2" onclick="getUnanswered()">Show Unanswered</a></li>

                </ul>`);

    $('#adminAccountContainer').append(`<h1>Messages:</h1>`);
    $.ajax({
        url: `http://localhost:8080/api/messages`,
        "headers": {
            "Authorization": "Bearer " + localStorage.getItem('token'),
        },
        success: function (responece) {
            if (responece.length === 0) {
                $('#adminAccountContainer').append(`<h4>No messages yet.</h4>`);
            } else {
                $.each(responece, function (i) {
                    let respondBtn = "RespondBtn_" + responece[i].id;
                    let answered = "Answered_" + responece[i].id;
                    getMessageCard(responece, i);
                    if (responece[i].responded === true) {
                        document.getElementById(answered).style.display = "block";
                        document.getElementById(respondBtn).style.display = "none";
                    } else if (responece[i].responded === false) {
                        document.getElementById(respondBtn).style.display = "block";
                        document.getElementById(answered).style.display = "none";
                    }
                })
            }
        }
    });
}


function showResponseModal(id) {
    console.log("ID: " + id);

    getMessageFromId(id);

    console.log("Email: " + messageFromId.email);


    $('#modalContainer').html('');
    // $('#email').append(messageFromId.email);

    $('#modalContainer').append(`
<div id="modalRespondToMessage" class="modal fade modalRespond" tabindex="-1" role="dialog" aria-labelledby="modalRespond" aria-hidden="true">
 <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLongTitle">Respond</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
 <form class="respondToMessage">
  <div class="form-group">
                                <h2>To: ${messageFromId.name}</h2>
                            </div>
                            <div class="form-group">
                                <textarea oninput="validateEmail(this)" type="email" class="form-control br-radius-zero"
                                       name="email" id="email" required rows="1"> ${messageFromId.email}</textarea>
                            </div>

                            <div class="form-group">
                                <input oninput="validateName(this)" type="text" class="form-control br-radius-zero"
                                       name="subject" id="subject" required
                                       placeholder="Subject"/>
                            </div>
                            <div class="form-group">
                            <textarea oninput="validateText(this)" class="form-control br-radius-zero" name="message"
                                      rows="5" id="messageText" required
                                      data-msg="Please write something for us" placeholder="Message"></textarea>
                            </div>
                            <div id="sendmessageRespond"></div>
                            <div id="errormessageRespond"></div>
                            <div class="modal-footer">
                            <div class="form-action">
                                <button type="submit" class="btn btn-primary">Send Message</button>
                              <button type="button" class="btn btn-primary btn-sm" onclick="closeModalRespond()">Cancel</button>
                            </div>
                        </form>
                         </div>
                        </div>`);

    $('#modalRespondToMessage').modal("show");


    $('form.respondToMessage').submit(function (event) {
        event.preventDefault();

        let email = $('#email').val();
        let subject = $('#subject').val();
        let text = $('#messageText').val();


        $.ajax
        ({
            method: "POST",
            url: `http://localhost:8080/api/messages/respond`,
            dataType: 'json',
            contentType: 'application/json',
            //json object to sent to the authentication url
            data: JSON.stringify({
                "initialMessageID": id,
                "email": email,
                "subject": subject,
                "text": text,
            }),
            success: function () {
                $('#errormessageRespond').html('');
                $('#sendmessageRespond').html('');
                $('#sendmessageRespond').append(`<h5 style="color: green">Successfully sent!</h5>`)
                alert("Message sent!")
                closeModalRespond();
            },
            error: function (xhr, status, error) {
                $('#sendmessageRespond').html('');
                $('#sendmessageRespond').append(`<h5 style="color: green">Successfully sent!</h5>`)
                closeModalRespond();

                // } else if (err.status === 500) {
                //     $('#errormessage').append(`<h6 style="color: red">First name and Last name must be between 3 and 30 characters.</h6>`);
                // }
            }
        });
    })
};

function closeModalRespond() {

    $('#modalRespondToMessage').modal("hide");
}

function getMessageFromId(id) {
    $.ajax
    ({
        url: `http://localhost:8080/api/messages/${id}`,
        dataType: 'json',
        async: false,
        //json object to sent to the authentication url
        success: function (data) {
            messageFromId = data;
        },
        error: function (xhr, status, error) {
            let err = JSON.parse(xhr.responseText);
            alert(err.message);
        }
    });
}
function appendFilters() {
    $('#bs-example-navbar-collapse-1').html('');
    $('#bs-example-navbar-collapse-1').append(`           <ul class="nav navbar-nav navbar-right">
                    <li><button data-toggle="collapse" href="#filters"  style="background-color: #3d8394;margin-right: 2px" type="button" id="filtersBtn" class="btn btn-info navbar-btn">
                        <i class="glyphicon glyphicon-align-right"></i>
                        <span>Filters</span>
                    </button></li>
                </ul>`)
}
function emptyNav() {
    $('#bs-example-navbar-collapse-1').html('');
}

function getUnanswered() {
    clearALLContainers();


    $('#bs-example-navbar-collapse-1').html('');
    $('#bs-example-navbar-collapse-1').append(`       <ul class="nav navbar-nav navbar-right">
                    <li><a style="color: gray" onclick="getAllMessages()">Show All</a></li>
                    <li><a style="color: gray"onclick="getUnanswered()">Show Unanswered</a></li>

                </ul>`);

    $('#adminAccountContainer').append(`<h1>Messages:</h1>`);
    $.ajax({
        url: `http://localhost:8080/api/messages/pending`,
        "headers": {
            "Authorization": "Bearer " + localStorage.getItem('token'),
        },
        success: function (responece) {
            if (responece.length === 0) {
                $('#adminAccountContainer').append(`<h4>No unanswered messages yet.</h4>`);
            } else {
                $.each(responece, function (i) {
                    let answered = "Answered_" + responece[i].id;
                    getMessageCard(responece, i);
                    document.getElementById(answered).style.display = "none";

                })
            }
        }
    });
}
