
let inPending = false;
let inApproved = false;
let inCanceled = false;
let inRejected = false;
let inAll = false;
let imageURL;

function setInToFalse() {
    inPending = false;
    inApproved = false;
    inCanceled = false;
    inRejected = false;
    inAll = false;
}

function clearALLContainers() {
    $('#adminAccountContainer').html('');
    $('#cardContainerAgent').html('');
    $('#userAccountContainer').html('');

}

function showPending() {
    setInToFalse();
    inPending = true;
    appendFilters();
    clearALLContainers();
    $('#adminAccountContainer').html('');
    $('#cardContainerAgent').html('');
    $('#refContainer').html('');
    $.ajax({
        url: `http://localhost:8080/api/agent/pending`,
        "headers": {
            "Authorization": "Bearer " + localStorage.getItem('token'),
        },
        success: function (responece) {
            if (responece.length === 0) {
                $('#cardContainerAgent').append(`<h4>No pending policies</h4>`);
            } else {
                $('#cardContainerAgent').append(`<h3>${responece.length} Pending</h3>`);
                $.each(responece, function (i) {
                    let pictureId = "policyImage_" + responece[i].id;
                    $('#cardContainerAgent').append(getPolicyCard(responece, i));
                    animatePictures(pictureId);
                })
            }
        }
    });
}
function showAll() {
    setInToFalse();
    inAll = true;
    $('#refContainer').html('');
    inAll= true;
    $.ajax({
        url: `http://localhost:8080/api/policies/search?criteria=`,
        success: function (response) {
            $('#cardContainerAgent').html('');
            if (response.length === 0) {
                $('#cardContainerAgent').append(`<h3>No Policies</h3>`);
            } else {
                $('#cardContainerAgent').append(`<h3>${response.length} Results:</h3>`);
                $.each(response, function (i) {
                    let pictureId = "policyImage_" + response[i].id;
                    $('#cardContainerAgent').append(getPolicyCard(response, i));
                    animatePictures(pictureId);
                    if (response[i].pending === false) {
                        let a = "A/R_" + response[i].id;
                        document.getElementById(a).style.display = "none";
                    }
                })
            }
        }
    })
}
function showApproved() {
    $('#refContainer').html('');
    setInToFalse();
    inApproved = true;
    clearALLContainers();

    $.ajax({
        url: `http://localhost:8080/api/agent/approved`,
        "headers": {
            "Authorization": "Bearer " + localStorage.getItem('token'),
        },
        success: function (responece) {
            console.log(responece);
            if (responece.length === 0) {
                $('#cardContainerAgent').append(`<h4>No approved policies</h4>
<!--                <button onclick="requestPolicyStep1()" class="btn btn-primary">Request a policy</button>-->
                `);
            } else {
                $('#cardContainerAgent').append(`<h3>(${responece.length}) Approved Policies</h3>`);
                $.each(responece, function (i) {
                    let a = "A/R_" + responece[i].id;
                    let pictureId = "policyImage_" + responece[i].id;

                    $('#cardContainerAgent').append(getPolicyCard(responece, i));
                    document.getElementById(a).style.display = "none"
                    animatePictures(pictureId);

                })
            }
        }
    });
}


function showRejected() {
    $('#refContainer').html('');
    setInToFalse();
    inRejected = true;
    clearALLContainers();

    $.ajax({
        url: `http://localhost:8080/api/agent/rejected`,
        "headers": {
            "Authorization": "Bearer " + localStorage.getItem('token'),
        },
        "method": "GET",
        success: function (responece) {
            if (responece.length === 0) {
                $('#cardContainerAgent').append(`<h4>No rejected policies</h4>
<!--                <button onclick="requestPolicyStep1()" class="btn btn-primary">Request a policy</button>-->
                `);
            } else {
                $('#cardContainerAgent').append(`<h3>(${responece.length}) Rejected Policies</h3>`);
                $.each(responece, function (i) {
                    let a = "A/R_" + responece[i].id;
                    let pictureId = "policyImage_" + responece[i].id;
                    $('#cardContainerAgent').append(getPolicyCard(responece, i));
                    document.getElementById(a).style.display = "none";
                    animatePictures(pictureId);
                })
            }
        }
    });
}

function showCanceled() {
    $('#refContainer').html('');
    setInToFalse();
    inCanceled = true;
    clearALLContainers();

    $.ajax({
        url: `http://localhost:8080/api/agent/canceledByUser`,
        "headers": {
            "Authorization": "Bearer " + localStorage.getItem('token'),
        },
        "method": "GET",
        success: function (responece) {
            if (responece.length === 0) {
                $('#cardContainerAgent').append(`<h4>No canceled policies</h4>
<!--                <button onclick="requestPolicyStep1()" class="btn btn-primary">Request a policy</button>-->
                `);
            } else {
                $('#cardContainerAgent').append(`<h3>(${responece.length}) Canceled Policies</h3>`);
                $.each(responece, function (i) {
                    let a = "A/R_" + responece[i].id;
                    let pictureId = "policyImage_" + responece[i].id;
                    $('#cardContainerAgent').append(getPolicyCard(responece, i));
                    document.getElementById(a).style.display = "none"
                    animatePictures(pictureId);
                })
            }
        }
    });
}

function approvePolicy(policyId) {

    $.ajax({
        url: `http://localhost:8080/api/agent/approve/${policyId}`,
        "headers": {
            "Authorization": "Bearer " + localStorage.getItem('token'),
        },
        "method": "POST",
        success: function () {
            closeModalAR();
            showPending();
        }
    });


}


function rejectPolicy(policyId) {
    $.ajax({
        url: `http://localhost:8080/api/agent/reject/${policyId}`,
        "headers": {
            "Authorization": "Bearer " + localStorage.getItem('token'),
        },
        "method": "POST",
        success: function () {
            closeModalAR();
            showPending();
        }
    })

}


function showModalApprove(id) {
    $('#modalContainer').html('');
    $('#modalContainer').append(`
<div id="modalAreYouSure" class="modal fade modalSignOut" tabindex="-1" role="dialog" aria-labelledby="modalSignOut" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Approve</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h6>Are you sure you want to approve this policy?</h6>
      </div>
      <div class="modal-footer">
    <button type="button" class="btn btn-primary btn-sm" onclick="approvePolicy(${id})">Yes</button>
    <button type="button" class="btn btn-primary btn-sm" onclick="closeModalAR()">Cancel</button>
  </div>
</div>`);

    $('#modalAreYouSure').modal("show");
}

function closeModalAR() {
    $('#modalAreYouSure').modal("hide");
}

function showModalReject(id) {
    $('#modalContainer').html('');
    $('#modalContainer').append(`
<div id="modalAreYouSure" class="modal fade modalSignOut" tabindex="-1" role="dialog" aria-labelledby="modalSignOut" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Reject</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h6>Are you sure you want to reject this policy?</h6>
      </div>
      <div class="modal-footer">
    <button type="button" class="btn btn-primary btn-sm" onclick="rejectPolicy(${id})">Yes</button>
    <button type="button" class="btn btn-primary btn-sm" onclick="closeModalAR()">Cancel</button>
  </div>
</div>`);

    $('#modalAreYouSure').modal("show");
}


function animatePictures(pictureId) {
    let modal = document.getElementById("myModalImg");

    let img = document.getElementById(pictureId);
    let modalImg = document.getElementById("img01");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
    };

    let span = document.getElementsByClassName("img-close")[0];

    span.onclick = function () {
        modal.style.display = "none";
    };
}

function getPolicyCard(responce, i) {
    console.log(responce[i].carDetails.imageRegCertificate);

    let totPrice = responce[i].totalPrice.toFixed(2);

    let status;
    if (responce[i].pending && !(responce[i].canceledByUser)) {
        status = "Pending"
    } else if (responce[i].approved) {
        status = "Approved"
    } else if (responce[i].canceledByUser) {
        status = "Canceled by User"
    } else if (!(responce[i].approved) && !(responce[i].pending)) {
        status = "Rejected"
    }

    return `<div class="line"></div>
    <div  class="card">
        <div class="container" >
        <div class="card-body">
            <h1>Policy Details:<a> </a> </h1> 
                <div class="row">
                    <div class="col-sm-4">
                        <h5 style="color: rgb(96,192,220);">Brand: </h5><h5>${responce[i].carDetails.car.carBrand.brand}</h5>
                        <h5 style="color: rgb(96,192,220);">Model: </h5><h5>${responce[i].carDetails.car.model} </h5>
                        <h5 style="color: rgb(96,192,220);">Cubic capacity: </h5><h5> ${responce[i].carDetails.cubic_capacity}</h5>
                    </div>
                    <div class="col-sm-4">
                         <h5 style="color: rgb(96,192,220);">First registration date: </h5><h5>${responce[i].carDetails.regDate} </h5>
                        <h5 style="color: #60c0dc;"> Driver age: </h5><h5>${responce[i].driverDetails.age} </h5>
                        <h5 style="color: #60c0dc;">Accidents in previous year: </h5><h5>${responce[i].carDetails.accidents} </h5>
                        <h5 style="color: #60c0dc;">Policy activation date: </h5> <h5>${responce[i].statDate}</h5>
                    </div>
                    <div class="col-sm-3">
                        <img id="policyImage_${responce[i].id}" style="max-height: 120px; margin-left: 15px"  src="../picturesUploaded/${responce[i].carDetails.imageRegCertificate}" class="img-thumbnail" alt="img"/>
                    </div>
                </div>
       
        <h3>Contact Information: </h3> 
            <div class="row">
                <div class="col-sm-4">
                    <h5 style="color: rgb(96,192,220);">Email: </h5><h5>  ${responce[i].driverDetails.email} </h5>
                  <h5 style="color: rgb(96,192,220);">Phone: </h5><h5>  ${responce[i].driverDetails.phone}<a> </a> </h5>
                    <h5 style="color: rgb(96,192,220);">Address: </h5><h5> ${responce[i].driverDetails.address.text}</h5>
                    <h5 style="color: rgb(96,192,220);">No: </h5><h5> ${responce[i].driverDetails.address.no} </h5>
                </div>
                  <div class="col-sm-4">
                    <h5 style="color: rgb(96,192,220);">City: </h5><h5> ${responce[i].driverDetails.address.city}</h5>
                   <h5 style="color: rgb(96,192,220);">Post Code: </h5><h5> ${responce[i].driverDetails.address.postalCode} </h5>
                </div>
              
                <div  id="price" class="col-sm-4">    
                   <h5 style="color: rgb(96,192,220);">Status: </h5><h5> ${status} </h5>                
                   <h3>Total Price: </h3><h3> ${totPrice} lv.</h3>

                </div>
                </div>
           
               <br>
                <div style="display: flex;">
                   <div id="A/R_${responce[i].id}">
                    <button onclick="showModalApprove(${responce[i].id})" type="button" class="btn btn-success">Approve</button>
                    <button onclick="showModalReject(${responce[i].id})" type="button" class="btn btn-danger">Reject</button>
                </div>
               <div style="margin-left: 15px" id="errorMessageRequest"></div>
               </div>

            </div>
        </div>
        </div>
    </div>
</div>`;
}


function getImagePolicy(policyId) {
    $.ajax({
        url: `http://localhost:8080/api/policies/${policyId}/policypicture`,
        "headers": {
            "Authorization": "Bearer " + localStorage.getItem('token'),
        },
        success: function (data) {
            let pictureId = "#policyImage_" + policyId;
            console.log("Picture ID :" + pictureId);
            // $(pictureId).append(`<img id="img_${policyId}" style="max-height: 120px; margin-left: 15px"  src="../picturesUploaded/${data}" class="img-thumbnail" alt="img"/>`);
            let imgId = "img_" + policyId;

            animatePictures(imgId);
        }
    });
}