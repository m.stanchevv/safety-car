$('#signUpForm').submit(function (event) {
    event.preventDefault();
    $('#errormessageSignUp').html('');


    let firstName = $('#firstNameSignUp').val();
    let secondName = $('#secondNameSignUp').val();
    let newPassword = $('#passwordSignUp').val();
    let confirmedPass = $('#confirmPassword').val();
    let newEmail = $('#emailSignUp').val();

    if (newPassword === confirmedPass) {
        $.ajax
        ({
            type: "POST",
            // headers: {
            //     'Access-Control-Allow-Origin': '*'
            // },
            url: `http://localhost:8080/api/users/signup`,
            contentType: 'application/json',
            // dataType: 'json',
            async: true,
            //json object to sent to the authentication url
            data: JSON.stringify({
                "firstName": firstName,
                "lastName": secondName,
                "password": newPassword,
                "email": newEmail,
            }),

            success: function () {
                console.log("SUCCESS SIGN UP");
                localStorage.setItem('email', newEmail);
                $('#sighUpModal').modal("hide");
                showModalThankYouForRegister();
            },
            error: function (xhr, status, error) {
                console.log("ERROR SIGN UP");
                $('#sighUpModal').modal("hide");
                $('#errormessageSignUp').html('');
                let err = JSON.parse(xhr.responseText);
                console.log(err.message);
                $('#errormessageSignUp').append(`<h6 style="color : red">${err.message}</h6>`)
            }
        });
    } else {
        $('#errormessageSignUp').append(`<h6 style="color: red">Passwords do not match</h6>`)
    }
});


function showModalThankYouForRegister() {
    $('#modalContainer').html('');
    $('#modalContainer').append(`
<div id="modalAreYouSure" class="modal fade modalSignOut" tabindex="-1" role="dialog" aria-labelledby="modalSignOut" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLongTitle">Thank You For Signing Up</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <h6>Shortly you will receive an email with a confirmation link.</h6>
        <h6>Please follow the link in order to complete your registration.</h6>
      </div>
      <div class="modal-footer">
    <button type="button" class="btn btn-primary btn-sm" onclick="closeModalAR()">Close</button>
  </div>
</div>`);

    $('#modalAreYouSure').modal("show");
    $('#sighUpModal').modal("hide");
}

function closeModalAR() {
    $('#modalAreYouSure').modal("hide");
    $('#sighUpModal').modal("hide");
}