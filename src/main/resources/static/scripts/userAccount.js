function addCss(fileName) {
    let link = $("<link />", {
        rel: "stylesheet",
        type: "text/css",
        href: fileName
    })
    $('head').append(link);
}


function showUserAccountPage() {
    $('#mainContainer').html('');
    $('link[rel=stylesheet][href*="style"]').remove();

    addCss("styles/style3.css");
    $('#mainContainer').append(` 
  <style>
        /*
    DEMO STYLE
*/
        @import "https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700";


        body {
            font-family: 'Poppins', sans-serif;
            background: #fafafa;
        }

        p {
            font-family: 'Poppins', sans-serif;
            font-size: 1.1em;
            font-weight: 300;
            line-height: 1.7em;
            color: #999;
        }

        a, a:hover, a:focus {
            color: inherit;
            text-decoration: none;
            transition: all 0.3s;
        }

        .navbar {
            padding: 15px 10px;
            background: #fff;
            border: none;
            border-radius: 0;
            margin-bottom: 40px;
            box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.1);
        }

        .navbar-btn {
            box-shadow: none;
            outline: none !important;
            border: none;
        }

        .line {
            width: 100%;
            height: 1px;
            border-bottom: 1px dashed #ddd;
            margin: 40px 0;
        }

        /* ---------------------------------------------------
            SIDEBAR STYLE
        ----------------------------------------------------- */
        #sidebar {
            width: 250px;
            position: fixed;
            top: 0;
            left: 0;
            height: 100vh;
            z-index: 999;
            background: #367585;
            color: #fff;
            transition: all 0.3s;
        }

        #sidebar.active {
            margin-left: -250px;
        }

        #sidebar .sidebar-header {
            padding: 20px;
            background: #3d8394;
        }

        #sidebar ul.components {
            padding: 20px 0;
            border-bottom: 1px solid #437c8b;
        }

        #sidebar ul p {
            color: #fff;
            padding: 10px;
        }

        #sidebar ul li a {
            padding: 10px;
            font-size: 1.1em;
            display: block;
        }

        #sidebar ul li a:hover {
            color: #3e95a0;
            background: #fff;
        }

        #sidebar ul li.active > a, a[aria-expanded="true"] {
            color: #fff;
            background: #3a909b;
        }


        a[data-toggle="collapse"] {
            position: relative;
        }

        a[aria-expanded="false"]::before, a[aria-expanded="true"]::before {
            content: '\\e259';
            display: block;
            position: absolute;
            right: 20px;
            font-family: 'Glyphicons Halflings';
            font-size: 0.6em;
        }

        a[aria-expanded="true"]::before {
            content: '\\e260';
        }


        ul ul a {
            font-size: 0.9em !important;
            padding-left: 30px !important;
            background: #36828c;
        }

        ul.CTAs {
            padding: 20px;
        }

        ul.CTAs a {
            text-align: center;
            font-size: 0.9em !important;
            display: block;
            border-radius: 5px;
            margin-bottom: 5px;
        }

        a.download {
            background: #fff;
            color: #3d8f99;
        }

        a.article, a.article:hover {
            background: #377c86 !important;
            color: #fff !important;
        }


        /* ---------------------------------------------------
            CONTENT STYLE
        ----------------------------------------------------- */
        #content {
            width: calc(100% - 250px);
            padding: 40px;
            min-height: 100vh;
            transition: all 0.3s;
            position: absolute;
            top: 0;
            right: 0;
        }

        #content.active {
            width: 100%;
        }


        /* ---------------------------------------------------
            MEDIAQUERIES
        ----------------------------------------------------- */
        @media (max-width: 768px) {
            #sidebar {
                margin-left: -250px;
            }

            #sidebar.active {
                margin-left: 0;
            }

            #content {
                width: 100%;
            }

            #content.active {
                width: calc(100% - 250px);
            }

            #sidebarCollapse span {
                display: none;
            }
        }
        
        
        
.img-thumbnail{
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}
.img-thumbnail:hover {opacity: 0.7;}


.img-modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (Image) */
.img-modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Add Animation - Zoom in the Modal */
.img-modal-content {
    animation-name: zoom;
    animation-duration: 0.6s;
}

@keyframes zoom {
    from {transform:scale(0)}
    to {transform:scale(1)}
}

/* The Close Button */
.img-close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}

.img-close:hover,
.img-close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .img-modal-content {
        width: 100%;
    }
}


    </style>
 <div onload="requestPolicyStep1()" class="wrapper">
    <nav id="sidebar">
        <div class="sidebar-header">
            <a style="font-size: 25px; font-weight: bolder" href="">Safety Car</a>
        </div>

        <ul class="list-unstyled components">
            <p>User Page</p>
            <li class="active">
                <a onclick="requestPolicyStep1()" >Request a Policy</a>
          
            </li>
           
            <li>
                <a onclick="getPendingPolicies()" >Pending Policies</a>
            </li>   
            <li>
                <a onclick="getPolicyHistory()" >History</a>
            </li>
            <li>
                <a onclick="getUserAccount()" >Account</a>
            </li>
                <li>
                <a onclick="showModalSighOut()">Log Out</a>
            </li>


        </ul>
    </nav>
    
    <!-- Page Content Holder -->
    <div id="content">

        <nav class="navbar navbar-default">
            <div class="container-fluid">

                <div class="navbar-header">
                    <button style="background-color: #3d8394" type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                        <i class="glyphicon glyphicon-align-left"></i>
                        <span >Toggle Sidebar</span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<!--                    <ul class="nav navbar-nav navbar-right">-->
<!--                    <li><a>Order By:</a></li>-->
<!--                    <li><a href="#">Latest</a></li>-->
<!--                    <li><a href="#">Oldest</a></li>-->
<!--                </ul>-->
                </div>
                </div>
              </nav>
              
              
              
        <div onload="requestPolicyStep1()" id="userAccountContainer">
        </div>
        </div>
  

<script type="text/javascript">
    $(document).ready(function () {
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar, #content').toggleClass('active');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
               });
     
    });
</script>
    

 
 `);
}

function getUserAccount() {
    $('#errorEditAccount').html(' ');
    $('#userAccountContainer').html('');
    $('#userAccountContainer').append(`<div >
  <h1 >Your Account</h1>
  <br>
    <p >First name: ${userLogedIn.firstName}</p>
    <br>
    <p >Last name: ${userLogedIn.lastName}</p>
    <br>
    <p >Email: ${userLogedIn.email}</p>
    <br>
    <p >Role: ${localStorage.getItem("role")}</p>
    <button onclick="editAccount()" class="btn btn-primary">Edit</button>
  </div>
</div>`)
}


function editAccount() {

    $('#firstNameUpdate').append(userLogedIn.firstName);
    $('#secondNameUpdate').append(userLogedIn.lastName);

    $('#userAccountContainer').html('');
    $('#userAccountContainer').append(`   <div style="margin-right: 300px">
     <h1>Your Account</h1>
                <form>
                           <div class="form-row">
                           <div class="form-group col-md-5">

                        <input id="firstNameUpdate" class="form-control" type="text" placeholder="First Name"><br>
                        <input id="secondNameUpdate" class="form-control" type="text" placeholder="Second Name"><br>
                        <input id="oldPassword" class="form-control" type="password" placeholder="Old Password"><br>
                        <input id="newPassword" class="form-control" type="password" placeholder="New Password"><br>
                        </div>       
                      </div>
               </form>
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />   
              <br />
              <br />
              <br /> 
              <br />
              <br />
              
                        <div style="display: flex; margin-left: 15px">
                            <div>
                               <button onclick="updateAccount()" type="button" class="btn btn-primary">Save</button>
                            </div>
                            <div style="margin-left: 20px">
                               <button onclick="getUserAccount()" type="button" class="btn btn-outline-primary">Cancel</button>
                            </div>
                          <div style="margin-left: 20px" id="errorEditAccount">
                          </div>

                         </div>
</div>`);
}


function updateAccount() {

    let firstName = $('#firstNameUpdate').val();
    let lastName = $('#secondNameUpdate').val();
    let oldPassword = $('#oldPassword').val();
    let newPassword = $('#newPassword').val();

    $.ajax
    ({
        type: "PUT",
        url: `http://localhost:8080/api/users/editaccount`,
        headers: {Authorization: "Bearer " + localStorage.getItem("token")},
        dataType: 'json',
        contentType: 'application/json',
        //json object to sent to the authentication url
        data: JSON.stringify({
            "firstName": firstName, "lastName": lastName,
            "email": userLogedIn.email,
            "oldPassword": oldPassword, "newPassword": newPassword
        }),
        success: function (response) {
            $('#errorEditAccount').html(' ');
            $('#userAccountContainer').html('');
            $('#userAccountContainer').append(``);
            userLogedIn = response;
            getUserAccount();
        },
        error: function (xhr, status, error) {
            $('#errorEditAccount').html(' ');

            let xhrResponse = JSON.parse(xhr.responseText);
            let arrayErrors = xhrResponse.errors;

            if (typeof arrayErrors === "undefined") {
                $('#errorEditAccount').append(`<h5 style="color: red">${xhrResponse.message}</h5>`);

            } else {

                $.each(arrayErrors, function (i) {
                    $('#errorEditAccount').append(`<h5 style="color: red">${arrayErrors[i].defaultMessage}</h5>`);
                })

            }
        }
    })
    $('#errorEditAccount').html(' ');

}


function getPendingPolicies() {

    $('#userAccountContainer').html('');
    $('#userAccountContainer').append(` <h1> Pending policies </h1>`);
    $.ajax({
        url: `http://localhost:8080/api/users/pendingpolicies`,
        "headers": {
            "Authorization": "Bearer " + localStorage.getItem('token'),
        },
        "method": "GET",
        success: function (responece) {
            console.log(responece);
            if (responece.length === 0) {
                $('#userAccountContainer').append(`<h4>No pending policies</h4>
                <button onclick="requestPolicyStep1()" class="btn btn-primary">Request a policy</button>
                `);
            } else {
                $.each(responece, function (i) {
                        let a = "CancelBtn_" + responece[i].id;
                        let pictureId = "policyImage_" + responece[i].id;
                        $('#userAccountContainer').append(`<br><br>`);
                        $('#userAccountContainer').append(getPolicyCardUser(responece, i));
                        // document.getElementById(a).style.display = "none"

                        let modal = document.getElementById("myModalImg");

                        let img = document.getElementById(pictureId);
                        let modalImg = document.getElementById("img01");
                        img.onclick = function () {
                            modal.style.display = "block";
                            modalImg.src = this.src;
                        };

                        let span = document.getElementsByClassName("img-close")[0];

                        span.onclick = function () {
                            modal.style.display = "none";
                        };

                    }
                )
            }
        }
    });

}


function getPolicyCardUser(responce, i) {

    let status;
    if (responce[i].pending) {
        status = "Pending"
    } else if (responce[i].approved && !(responce[i].canceledByUser)) {
        status = "Approved"
    } else if (responce[i].canceledByUser) {
        status = "Canceled by User"
    } else if (!(responce[i].approved) && !(responce[i].pending)) {
        status = "Rejected"
    }

    let prise = responce[i].totalPrice.toFixed(2);

    let card = `<div class="line"></div>
    <div  class="card">
        <div class="container" >
        <div class="card-body">
            <h1>Policy Details:<a> </a> </h1> 
                <div class="row">
                    <div class="col-sm-4">
                        <h5 style="color: rgb(96,192,220);">Brand: </h5><h5>${responce[i].carDetails.car.carBrand.brand}</h5>
                        <h5 style="color: rgb(96,192,220);">Model: </h5><h5>${responce[i].carDetails.car.model} </h5>
                        <h5 style="color: rgb(96,192,220);">Cubic capacity: </h5><h5> ${responce[i].carDetails.cubic_capacity}</h5>
                    </div>
                    <div class="col-sm-4">
                         <h5 style="color: rgb(96,192,220);">First registration date: </h5><h5>${responce[i].carDetails.regDate} </h5>
                        <h5 style="color: #60c0dc;"> Driver age: </h5><h5>${responce[i].driverDetails.age} </h5>
                        <h5 style="color: #60c0dc;">Accidents in previous year: </h5><h5>${responce[i].carDetails.accidents} </h5>
                        <h5 style="color: #60c0dc;">Policy activation date: </h5> <h5>${responce[i].statDate}</h5>
                    </div>
                    <div class="col-sm-3">
                        <img id="policyImage_${responce[i].id}" style="max-height: 120px; margin-left: 15px"  
                        src="../picturesUploaded/${responce[i].carDetails.imageRegCertificate}" 
                        class="img-thumbnail" alt="img"/>
                    </div>
                </div>
       
        <h3>Contact Information: </h3> 
            <div class="row">
                <div class="col-sm-4">
                    <h5 style="color: rgb(96,192,220);">Email: </h5><h5>  ${responce[i].driverDetails.email} </h5>
                  <h5 style="color: rgb(96,192,220);">Phone: </h5><h5>  ${responce[i].driverDetails.phone}<a> </a> </h5>
                    <h5 style="color: rgb(96,192,220);">Address: </h5><h5> ${responce[i].driverDetails.address.text}</h5>
                    <h5 style="color: rgb(96,192,220);">No: </h5><h5> ${responce[i].driverDetails.address.no} </h5>
                </div>
                  <div class="col-sm-4">
                    <h5 style="color: rgb(96,192,220);">City: </h5><h5> ${responce[i].driverDetails.address.city}</h5>
                   <h5 style="color: rgb(96,192,220);">Post Code: </h5><h5> ${responce[i].driverDetails.address.postalCode} </h5>
                </div>
              
                <div  id="price" class="col-sm-4">    
                   <h5 style="color: rgb(96,192,220);">Status: </h5><h5> ${status} </h5>                
                   <h3>Total Price: </h3><h3> ${prise} lv.</h3>

                </div>
                </div>
           
               <br>
                <div id="userCardButtons_${responce[i].id}" style="display: flex;">
                    <div  id="CancelBtn_${responce[i].id}" class="col-sm-2">
                    <button onclick="showModalCancelRequest(${responce[i].id})" type="button" class="btn btn-primary">Cancel</button>
                </div>
               <div style="margin-left: 15px" id="errorMessagePolicy"></div>
               </div>

            </div>
        </div>
        </div>
    </div>
</div>`;


    return card;
}


function showModalCancelRequest(policyId) {
    $('#modalContainer').html('');
    $('#modalContainer').append(`
    <div id="modalCancelRequest" class="modal fade modalSignOut" tabindex="-1" role="dialog" aria-labelledby="modalSignOut" >
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Cancel request?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h6>Are you sure you want to cancel your request?</h6>
      </div>
      <div class="modal-footer">
    <button type="button" class="btn btn-primary btn-sm" onclick="cancelPolicyRequest(${policyId})">Yes</button>
    <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" aria-label="Close">Cancel</button>
  </div>
</div>`);

    $('#modalCancelRequest').modal("show");
}


function cancelPolicyRequest(idPolicy) {
    $.ajax({
        url: `http://localhost:8080/api/users/cancel/${idPolicy}`,
        "headers": {
            "Authorization": "Bearer " + localStorage.getItem('token'),
        },
        "method": "POST",
        success: function (responece) {
            $('#modalCancelRequest').modal("hide");
            $('#userAccountContainer').html('');
            $('#userAccountContainer').append(`<h5>Request Canceled.</h5>
    <button onclick="requestPolicyStep1()" class="btn btn-primary">Send another request?</button>
`);

        }
    });
}


// function closeModal() {
//     $('#modalCancelRequest').modal("hide");
// }


function getPolicyHistory() {
    $('#userAccountContainer').html('');
    $('#userAccountContainer').append(`<h1>History</h1>`);
    $.ajax({
        url: `http://localhost:8080/api/users/history/${userLogedIn.id}`,
        "headers": {
            "Authorization": "Bearer " + localStorage.getItem('token'),
        },
        "method": "GET",
        success: function (responece) {
            $.each(responece, function (i) {
                let a = "CancelBtn_" + responece[i].id;
                let pictureId = "policyImage_" + responece[i].id;

                $('#userAccountContainer').append(`<br><br>`);
                $('#userAccountContainer').append(getPolicyCardUser(responece, i));
                if (!responece[i].pending) {
                    document.getElementById(a).style.display = "none"
                }

                let modal = document.getElementById("myModalImg");

                let img = document.getElementById(pictureId);
                let modalImg = document.getElementById("img01");
                img.onclick = function () {
                    modal.style.display = "block";
                    modalImg.src = this.src;
                };

                let span = document.getElementsByClassName("img-close")[0];

                span.onclick = function () {
                    modal.style.display = "none";
                };


            })
        },
        error: function (xhr, status, error) {
            $('#errormessageSignUp').html(' ');
            let err = JSON.parse(xhr.responseText);
            console.log(err.status);
            if (err.status === 400) {
                $('#userAccountContainer').append(`<h4>No Policy History</h4>
                <button onclick="requestPolicyStep1()" class="btn btn-primary">Request a policy</button>
                `);
            }
        }
    });

}





