function addCss(fileName) {
    let link = $("<link />", {
        rel: "stylesheet",
        type: "text/css",
        href: fileName
    });
    $('head').append(link);
}


function showAgentPage() {

    $('#mainContainer').html('');

    $('link[rel=stylesheet][href*="styles/style.css"]').remove();
    $('link[rel=stylesheet][href*="styles/login.css"]').remove();
    $('link[rel=stylesheet][href*="styles/map.css"]').remove();

    addCss("styles/style3.css");

    $('#mainContainer').append(`

  <style>
        /*
    DEMO STYLE
*/
        @import "https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700";


        body {
            font-family: 'Poppins', sans-serif;
            background: #fafafa;
        }

        p {
            font-family: 'Poppins', sans-serif;
            font-size: 1.1em;
            font-weight: 300;
            line-height: 1.7em;
            color: #999;
        }

        a, a:hover, a:focus {
            color: inherit;
            text-decoration: none;
            transition: all 0.3s;
        }

        .navbar {
            padding: 15px 10px;
            background: #fff;
            border: none;
            border-radius: 0;
            margin-bottom: 40px;
            box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.1);
        }

        .navbar-btn {
            box-shadow: none;
            outline: none !important;
            border: none;
        }

        .line {
            width: 100%;
            height: 1px;
            border-bottom: 1px dashed #ddd;
            margin: 40px 0;
        }

        /* ---------------------------------------------------
            SIDEBAR STYLE
        ----------------------------------------------------- */
        #sidebar {
            width: 250px;
            position: fixed;
            top: 0;
            left: 0;
            height: 100vh;
            z-index: 999;
            background: #367585;
            color: #fff;
            transition: all 0.3s;
        }

        #sidebar.active {
            margin-left: -250px;
        }

        #sidebar .sidebar-header {
            padding: 20px;
            background: #3d8394;
        }

        #sidebar ul.components {
            padding: 20px 0;
            border-bottom: 1px solid #437c8b;
        }

        #sidebar ul p {
            color: #fff;
            padding: 10px;
        }

        #sidebar ul li a {
            padding: 10px;
            font-size: 1.1em;
            display: block;
        }

        #sidebar ul li a:hover {
            color: #3e95a0;
            background: #fff;
        }

        #sidebar ul li.active > a, a[aria-expanded="true"] {
            color: #fff;
            background: #3a909b;
        }


        a[data-toggle="collapse"] {
            position: relative;
        }

        a[aria-expanded="false"]::before, a[aria-expanded="true"]::before {
            content: '\\e259';
            display: block;
            position: absolute;
            right: 20px;
            font-family: 'Glyphicons Halflings';
            font-size: 0.6em;
        }

        a[aria-expanded="true"]::before {
            content: '\\e260';
        }


        ul ul a {
            font-size: 0.9em !important;
            padding-left: 30px !important;
            background: #36828c;
        }

        ul.CTAs {
            padding: 20px;
        }

        ul.CTAs a {
            text-align: center;
            font-size: 0.9em !important;
            display: block;
            border-radius: 5px;
            margin-bottom: 5px;
        }

        a.download {
            background: #fff;
            color: #3d8f99;
        }

        a.article, a.article:hover {
            background: #377c86 !important;
            color: #fff !important;
        }


        /* ---------------------------------------------------
            CONTENT STYLE
        ----------------------------------------------------- */
        #content {
            width: calc(100% - 250px);
            padding: 40px;
            min-height: 100vh;
            transition: all 0.3s;
            position: absolute;
            top: 0;
            right: 0;
        }

        #content.active {
            width: 100%;
        }


        /* ---------------------------------------------------
            MEDIAQUERIES
        ----------------------------------------------------- */
        @media (max-width: 768px) {
            #sidebar {
                margin-left: -250px;
            }

            #sidebar.active {
                margin-left: 0;
            }

            #content {
                width: 100%;
            }

            #content.active {
                width: calc(100% - 250px);
            }

            #sidebarCollapse span {
                display: none;
            }
        }
        
            
        
.img-thumbnail{
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}
.img-thumbnail:hover {opacity: 0.7;}


.img-modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (Image) */
.img-modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Add Animation - Zoom in the Modal */
.img-modal-content {
    animation-name: zoom;
    animation-duration: 0.6s;
}

@keyframes zoom {
    from {transform:scale(0)}
    to {transform:scale(1)}
}

/* The Close Button */
.img-close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}

.img-close:hover,
.img-close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .img-modal-content {
        width: 100%;
    }
}



    </style>
<div onload="showPending()" class="wrapper">
    <!-- Sidebar Holder -->
    <nav id="sidebar">
        <div href="" class="sidebar-header">
            <a style="font-size: 25px; font-weight: bolder" href="">Safety Car</a>  
       </div>

        <ul class="list-unstyled components">
            <p>Agent Page</p>
<!--            <li class="active">-->
<!--                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false">Policies</a>-->
<!--                <ul class="collapse list-unstyled" id="homeSubmenu">-->
<!--                    <li><a onclick="showPending()" href="#">Pending</a></li>-->
<!--                    <li><a onclick="showApproved()" href="#">Approved</a></li>-->
<!--                    <li><a onclick="showRejected()" href="#">Rejected</a></li>-->
<!--                    <li><a onclick="showCanceled()" href="#">Canceled</a></li>-->
<!--                </ul>-->
<!--            </li>-->
  <li>
                <a onclick="showPending()">Policies</a>
            </li>
                   <li >
                <a onclick="requestPolicyStep1()" >Request a Policy</a>
          
            </li>
            <li>
                <a onclick="getReferenceTable()">Reference table</a>
            </li>
                <li>
                <a onclick="getAgentAccount()">Account</a>
            </li>
                <li>
                <a onclick="showModalSighOut()">Log Out</a>
            </li>


        </ul>

       
          <ul class="list-unstyled CTAs">
            <li><a onclick="showUploadReferenceTable()" class="upload">Upload new table</a></li>
        </ul>
    </nav>

    <!-- Page Content Holder -->
    <div id="content">

        <nav class="navbar navbar-default">
            <div class="container-fluid">

                <div class="navbar-header">
                    <button style="background-color: #3d8394" type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                        <i class="glyphicon glyphicon-align-left"></i>
                        <span>Toggle Sidebar</span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               
                    <ul class="nav navbar-nav navbar-right">
                    <li><button data-toggle="collapse" href="#filters"  style="background-color: #3d8394;margin-right: 2px" type="button" id="filtersBtn" class="btn btn-info navbar-btn">
                        <i class="glyphicon glyphicon-align-right"></i>
                        <span>Filters</span>
                    </button></li>

                </ul>
                </div>
            </div>
        </nav>
        <div class="collapse  navbar navbar-default"  id="filters">
          <div class="row">         
  <div class="col-md-12">   
  <div class="container">       
<div class="btn-group" role="group" aria-label="Basic example">
  <button type="button" onclick="showPending()" class="btn btn-info navbar-btn">Pending</button>
  <button type="button" onclick="showApproved()" class="btn btn-info navbar-btn">Approved</button>
  <button type="button" onclick="showRejected()" class="btn btn-info navbar-btn">Rejected</button>
  <button type="button" onclick="showCanceled()" class="btn btn-info navbar-btn">Canceled</button>
  <button type="button" onclick="showAll()" class="btn btn-info navbar-btn">All</button>
</div></div></div></div>
  <form id="filterForm">

<br/>
     <div class="form-row">
        <div class="col-md-3">             
            <label  for="carBrandFilter">Car Brand</label>
             <select onchange="addCarModelToForm()" id="inputCarBrandBrand" class="form-control is-invalid">
                                <option id="optionSelectedCarBrand" value="">Choose...</option>
                            </select>
        </div>
        <div class="form-group col-md-3">
            <label for="carModelFilter">Car Model</label>
              <select id="inputCarModel" class="form-control is-invalid">
                                <option id="optionCarModel" value="">Choose...</option>
                            </select>
        </div>
        <div class="col-md-3">
            <label for="ccFilter">Cubic Capacity</label>
            <input type="text" class="form-control is-invalid" id="ccFilter"/>
        </div>
          <div class="form-group col-md-3">
            <label for="datetimepickerFRD">First Registration Date</label>
                <input type='text' class="form-control" id='datetimepickerFRD'/>
        </div>
    </div>
      <div class="form-row">
           <div class="form-group col-md-3">
            <label for="datetimepickerAFfrom">Active from (from)</label>
            <input type='text' class="form-control" id='datetimepickerAFfrom'/>
        </div> 
        <div class="form-group col-md-3">
            <label for="datetimepickerAFto">Active from (to)</label>
            <input type='text' class="form-control" id='datetimepickerAFto'/>
        </div> 
           <div class="form-group col-md-3">
            <label for="datetimepickerDOfrom">Date ordered (from)</label>
            <input type='text' class="form-control" id='datetimepickerDOfrom'/>
        </div> 
          <div class="form-group col-md-3">
            <label for="datetimepickerDOto">Date ordered (to)</label>
            <input type='text' class="form-control" id='datetimepickerDOto'/>
        </div> 

              </div>
                 <div class="form-row">
       <div class="form-group col-md-3">
            <label for="driverAgeFilter">Driver's age</label>
            <input oninput="validateAge(this)" type="text" class="form-control" id="driverAgeFilter"/>
        </div>
         <div class="form-group col-md-3">
            <label for="driverEmailFilter">Driver's email</label>
            <input oninput="validateEmail(this)" type="email" class="form-control" id="DriverEmailFilter" />
        </div>
         <div class="form-group col-md-3">
            <label for="driverPhoneFilter">Driver's phone</label>
            <input oninput="validatePhone(this)" type="text" class="form-control" id="DriverPhoneFilter" />
        </div>
        <div style="margin-top: 30px" class="form-group col-md-3">
                   <div class="form-check"> 
           <input class="form-check-input" type="checkbox" id="gridCheckAccidents">
            <label class="form-check-label" for="gridCheckAccidents">   
                Accidents
            </label>
            </div>
               </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-3">
       <button style="background-color: #3d8394" type="submit" id="filtersBtn" class="btn btn-info navbar-btn">
                    <i class="glyphicon glyphicon-search"></i><span> Search</span></button>

<button style="background-color: #3d8394" type="reset" class="btn btn-info navbar-btn">
                    <i class="glyphicon glyphicon-remove"></i><span> Reset</span></button></div>
    </div>
   
     </form>
     </div>
        <div id="pageContent">
  
</div>     
          <div onload="showPending()" id="cardContainerAgent">
                </div>
                <div id="agentErroContainer">
                </div>  
                    <div id="userAccountContainer">
        </div>
                      <div id="refContainer">
                </div>  
               
                </div>
                </div>
              
                
<script type="text/javascript">
    $(document).ready(function () {
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar, #content').toggleClass('active');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });
</script>
               
`);
    $('#datetimepickerFRD').datetimepicker({
        viewMode: 'years',
        format: 'YYYY-MM-DD'
    });

    $('#datetimepickerAFfrom').datetimepicker({
        viewMode: 'years',
        format: 'YYYY-MM-DD'
    });
    $('#datetimepickerDOfrom').datetimepicker({
        viewMode: 'years',
        format: 'YYYY-MM-DD'
    });
    $('#datetimepickerAFto').datetimepicker({
        viewMode: 'years',
        format: 'YYYY-MM-DD'
    });
    $('#datetimepickerDOto').datetimepicker({
        viewMode: 'years',
        format: 'YYYY-MM-DD'
    });


    addCarBrandToForm();
    addCarModelToForm();

    $('#filterForm').submit(function (event) {
        event.preventDefault();

        let brand = $("#inputCarBrandBrand option:selected").text();
        let model = $('#inputCarModel option:selected').text();
        let cc = $('#ccFilter').val();
        let FRD = $('#datetimepickerFRD').val();
        let age = $('#driverAgeFilter').val();
        let hasAccidents;
        if ($('#gridCheckAccidents').is(":checked")) {
            hasAccidents = true;
        } else {
            hasAccidents = false;
        }
        brand.slice(" ");
        // let pending = $('#optionPending').is(":checked");
        // let canceled = $('#optionCanceled').is(":checked");
        // let rejected = !!$('#optionRejected').is(":checked");
        // let approved = $('#optionApproved').is(":checked");
        // let all = $('#optionAll').is(":checked");

        let driverEmail = $('#driverEmailFilter').val();
        let driverPhone = $('#driverPhoneFilter').val();
        let activeFromFrom = $('#datetimepickerAFfrom').val();
        let activeFromTo = $('#datetimepickerAFto').val();
        let dateOfOrderFrom = $('#datetimepickerDOfrom').val();
        let dateOfOrderTo = $('#datetimepickerDOto').val();

        let criteria = "";

        if (hasAccidents) {
            criteria += "accidents:true,";
        }
        if (inPending) {
            criteria += "pending:true,";
        }
        if (inApproved) {
            criteria += "approved:true,";
        }
        if (inCanceled) {
            criteria += "canceledByUser:true,";
        }
        if (brand !== "Choose...") {
            criteria += "brand:" + brand + ",";
        }
        if (model !== "Choose...") {
            criteria += "model:" + model + ",";
        }
        if (cc !== "") {
            criteria += "cubic_capacity:" + cc + ","
        }
        if (FRD !== "") {
            criteria += "regDate:" + FRD + ","
        }
        if (age !== "") {
            criteria += "age:" + age + ",";
        }
        if (driverEmail !== undefined) {
            criteria += "email:" + driverEmail + ",";
        }
        if (driverPhone !== undefined) {
            criteria += "phone:" + driverPhone + ",";
        }
        if (activeFromFrom !== "") {
            criteria += "statDate>" + activeFromFrom + ",";
        }
        if (activeFromTo !== "") {
            criteria += "statDate<" + activeFromTo + ",";
        }
        if (dateOfOrderFrom !== "") {
            criteria += "orderDate>" + dateOfOrderFrom + ",";
        }
        if (dateOfOrderTo !== "") {
            criteria += "orderDate<" + dateOfOrderTo + ",";
        }
        if (inRejected) {
            criteria += "pending:false,approved:false,"
        }
        if (inAll){
            criteria+=""
        }

        console.log(criteria);
        searchPolicies(criteria);
    });
}

function searchPolicies(criteria) {
    $.ajax({
        url: `http://localhost:8080/api/policies/search?criteria=${criteria}`,
        success: function (response) {
            $('#cardContainerAgent').html('');
            if (response.length === 0) {
                $('#cardContainerAgent').append(`<h3>No Policies</h3>`);
            } else {
                $('#cardContainerAgent').append(`<h3>${response.length} Results:</h3>`);
                $.each(response, function (i) {
                    let pictureId = "policyImage_" + response[i].id;
                    $('#cardContainerAgent').append(getPolicyCard(response, i));
                    animatePictures(pictureId);
                    if (response[i].pending === false) {
                        let a = "A/R_" + response[i].id;
                        document.getElementById(a).style.display = "none";
                    }
                })
            }
        }
    })
}

function getAgentAccount() {
    emptyNav();
    $('#filters').collapse('hide');
    clearALLContainers();

    $('#cardContainerAgent').append(`<div >
  <h1 >Your Account</h1>
  <br>
    <p >First name: ${userLogedIn.firstName}</p>
    <br>
    <p >Last name: ${userLogedIn.lastName}</p>
    <br>
    <p >Email: ${userLogedIn.email}</p>
    <button onclick="editAgentAccount()" class="btn btn-primary">Edit</button>
  </div>
</div>`)
}


function editAgentAccount() {

    $('#firstNameUpdate').append(userLogedIn.firstName);
    $('#secondNameUpdate').append(userLogedIn.lastName);

    $('#cardContainerAgent').html('');
    $('#cardContainerAgent').append(`   <div style="margin-right: 300px">
     <h1>Your Account</h1>
                <form>
                           <div class="form-row">
                           <div class="form-group col-md-5">

                        <input id="firstNameUpdate" class="form-control" type="text" placeholder="First Name"><br>
                        <input id="secondNameUpdate" class="form-control" type="text" placeholder="Second Name"><br>
                        <input id="oldPassword" class="form-control" type="password" placeholder="Old Password"><br>
                        <input id="newPassword" class="form-control" type="password" placeholder="New Password"><br>
                        </div>       
                      </div>
               </form>
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />   
              <br />
              <br />
              <br /> 
              <br />
              <br />
              
                        <div style="display: flex; margin-left: 15px">
                            <div>
                               <button onclick="updateAgentAccount()" type="button" class="btn btn-primary">Save</button>
                            </div>
                            <div style="margin-left: 20px">
                               <button onclick="getAgentAccount()" type="button" class="btn btn-outline-primary">Cancel</button>
                            </div>
                          <div style="margin-left: 20px" id="errorEditAdminAccount">
                          </div>

                         </div>
</div>`);
}


function updateAgentAccount() {

    let firstName = $('#firstNameUpdate').val();
    let lastName = $('#secondNameUpdate').val();
    let oldPassword = $('#oldPassword').val();
    let newPassword = $('#newPassword').val();

    $.ajax
    ({
        type: "PUT",
        url: `http://localhost:8080/api/users/editaccount`,
        headers: {Authorization: "Bearer " + localStorage.getItem("token")},
        dataType: 'json',
        contentType: 'application/json',
        //json object to sent to the authentication url
        data: JSON.stringify({
            "firstName": firstName, "lastName": lastName,
            "email": userLogedIn.email,
            "oldPassword": oldPassword, "newPassword": newPassword
        }),
        success: function (response) {
            clearALLContainers();

            $('#cardContainerAgent').append(``);
            userLogedIn = response;
            getAgentAccount();
        },
        error: function (xhr, status, error) {
            $('#agentErroContainer').html(' ');

            let xhrResponse = JSON.parse(xhr.responseText);
            let arrayErrors = xhrResponse.errors;

            if (typeof arrayErrors === "undefined") {
                $('#agentErroContainer').append(`<h5 style="color: red">${xhrResponse.message}</h5>`);

            } else {

                $.each(arrayErrors, function (i) {
                    $('#agentErroContainer').append(`<h5 style="color: red">${arrayErrors[i].defaultMessage}</h5>`);
                })

            }
        }
    })
    $('#agentErroContainer').html(' ');

}

