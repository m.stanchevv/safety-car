function showOfferForm() {
    addCarBrandToFormOffer();

    $('#offer').html('');
    // $('body').css('background-image', 'url("../pictures/RECBLURsteinar-engeland-drw6RtOKDiA-unsplash.png")');
    $('#offer').append(`
<div class="container">
            <div class="row">
                <div class="col-md-5 col-sm-5">
                    <form id="formOffer">
                        <div class="form-group">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="inputBrandOffer">Car Brand</label>
                                    <select  id="inputBrandOffer" onchange="addCarModelToFormOffer()" class="form-control">
                                        <option id="optionsInputBrand" selected>Choose...</option>

                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputCarModelOffer">Car Model</label>
                                    <select id="inputCarModelOffer" class="form-control">
                                        <option id="optionCarModel" selected>Choose...</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="inputCubicCapacity4">Cubic Capacity</label>
                                    <input required oninput="validateCC(this)" type="text" class="form-control" id="inputCubicCapacity4"
                                           placeholder="Cubic Capacity"/>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputRegDate">First Registration Date</label>
                                    <input type='text' class="form-control" id='datetimepicker' />
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="DriverAge">Driver's age</label>
                                    <input required oninput="validateAge(this)" type="text" class="form-control" id="DriverAge"
                                     placeholder="Driver's age"/>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="form-check" style="margin-top: 30px">
                                        <input class="form-check-input" type="checkbox" id="gridCheck">
                                        <label class="form-check-label" for="gridCheck">
                                            Accidents in previous year
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                          <div class="form-action">
                        <button type="submit" class="btn btn-appoint">Get an offer</button>
                    </div>
                    </form>

                  
                </div>
                <div  id="offerOutPut" class="col-md-5 col-sm-5">


                </div>
            </div>
          
</div>`);

    $('#datetimepicker').datetimepicker({
        viewMode: 'years',
        format: 'YYYY-MM-DD'
    });

    $('#formOffer').submit(function (event) {
        event.preventDefault();
        $('#offerOutPut').html('');
        let brand = $("#inputBrandOffer option:selected").text();
        let model = $('#inputModel4 option:selected').text();
        let cc = $('#inputCubicCapacity4').val();
        let regDate = $('#datetimepicker').val();
        let age = $('#DriverAge').val();
        let hasAccidents;
        if ($('#gridCheck').is(":checked")) {
            hasAccidents = true;
        } else {
            hasAccidents = false;
        }
        console.log(regDate);

        localStorage.setItem("cc",cc);
        localStorage.setItem("regDate",regDate);
        localStorage.setItem("age",age);

        $.ajax
        ({
            type: "POST",
            url: `http://localhost:8080/api/offer`,
            dataType: 'json',
            contentType: 'application/json',
            //json object to sent to the authentication url
            data: JSON.stringify({
                "carModel": brand,
                "carBrand": model,
                "hasAccidents": hasAccidents,
                "driverAge": age,
                "cc": cc,
                "firstRegDate": regDate.toString()
            }),
            success: function (data) {
                let offerAmount = data.toFixed(2) + "lv";
                $('#offerOutPut').html('');
                $('#offerOutPut').append(`<div class="card w-50 h-50">
<div class="cardContainer">
    <div class="card-body">
        <h5 class="card-title">Your price for:</h5>
        <p class="card-text">${brand} ${model} <br> <h1>${offerAmount} </h1></p>
        <a href="#" onclick="goToRequestPolicy()" class="btn btn-appoint">Get Deal</a>
    </div>
    </div>
</div>`)
            },error: function (xhr, status, error) {
                let err = JSON.parse(xhr.responseText);
                console.log(err);
            }
        });
    })
}


function goToRequestPolicy() {
    if (localStorage.getItem('token') === null) {
        $('#logInModal').modal();
    } else {
        showMyAccount();
    }
}


function addCarBrandToFormOffer() {

    $('#inputBrandOffer').html('');
    $.ajax({
        url: `http://localhost:8080/api/cars/brands`,
        success: function (response) {
            $.each(response, function (i) {
                $('#inputBrandOffer').append(`<option>${response[i]}</option>`);

            })
        }
    })
}


function addCarModelToFormOffer() {
    let brand = $("#inputBrandOffer option:selected").text();
    $('#inputCarModelOffer').html('');
    $('#inputCarModelOffer').append(`<option selected>Choose...</option>`);
    $.ajax({
        url: `http://localhost:8080/api/cars/${brand}`,
        success: function (response) {
            $.each(response, function (i) {
                $('#inputCarModelOffer').append(`<option>${response[i]}</option>`);
            })
        }
    })
}


