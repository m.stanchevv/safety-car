// $(document).ready(function () {
//     if (localStorage.getItem('token') === null) {
//         $('#logInBtn').show();
//         $('#signUpBtn').show();
//         $('#logOutBtn').hide();
//         $('#myAccountBtn').hide();
//     }else {
//         $('#logInBtn').hide();
//         $('#signUpBtn').hide();
//         $('#logOutBtn').show();
//         $('#myAccountBtn').show();
//     }
// });


// $("body").onscroll(function () {
//     $("body").css('overflow', 'auto');
// });


$(document).ready(function () {
    if (localStorage.getItem('token') === null) {
        $('#homeMenu').append(`<li class=""><a id="logInBtn" data-toggle="modal" data-target="#logInModal">Log In</a>
                                </li>
                                <li class=""><a id="signUpBtn" data-toggle="modal" data-target="#sighUpModal">Sign up</a>
                                </li>`);
    } else {
        $('#homeMenu').append(`<li  class=""><a onclick="showModalSighOut()" id="logOutBtn">Log Out</a></li>
                                <li s class=""><a onclick="showMyAccount()" id="myAccountBtn">My account</a></li>`);
    }
});

function getUserFromToken() {
    // let user;
    let token = localStorage.getItem('token');
    $.ajax({
        url: `http://localhost:8080/api/users/me`,
        headers: {Authorization: "Bearer " + token},
        async: true,
        success: function (response) {
            console.log(response);
            userLogedIn = response;
            if (userLogedIn.roles.length === 1) {
                showUserAccountPage();
                requestPolicyStep1();
                localStorage.setItem('role', "user");
            } else if (userLogedIn.roles.includes("ROLE_ADMIN")) {
                showAdminPage();
                showPending();
                localStorage.setItem('role', "admin");
            } else {
                showAgentPage();
                showPending();
                localStorage.setItem('role', "agent");
            }
            localStorage.setItem('id', userLogedIn.id);
        }
    });
}


function showMyAccount() {
    getUserFromToken();
    if (localStorage.getItem('role') === "agent") {
        showAgentPage();
    } else if (localStorage.getItem('role') === "user") {
        showUserAccountPage();
    } else if (localStorage.getItem('role') === "admin") {
        showAdminPage();
    }
}

function jumpToHomePage() {

    $('.body').html(' ');
    $('.body').append(`<div id="mainContainer">
    <section id="banner" class="banner">
        <div class="bg-color">
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container">
                    <div class="col-md-12">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse navbar-right" id="myNavbar">
                            <ul id="homeMenu" class="nav navbar-nav">
                                <li class="active"><a href="#banner">Home</a></li>
                                <!--                                <li class=""><a id="logInBtn" data-toggle="modal" data-target="#logInModal">Log In</a>-->
                                <!--                                </li>-->
                                <!--                                <li class=""><a id="signUpBtn" data-toggle="modal" data-target="#sighUpModal">Sigh up</a>-->
                                <!--                                </li>-->
                                <!--                                <li  class=""><a onclick="showModalSighOut()" id="logOutBtn">Log Out</a></li>-->
                                <!--                                <li s class=""><a onclick="showMyAccount()" id="myAccountBtn">My account</a></li>-->
                                <li class=""><a href="#contact">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <div class="container">
                <div class="row">
                    <div class="banner-info">
                        <div class="banner-text text-center">
                            <h1 class="white">SAFETY CAR</h1>
                            <p>We are with you wherever you go</p>
                            <a href="#offer" class="btn btn-appoint">Get an offer</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--OFFER-->
    <section onload="showOfferForm()" id="offer" class="section-padding">

    </section>
    <!--/ service-->
    <!--cta-->
    <div class="line"></div>
    <div id="paralax" class="parallax">
        <div class="bg-color-paralax">
        </div>
    </div>

    <!--contact-->
    <section id="contact" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
<!--                    <h2 class="ser-title">Contact us</h2>-->
<!--                    <hr class="botm-line">-->
                </div>
                <div class="col-md-4 col-sm-4">
                    <h3>Contact Info</h3>
                    <div class="space"></div>
                    <p><i class="fa fa-map-marker fa-fw pull-left fa-2x"></i>164 "Georgi S. Rakovski" Street<br> Sofia,
                        Bulgaria
                    </p>
                    <div class="space"></div>
                    <p><i class="fa fa-envelope-o fa-fw pull-left fa-2x"></i>info@safetycar.com</p>
                    <div class="space"></div>
                    <p><i class="fa fa-phone fa-fw pull-left fa-2x"></i>+359 800 123 124</p>
                    <br>
                    <section id="contact1" class="map">

                        <iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2932.6537709651816!2d23.325448015333446!3d42.68987587916611!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40aa850cae7f7b49%3A0xd1572ff0ffa39833!2sul.+%22Graf+Ignatiev%22+25%2C+1142+Sofia+Center%2C+Sofia!5e0!3m2!1sen!2sbg!4v1564752144510!5m2!1sen!2sbg"></iframe>
                        <br/>
                        <small>
                            <a href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2932.6537709651816!2d23.325448015333446!3d42.68987587916611!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40aa850cae7f7b49%3A0xd1572ff0ffa39833!2sul.+%22Graf+Ignatiev%22+25%2C+1142+Sofia+Center%2C+Sofia!5e0!3m2!1sen!2sbg!4v1564752144510!5m2!1sen!2sbg"></a>
                        </small>
                    </section>
                </div>
                <div class="col-md-8 col-sm-8 marb20">
                    <div class="contact-info">
                        <h3 class="cnt-ttl">Having Any Query? Or want to Book an appointment?</h3>
                        <div class="space"></div>
                        <form class="contactForm">
                            <div class="form-group">
                                <input oninput="validateName(this)" type="text" name="name"
                                       class="form-control br-radius-zero" id="name" required
                                       placeholder="Your Name"/>
                            </div>
                            <div class="form-group">
                                <input oninput="validateEmail(this)" type="email" class="form-control br-radius-zero"
                                       name="email" id="email" required
                                       placeholder="Your Email"/>
                            </div>

                            <div class="form-group">
                                <input oninput="validateName(this)" type="text" class="form-control br-radius-zero"
                                       name="subject" id="subject" required
                                       placeholder="Subject"/>
                            </div>
                            <div class="form-group">
                            <textarea oninput="validateText(this)" class="form-control br-radius-zero" name="message"
                                      rows="10" id="messageText" required
                                      data-msg="Please write something for us" placeholder="Message"></textarea>
                            </div>
                            <div id="sendmessageContactForm"></div>
                            <div id="errormessageContactForm"></div>

                            <div class="form-action">
                                <button type="submit" class="btn btn-appoint">Send Message</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!--/ contact-->
    <!--footer-->
    <footer id="footer">
<!--        <div class="top-footer">-->
<!--            <div class="container">-->
<!--                <div class="marb20">-->
<!--                    <div class="ftr-tle">-->
<!--                        <h4 class="white no-padding">About Us</h4>-->
<!--                    </div>-->
<!--                    <div class="info-sec">-->
<!--                        <p>Praesent convallis tortor et enim laoreet, vel consectetur purus latoque penatibus et dis-->
<!--                            parturient.</p>-->
<!--                    </div>-->
<!--                </div>-->

<!--            </div>-->
<!--        </div>-->
        <div class="footer-line">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        © Copyright Medilab Theme. All Rights Reserved
                        <div class="credits">
                            <!--
                              All the links in the footer should remain intact.
                              You can delete the links only if you purchased the pro version.
                              Licensing information: https://bootstrapmade.com/license/
                              Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Medilab
                            -->
                            Designed by <a href="https://bootstrapmade.com/">BootstrapMade.com</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<div id="modalContainer">
    <div class="modal fade" id="logInModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="wrapper fadeInDown">
            <div id="formContent">
                <button style="margin: 10px" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <br><br><br>
                <!-- Login Form -->
                <form>
                    <input type="email" id="loginSignIn" class="fadeIn second" name="login" placeholder="Email">
                    <input type="password" id="passwordSignIn" class="fadeIn third" name="login"
                           placeholder="Password">
                    <div id="errormessageLogIn"></div>
                    <input type="button" onclick="signIn()" class="fadeIn fourth" value="Log In">

                </form>

                <!-- Remind Passowrd -->
                <div id="formFooter">
                    <a class="underlineHover" href="#">Forgot Password?</a>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade" id="sighUpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="wrapper fadeInDown">
            <div id="formContent1">
                <button style="margin: 10px" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <br><br>
                <!-- SignUp Form -->
                <form id="signUpForm">
                    <br>
                    <input required oninput="validateName(this)" type="text" id="firstNameSignUp" class="fadeIn second"
                           placeholder="First name">
                    <input required oninput="validateName(this)" type="text" id="secondNameSignUp" class="fadeIn second"
                           placeholder="Second name">
                    <input required oninput="validateEmail(this)" type="email" id="emailSignUp" class="fadeIn second"
                           placeholder="Email">
                    <input required oninput="validatePassword(this)" type="password" id="passwordSignUp" class="fadeIn third" placeholder="password">
                    <input required type="password" id="confirmPassword" class="fadeIn third"
                           placeholder="Repeat password">
                    <div id="errormessageSignUp"></div>
                    <div class="form-action">
                        <input type="submit" class="fadeIn fourth" value="Sign Up!"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--/ footer-->

<!--<script src="scripts/js/jquery.min.js"></script>-->
<!--<script src="scripts/js/jquery.easing.min.js"></script>-->
<!--<script src="scripts/js/bootstrap.min.js"></script>-->
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/v4.0.0/src/js/bootstrap-datetimepicker.js"></script>
<script src="scripts/js/custom.js"></script>
<script src="scripts/main.js"></script>
<script src="scripts/validations.js"></script>
<script src="scripts/messages.js"></script>
<script src="scripts/requestPolicyFunctionality.js"></script>
<script src="scripts/offer.js"></script>
<script src="scripts/userAccount.js"></script>
<script src="scripts/adminAccount.js"></script>
<script src="scripts/agentAccount.js"></script>
<script src="scripts/signInFunctionality.js"></script>
<script src="scripts/signUpFunctionality.js"></script>
<script src="scripts/policyFunctionality.js"></script>
<script src="scripts/testagetnpagejs.js"></script>
<script src="scripts/referenceTable.js"></script>
<script>$('#datetimepicker1').datetimepicker();</script>


<!--<script src="scripts/contactform/contactform.js"></script>-->

<script src="https://bossanova.uk/jexcel/v3/jexcel.js"></script>
<script src="https://bossanova.uk/jsuites/v2/jsuites.js"></script>

<!-- Font Awesome JS -->
<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js"
        integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ"
        crossorigin="anonymous"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js"
        integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY"
        crossorigin="anonymous"></script>

`)
}
