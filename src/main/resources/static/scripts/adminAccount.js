function addCss(fileName) {
    let link = $("<link />", {
        rel: "stylesheet",
        type: "text/css",
        href: fileName
    })
    $('head').append(link);
}

function showAdminPage() {
    $('#mainContainer').html('');
    $('link[rel=stylesheet][href*="style"]').remove();

    addCss("styles/style3.css");

    $('#mainContainer').append(` 
  <style>
        /*
    DEMO STYLE
*/
        @import "https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700";


        body {
            font-family: 'Poppins', sans-serif;
            background: #fafafa;
        }

        p {
            font-family: 'Poppins', sans-serif;
            font-size: 1.1em;
            font-weight: 300;
            line-height: 1.7em;
            color: #999;
        }

        a, a:hover, a:focus {
            color: inherit;
            text-decoration: none;
            transition: all 0.3s;
        }

        .navbar {
            padding: 15px 10px;
            background: #fff;
            border: none;
            border-radius: 0;
            margin-bottom: 40px;
            box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.1);
        }

        .navbar-btn {
            box-shadow: none;
            outline: none !important;
            border: none;
        }

        .line {
            width: 100%;
            height: 1px;
            border-bottom: 1px dashed #ddd;
            margin: 40px 0;
        }

        /* ---------------------------------------------------
            SIDEBAR STYLE
        ----------------------------------------------------- */
        #sidebar {
            width: 250px;
            position: fixed;
            top: 0;
            left: 0;
            height: 100vh;
            z-index: 999;
            background: #367585;
            color: #fff;
            transition: all 0.3s;
        }

        #sidebar.active {
            margin-left: -250px;
        }

        #sidebar .sidebar-header {
            padding: 20px;
            background: #3d8394;
        }

        #sidebar ul.components {
            padding: 20px 0;
            border-bottom: 1px solid #437c8b;
        }

        #sidebar ul p {
            color: #fff;
            padding: 10px;
        }

        #sidebar ul li a {
            padding: 10px;
            font-size: 1.1em;
            display: block;
        }

        #sidebar ul li a:hover {
            color: #3e95a0;
            background: #fff;
        }

        #sidebar ul li.active > a, a[aria-expanded="true"] {
            color: #fff;
            background: #3a909b;
        }


        a[data-toggle="collapse"] {
            position: relative;
        }

        a[aria-expanded="false"]::before, a[aria-expanded="true"]::before {
            content: '\\e259';
            display: block;
            position: absolute;
            right: 20px;
            font-family: 'Glyphicons Halflings';
            font-size: 0.6em;
        }

        a[aria-expanded="true"]::before {
            content: '\\e260';
        }


        ul ul a {
            font-size: 0.9em !important;
            padding-left: 30px !important;
            background: #36828c;
        }

        ul.CTAs {
            padding: 20px;
        }

        ul.CTAs a {
            text-align: center;
            font-size: 0.9em !important;
            display: block;
            border-radius: 5px;
            margin-bottom: 5px;
        }

        a.download {
            background: #fff;
            color: #3d8f99;
        }

        a.article, a.article:hover {
            background: #377c86 !important;
            color: #fff !important;
        }


        /* ---------------------------------------------------
            CONTENT STYLE
        ----------------------------------------------------- */
        #content {
            width: calc(100% - 250px);
            padding: 40px;
            min-height: 100vh;
            transition: all 0.3s;
            position: absolute;
            top: 0;
            right: 0;
        }

        #content.active {
            width: 100%;
        }


        /* ---------------------------------------------------
            MEDIAQUERIES
        ----------------------------------------------------- */
        @media (max-width: 768px) {
            #sidebar {
                margin-left: -250px;
            }

            #sidebar.active {
                margin-left: 0;
            }

            #content {
                width: 100%;
            }

            #content.active {
                width: calc(100% - 250px);
            }

            #sidebarCollapse span {
                display: none;
            }
        }
        
        
            
        
.img-thumbnail{
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}
.img-thumbnail:hover {opacity: 0.7;}


.img-modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (Image) */
.img-modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Add Animation - Zoom in the Modal */
.img-modal-content {
    animation-name: zoom;
    animation-duration: 0.6s;
}

@keyframes zoom {
    from {transform:scale(0)}
    to {transform:scale(1)}
}

/* The Close Button */
.img-close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}

.img-close:hover,
.img-close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .img-modal-content {
        width: 100%;
    }
}



    </style>
 <div onload="showPending()" class="wrapper">
    <nav id="sidebar">
        <div class="sidebar-header">
            <a style="font-size: 25px; font-weight: bolder" href="">Safety Car</a>
        </div>

        <ul class="list-unstyled components">
            <p style="font-weight: bolder">Admin Page</p>
<!--            <li class="active">-->
<!--                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false">Policies</a>-->
<!--                <ul class="collapse list-unstyled" id="homeSubmenu">-->
<!--                    <li><a onclick="showPending()" href="#">Pending</a></li>-->
<!--                    <li><a onclick="showApproved()" href="#">Approved</a></li>-->
<!--                    <li><a onclick="showRejected()" href="#">Rejected</a></li>-->
<!--                    <li><a onclick="showCanceled()" href="#">Canceled</a></li>-->
<!--                </ul>-->
<!--            </li>-->
     <li>
                <a onclick="showPending()" >Policies</a>
            </li>
              <li >
                <a onclick="requestPolicyStep1()" >Request a Policy</a>
          
            </li>
            <li>
                <a onclick="getAllUsers()" >Users</a>
            </li>
             <li>
                 <a onclick="showCreateAgent()" class="upload">Create Agent</a>
             </li>
              <li>
                <a onclick="getReferenceTable()" >Reference table</a>
            </li>
             <li>
                <a onclick="getAdminAccount()">Account</a>
            </li>
             <li>
                <a onclick="showModalSighOut()">Log Out</a>
            </li>
             </ul>
          <ul class="list-unstyled CTAs">
<!--            <li><a onclick="showUploadReferenceTable()" class="upload">Upload new table</a></li>-->
            <li><a style="font-size: 15px" onclick="getAllMessages()">Messages</a></li>
          </ul>
          </nav>
    
    <!-- Page Content Holder -->
    <div id="content">

        <nav class="navbar navbar-default">
            <div class="container-fluid">

                <div class="navbar-header">
                    <button style="background-color: #3d8394" type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                        <i class="glyphicon glyphicon-align-left"></i>
                        <span>Toggle Sidebar</span>
                    </button>
                </div>
                
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                    <li><button data-toggle="collapse" href="#filters"  style="background-color: #3d8394;margin-right: 2px" type="button" id="filtersBtn" class="btn btn-info navbar-btn">
                        <i class="glyphicon glyphicon-align-right"></i>
                        <span>Filters</span>
                    </button></li>
                </ul>
                </div>
                </div>
              </nav><div class="collapse  navbar navbar-default"  id="filters">
                        <div class="row">         
  <div class="col-md-12">   
  <div class="container">       
<div class="btn-group" role="group" aria-label="Basic example">
  <button type="button" onclick="showPending()" class="btn btn-info navbar-btn">Pending</button>
  <button type="button" onclick="showApproved()" class="btn btn-info navbar-btn">Approved</button>
  <button type="button" onclick="showRejected()" class="btn btn-info navbar-btn">Rejected</button>
  <button type="button" onclick="showCanceled()" class="btn btn-info navbar-btn">Canceled</button>
  <button type="button" onclick="showAll()" class="btn btn-info navbar-btn">All</button>
</div></div></div></div>
  <form id="filterForm">
<br/>
     <div class="form-row">
        <div class="col-md-3">             
            <label  for="carBrandFilter">Car Brand</label>
             <select onchange="addCarModelToForm()" id="inputCarBrandBrand" class="form-control is-invalid">
                                <option id="optionSelectedCarBrand" value="">Choose...</option>
                            </select>
        </div>
        <div class="form-group col-md-3">
            <label for="carModelFilter">Car Model</label>
              <select id="inputCarModel" class="form-control is-invalid">
                                <option id="optionCarModel" value="">Choose...</option>
                            </select>
        </div>
        <div class="col-md-3">
            <label for="ccFilter">Cubic Capacity</label>
            <input type="text" class="form-control is-invalid" id="ccFilter"/>
        </div>
          <div class="form-group col-md-3">
            <label for="datetimepickerFRD">First Registration Date</label>
                <input type='text' class="form-control" id='datetimepickerFRD'/>
        </div>
    </div>
      <div class="form-row">
           <div class="form-group col-md-3">
            <label for="datetimepickerAFfrom">Active from (from)</label>
            <input type='text' class="form-control" id='datetimepickerAFfrom'/>
        </div> 
        <div class="form-group col-md-3">
            <label for="datetimepickerAFto">Active from (to)</label>
            <input type='text' class="form-control" id='datetimepickerAFto'/>
        </div> 
           <div class="form-group col-md-3">
            <label for="datetimepickerDOfrom">Date ordered (from)</label>
            <input type='text' class="form-control" id='datetimepickerDOfrom'/>
        </div> 
          <div class="form-group col-md-3">
            <label for="datetimepickerDOto">Date ordered (to)</label>
            <input type='text' class="form-control" id='datetimepickerDOto'/>
        </div> 

              </div>
                 <div class="form-row">
       <div class="form-group col-md-3">
            <label for="driverAgeFilter">Driver's age</label>
            <input oninput="validateAge(this)" type="text" class="form-control" id="driverAgeFilter"/>
        </div>
         <div class="form-group col-md-3">
            <label for="driverEmailFilter">Driver's email</label>
            <input oninput="validateEmail(this)" type="email" class="form-control" id="DriverEmailFilter" />
        </div>
         <div class="form-group col-md-3">
            <label for="driverPhoneFilter">Driver's phone</label>
            <input oninput="validatePhone(this)" type="text" class="form-control" id="DriverPhoneFilter" />
        </div>
        <div style="margin-top: 30px" class="form-group col-md-3">
                   <div class="form-check"> 
           <input class="form-check-input" type="checkbox" id="gridCheckAccidents">
            <label class="form-check-label" for="gridCheckAccidents">   
                Accidents
            </label>
            </div>
               </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-3">
       <button style="background-color: #3d8394" type="submit" id="filtersBtn" class="btn btn-info navbar-btn">
                    <i class="glyphicon glyphicon-search"></i><span> Search</span></button>

<button style="background-color: #3d8394" type="reset" class="btn btn-info navbar-btn">
                    <i class="glyphicon glyphicon-remove"></i><span> Reset</span></button></div>
    </div>
   
     </form>
     </div>
              
        <div onload="showAdminPage()" id="cardContainerAgent">
        </div>      
        <div id="adminAccountContainer">
        </div> 
        <div id="userAccountContainer">
        </div>
           <div id="refContainer">
                </div>  

        </div>
        </div>
  

<script type="text/javascript">
    $(document).ready(function () {
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar, #content').toggleClass('active');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
        
    });
    
  
</script>
    

 
 `);

    document.getElementById("bs-example-navbar-collapse-1").style.display = "none";

    $('#datetimepickerFRD').datetimepicker({
        viewMode: 'years',
        format: 'YYYY-MM-DD'
    });

    $('#datetimepickerAFfrom').datetimepicker({
        viewMode: 'years',
        format: 'YYYY-MM-DD'
    });
    $('#datetimepickerDOfrom').datetimepicker({
        viewMode: 'years',
        format: 'YYYY-MM-DD'
    });
    $('#datetimepickerAFto').datetimepicker({
        viewMode: 'years',
        format: 'YYYY-MM-DD'
    });
    $('#datetimepickerDOto').datetimepicker({
        viewMode: 'years',
        format: 'YYYY-MM-DD'
    });


    addCarBrandToForm();
    addCarModelToForm();

    $('#filterForm').submit(function (event) {
        event.preventDefault();

        let brand = $("#inputCarBrandBrand option:selected").text();
        let model = $('#inputCarModel option:selected').text();
        let cc = $('#ccFilter').val();
        let FRD = $('#datetimepickerFRD').val();
        let age = $('#driverAgeFilter').val();
        let hasAccidents;
        if ($('#gridCheckAccidents').is(":checked")) {
            hasAccidents = true;
        } else {
            hasAccidents = false;
        }
        brand.slice(" ");
        // let pending = $('#optionPending').is(":checked");
        // let canceled = $('#optionCanceled').is(":checked");
        // let rejected = !!$('#optionRejected').is(":checked");
        // let approved = $('#optionApproved').is(":checked");
        // let all = $('#optionAll').is(":checked");

        let driverEmail = $('#driverEmailFilter').val();
        let driverPhone = $('#driverPhoneFilter').val();
        let activeFromFrom = $('#datetimepickerAFfrom').val();
        let activeFromTo = $('#datetimepickerAFto').val();
        let dateOfOrderFrom = $('#datetimepickerDOfrom').val();
        let dateOfOrderTo = $('#datetimepickerDOto').val();

        let criteria = "";

        if (hasAccidents) {
            criteria += "accidents:true,";
        }
        if (inPending) {
            criteria += "pending:true,";
        }
        if (inApproved) {
            criteria += "approved:true,";
        }
        if (inCanceled) {
            criteria += "canceledByUser:true,";
        }
        if (brand !== "Choose...") {
            criteria += "brand:" + brand + ",";
        }
        if (model !== "Choose...") {
            criteria += "model:" + model + ",";
        }
        if (cc !== "") {
            criteria += "cubic_capacity:" + cc + ","
        }
        if (FRD !== "") {
            criteria += "regDate:" + FRD + ","
        }
        if (age !== "") {
            criteria += "age:" + age + ",";
        }
        if (driverEmail !== undefined) {
            criteria += "email:" + driverEmail + ",";
        }
        if (driverPhone !== undefined) {
            criteria += "phone:" + driverPhone + ",";
        }
        if (activeFromFrom !== "") {
            criteria += "statDate>" + activeFromFrom + ",";
        }
        if (activeFromTo !== "") {
            criteria += "statDate<" + activeFromTo + ",";
        }
        if (dateOfOrderFrom !== "") {
            criteria += "orderDate>" + dateOfOrderFrom + ",";
        }
        if (dateOfOrderTo !== "") {
            criteria += "orderDate<" + dateOfOrderTo + ",";
        }
        if (inRejected) {
            criteria += "pending:false,approved:false,"
        }
        if (inAll) {
            criteria += ""
        }

        console.log(criteria);
        searchPolicies(criteria);
    });
}


function getAdminAccount() {
    $('#refContainer').html("");
    emptyNav();
    $('#filters').collapse('hide');

    clearALLContainers();
    $('#errorEditAdminAccount').html(' ');

    $('#adminAccountContainer').append(`<div >
  <h1 >Your Account</h1>
  <br>
    <p >First name: ${userLogedIn.firstName}</p>
    <br>
    <p >Last name: ${userLogedIn.lastName}</p>
    <br>
    <p >Email: ${userLogedIn.email}</p>
    <button onclick="editAdminAccount()" class="btn btn-primary">Edit</button>
  </div>
</div>`)
}

function editAdminAccount() {

    $('#firstNameUpdate').append(userLogedIn.firstName);
    $('#secondNameUpdate').append(userLogedIn.lastName);
    clearALLContainers();

    $('#adminAccountContainer').append(`   <div style="margin-right: 300px">
     <h1>Your Account</h1>
                <form>
                           <div class="form-row">
                           <div class="form-group col-md-5">

                        <input id="firstNameUpdate" class="form-control" type="text" placeholder="First Name"><br>
                        <input id="secondNameUpdate" class="form-control" type="text" placeholder="Second Name"><br>
                        <input id="oldPassword" class="form-control" type="password" placeholder="Old Password"><br>
                        <input id="newPassword" class="form-control" type="password" placeholder="New Password"><br>
                        </div>       
                      </div>
               </form>
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />   
              <br />
              <br />
              <br /> 
              <br />
              <br />
              
                        <div style="display: flex; margin-left: 15px">
                            <div>
                               <button onclick="updateAdminAccount()" type="button" class="btn btn-primary">Save</button>
                            </div>
                            <div style="margin-left: 20px">
                               <button onclick="getAdminAccount()" type="button" class="btn btn-outline-primary">Cancel</button>
                            </div>
                          <div style="margin-left: 20px" id="errorEditAdminAccount">
                          </div>

                         </div>
</div>`);
}

function updateAdminAccount() {
    $('#cardContainerAgent').html('');
    let firstName = $('#firstNameUpdate').val();
    let lastName = $('#secondNameUpdate').val();
    let oldPassword = $('#oldPassword').val();
    let newPassword = $('#newPassword').val();

    $.ajax
    ({
        type: "PUT",
        url: `http://localhost:8080/api/users/editaccount`,
        headers: {Authorization: "Bearer " + localStorage.getItem("token")},
        dataType: 'json',
        contentType: 'application/json',
        //json object to sent to the authentication url
        data: JSON.stringify({
            "firstName": firstName, "lastName": lastName,
            "email": userLogedIn.email,
            "oldPassword": oldPassword, "newPassword": newPassword
        }),
        success: function (response) {
            $('#errorEditAdminAccount').html(' ');
            $('#adminAccountContainer').html('');
            $('#adminAccountContainer').append(``);
            userLogedIn = response;
            getAdminAccount();
        },
        error: function (xhr, status, error) {
            $('#errorEditAdminAccount').html(' ');

            let xhrResponse = JSON.parse(xhr.responseText);
            let arrayErrors = xhrResponse.errors;

            if (typeof arrayErrors === "undefined") {
                $('#errorEditAdminAccount').append(`<h5 style="color: red">${xhrResponse.message}</h5>`);

            } else {

                $.each(arrayErrors, function (i) {
                    $('#errorEditAdminAccount').append(`<h5 style="color: red">${arrayErrors[i].defaultMessage}</h5>`);
                })

            }
        }
    })
    $('#errorEditAdminAccount').html(' ');

}

function getAllUsers() {
    $('#refContainer').html("");
    emptyNav();
    $('#filters').collapse('hide');
    document.getElementById("bs-example-navbar-collapse-1").style.display = "block";
    clearALLContainers();

    $.ajax({
        url: `http://localhost:8080/api/admin/users`,
        "headers": {
            "Authorization": "Bearer " + localStorage.getItem('token'),
        },
        success: function (responece) {
            if (responece.length === 0) {
                $('#adminAccountContainer').append(`<h4>No users</h4>
<!--                <button onclick="requestPolicyStep1()" class="btn btn-primary">Request a policy</button>-->
                `);
            } else {
                $.each(responece, function (i) {
                    let agent = "Agent_" + responece[i].id;
                    let admin = "Admin_" + responece[i].id;
                    $('#adminAccountContainer').append(getCardUser(responece, i));
                    if (responece[i].roles.length === 1) {
                        document.getElementById(agent).style.display = "block";
                        document.getElementById(admin).style.display = "block";
                    } else if (responece[i].roles.includes("ROLE_ADMIN") && responece[i].roles.length === 2) {
                        document.getElementById(agent).style.display = "block";
                        document.getElementById(admin).style.display = "none";
                    } else if (responece[i].roles.includes("ROLE_ADMIN") && responece[i].roles.length === 3) {
                        document.getElementById(agent).style.display = "none";
                        document.getElementById(admin).style.display = "none";
                    } else {
                        document.getElementById(admin).style.display = "block";
                        document.getElementById(agent).style.display = "none";
                    }

                })
            }
        }
    });
}

function getCardUser(responce, i) {
    console.log("ID: " + responce[i].id);
    console.log("ROLE: " + responce[i].roles);
    let role;
    if (responce[i].roles.length === 1) {
        role = "User"
    } else if (responce[i].roles.includes("ROLE_ADMIN")) {
        role = "Admin"
    } else if (responce[i].roles.includes("ROLE_AGENT")) {
        role = "Agent"
    }

    let card = `<div class="line"></div>
    <div  class="card">
        <div class="container" >
                    <h2>${responce[i].firstName} ${responce[i].lastName}:<a> </a> </h2> 
        <div class="card-body">
                <div class="row">
                    <div style="margin-left: 10px" class="col-sm-4">
                                   <h5 style="color: rgb(96,192,220);">Email: </h5><h5> ${responce[i].email}</h5>
                        <h5 style="color: rgb(96,192,220);">Role: </h5><h5>${role} </h5>
                      
                    </div>
                   
                </div>
               <br>
                <div style="display: flex;" >
                    <div id="Agent_${responce[i].id}" style="margin-right: 15px"  >
                    <button onclick="showMakeAGENTModal(${responce[i].id})" type="button" class="btn btn-primary">Make Agent</button>
                    </div>
                    <div id="Admin_${responce[i].id}" style="margin-right: 15px">
                    <button onclick="showMakeAdminModal(${responce[i].id})" type="button" class="btn btn-primary">Make Admin</button>
                    </div>
                    <div id="Policies_${responce[i].id}" style="margin-right: 15px">
                    <button onclick="getUserPolicies(${responce[i].id})" type="button" class="btn btn-primary">See Policies</button>
                    </div>
                </div>
              
               <div style="margin-left: 15px" id="errorMessagePolicy"></div>
                 </div>
              
            </div>
        </div>
        </div>
    </div>
</div>`;
    return card;
}

function getUserPolicies(userId) {
    clearALLContainers();

    $('#adminAccountContainer').append(`<h1>User's policies:</h1>`);
    $.ajax({
        url: `http://localhost:8080/api/users/history/${userId}`,
        "headers": {
            "Authorization": "Bearer " + localStorage.getItem('token'),
        },
        "method": "GET",
        success: function (responce) {
            $.each(responce, function (i) {
                $('#adminAccountContainer').append(`<br><br>`);
                let a = "A/R_" + responce[i].id;
                let pictureId = "policyImage_" + responce[i].id;

                $('#adminAccountContainer').append(getPolicyCard(responce, i));
                if (!responce[i].pending) {
                    document.getElementById(a).style.display = "none"
                }

                let modal = document.getElementById("myModalImg");

                let img = document.getElementById(pictureId);
                let modalImg = document.getElementById("img01");
                img.onclick = function () {
                    modal.style.display = "block";
                    modalImg.src = this.src;
                };

                let span = document.getElementsByClassName("img-close")[0];

                span.onclick = function () {
                    modal.style.display = "none";
                };

            });
            $('#adminAccountContainer').append(`<br><br>`);
            $('#adminAccountContainer').append(`<button onclick="getAllUsers()" class="btn btn-primary">Go back to users page</button>`);

        },
        error: function (xhr, status, error) {
            $('#adminAccountContainer').html(' ');
            let err = JSON.parse(xhr.responseText);
            console.log(err.status);
            if (err.status === 400) {
                $('#adminAccountContainer').append(`<h4>User has No Policies yet</h4>
                   <button onclick="getAllUsers()" class="btn btn-primary">Go back to users page?</button>
                `);
            }
        }
    });


}

function showMakeAdminModal(userId) {
    $('#modalContainer').html('');
    $('#modalContainer').append(`
    <div id="modalMakeADMIN" class="modal fade modalSignOut" tabindex="-1" role="dialog" aria-labelledby="modalSignOut" >
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Make Admin</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h6>Are you sure you want to make this user admin?</h6>
      </div>
      <div class="modal-footer">
    <button type="button" class="btn btn-primary btn-sm" onclick="makeUserAdmin(${userId})">Yes</button>
    <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" aria-label="Close">Cancel</button>
  </div>
</div>`);

    $('#modalMakeADMIN').modal("show");
}

function makeUserAdmin(userId) {
    $.ajax({
        url: `http://localhost:8080/api/admin/makeadmin/${userId}`,
        "headers": {
            "Authorization": "Bearer " + localStorage.getItem('token'),
        },
        "method": "PUT",
        success: function (responece) {
            $('#modalMakeADMIN').modal("hide");
            $('#adminAccountContainer').html('');
            $('#adminAccountContainer').append(`<h5>User is now admin.</h5>
    <button onclick="getAllUsers()" class="btn btn-primary">Go back to users page?</button>
`);

        }
    });
}

function showMakeAGENTModal(userId) {
    $('#modalContainer').html('');
    $('#modalContainer').append(`
    <div id="modalMakeAGENT" class="modal fade modalSignOut" tabindex="-1" role="dialog" aria-labelledby="modalSignOut" >
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Make Agent</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h6>Are you sure you want to make this user agent?</h6>
      </div>
      <div class="modal-footer">
    <button type="button" class="btn btn-primary btn-sm" onclick="makeUserAGENT(${userId})">Yes</button>
    <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" aria-label="Close">Cancel</button>
  </div>
</div>`);

    $('#modalMakeAGENT').modal("show");
}

function makeUserAGENT(userId) {
    $.ajax({
        url: `http://localhost:8080/api/admin/makeagent/${userId}`,
        "headers": {
            "Authorization": "Bearer " + localStorage.getItem('token'),
        },
        "method": "PUT",
        success: function (responece) {
            $('#modalMakeAGENT').modal("hide");
            $('#adminAccountContainer').html('');
            $('#adminAccountContainer').append(`<h5>User is now agent.</h5>
    <button onclick="getAllUsers()" class="btn btn-primary">Go back to users page?</button>
`);

        }
    });
}

function showCreateAgent() {
    $('#refContainer').html("");
    emptyNav();
    $('#filters').collapse('hide');
    $('#errorEditAdminAccount').html(' ');
    clearALLContainers();
    $('#adminAccountContainer').append(`   <div style="margin-right: 300px">
     <h1>Create Agent Account</h1>
        <br/>
                <form>
                           <div class="form-row">
                           <div class="form-group col-md-5">

                        <input id="firstNameCreateAgent" oninput="validateName(this)" class="form-control" type="text" placeholder="First Name" required><br>
                        <input id="secondNameCreateAgent" oninput="validateName(this)" class="form-control" type="text" placeholder="Second Name" required><br>
                    <input  type="email" id="emailCreateAgent" oninput="validateEmail(this)" class="form-control" placeholder="Email" required><br>
                        <input id="passwordCreateAgent" oninput="validatePassword(this)" class="form-control" type="password" placeholder="Password" required><br>
                        <input id="confirmPasswordCreateAgent" class="form-control" type="password" placeholder="Repeat password" required><br>
                        </div>       
                      </div>
                      
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />   
              <br />
              <br />
              <br /> 
              <br />
              <br /> 
              <br />
              <br />
              <br />
                         <div style="display: flex; margin-left: 15px">
                            <div>
                               <button onclick="createAgent()" type="button" class="btn btn-primary">Create Agent</button>
                            </div>
                      
                          <div style="margin-left: 20px" id="errorCreateAgent">
                          </div>

                         </div>
                                        </form>

</div>`);
}

function createAgent() {
    let firstName = $('#firstNameCreateAgent').val();
    let secondName = $('#secondNameCreateAgent').val();
    let newPassword = $('#passwordCreateAgent').val();
    let confirmedPass = $('#confirmPasswordCreateAgent').val();
    let newEmail = $('#emailCreateAgent').val();

    let reg = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!reg.test(newEmail)) {
        $('#errorCreateAgent').html(' ');
        $('#errorCreateAgent').append(`<h6 style="color: red">You must enter a valid email address.</h6>`);
    } else if (firstName.length < 2 && secondName.length < 2) {
        $('#errorCreateAgent').html(' ');
        $('#errorCreateAgent').append(`<h6 style="color: red">First name and last name must be between 2 and 30 characters.</h6>`);
    } else if (newPassword === confirmedPass) {
        $.ajax
        ({
            type: "POST",
            // headers: {
            //     'Access-Control-Allow-Origin': '*'
            // },
            url: `http://localhost:8080/api/admin/createagent`,
            contentType: 'application/json',
            headers: {Authorization: "Bearer " + localStorage.getItem("token")},
            // dataType: 'json',
            async: true,
            //json object to sent to the authentication url
            data: JSON.stringify({
                "firstName": firstName,
                "lastName": secondName,
                "password": newPassword,
                "email": newEmail,
            }),

            success: function (responce) {
                if (responce === "") {
                    throw new Error("invalid user");
                }
                $('#adminAccountContainer').html('');
                $('#adminAccountContainer').append(`<div >
                          <h1 >Agent created:</h1>
                          <br>
                            <p >First name: ${responce.firstName}</p>
                            <br>
                            <p >Last name: ${responce.lastName}</p>
                            <br>
                            <p >Email: ${responce.email}</p>
                            <br>
                            <p >Role: Agent</p>
                          </div>
                        </div>`)
            },
            error: function (xhr, status, error) {
                let err = JSON.parse(xhr.responseText);
                $('#errorCreateAgent').html(' ');
                $('#errorCreateAgent').append(`<h6 style="color: red">${err.message}</h6>`);
            }
        });
    } else {
        $('#errorCreateAgent').html(' ');
        $('#errorCreateAgent').append(`<h6 style="color: red">Passwords do not match</h6>`)
    }
}