let userLogedIn;

function signIn() {
    $('#errormessageLogIn').html('');
    let user;
    let logInEmail = $('#loginSignIn').val();
    let logInPassword = $('#passwordSignIn').val();


    $.ajax
    ({
        type: "POST",
        // headers: {
        //     'Access-Control-Allow-Origin': '*'
        // },
        url: `http://localhost:8080/api/users/signin`,
        contentType: 'application/json',
        // dataType: 'json',
        async: true,
        //json object to sent to the authentication url
        data: JSON.stringify({
            "password": logInPassword,
            "email": logInEmail,
        }),
        success: function (data) {
            if (data === "") {
                throw new Error("invalid user");
            }
            localStorage.setItem('token', data);
            localStorage.setItem('email', logInEmail);
            getUserFromToken();
            $('#logInModal').modal("hide");

        },
        error: function (xhr, status, error) {
            let err = JSON.parse(xhr.responseText);
            $('#errormessageLogIn').append(`<h6 style="color: red">${err.message}</h6>`);

        }
    });

    // document.getElementById('logInBtn').style.display = "none";
    // document.getElementById('signUpBtn').style.display = "none";
    // document.getElementById('logOutBtn').style.display = "block";
    // document.getElementById('myAccountBtn').style.display = "block";
}

function showModalSighOut() {
    // $('#modalContainer').html('');
    $('#modalContainer').append(`
<div id="sighOutModal" class="modal fade modalSignOut" tabindex="-1" role="dialog" aria-labelledby="modalSignOut" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Sigh Out?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h6>Are you sure you want to sign out?</h6>
      </div>
      <div class="modal-footer">
    <button type="button" class="btn btn-primary btn-sm" onclick="signOut()">Yes</button>
    <button type="button" class="btn btn-primary btn-sm" onclick="closeModal()">Cancel</button>
  </div>
</div>`);

    $('#sighOutModal').modal("show");
}

function signOut() {
    localStorage.clear();
    $('#homeMenu').append(`<li class=""><a id="logInBtn" data-toggle="modal" data-target="#logInModal">Log In</a>
                                </li>
                                <li class=""><a id="signUpBtn" data-toggle="modal" data-target="#sighUpModal">Sign up</a>
                                </li>`);

    $('#logOutBtn').hide();
    $('#myAccountBtn').hide();
    $('#sighOutModal').modal("hide");
    location.reload();
}

function closeModal() {
    $('#sighOutModal').modal("hide");
}