package com.teamproject.safetycar;

import com.teamproject.safetycar.models.CarDetails;
import com.teamproject.safetycar.models.DriverDetails;
import com.teamproject.safetycar.models.Policy;
import com.teamproject.safetycar.models.User;
import com.teamproject.safetycar.repository.CoeffRepository;
import com.teamproject.safetycar.repository.PolicyRepository;
import com.teamproject.safetycar.repository.ReferenceRepository;
import com.teamproject.safetycar.repository.UserRepository;
import com.teamproject.safetycar.security.JwtTokenProvider;
import com.teamproject.safetycar.service.PolicyServiceImpl;
import com.teamproject.safetycar.service.contracts.EmailService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.server.ResponseStatusException;

import javax.mail.MessagingException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

@RunWith(MockitoJUnitRunner.class)
public class PolicyServiceImplTests {

    @Mock
    PolicyRepository repositoryMock;
    @Mock
    UserRepository userRepository;
    @Mock
    private HttpServletRequest httpServletRequestMock;
    @Mock
    private HttpSession httpsSessionMock;
    @Mock
    private ServletContext servletContextMock;
    @Mock
    private JwtTokenProvider jwtTokenProvider;
    @Mock
    private CoeffRepository coeffRepository;
    @Mock
    private ReferenceRepository referenceRepository;
    @Mock
    private EmailService emailService;
    @InjectMocks
    PolicyServiceImpl service;

    private User user1 = new User("User01", "user1@mail.com");

    private Policy policy1 = new Policy();
    private Policy policy2 = new Policy();
    private Policy policy3 = new Policy();


    @Test
    public void approve_Should_ReturnApprovedPolicy_WhenSuccessful() {
        //Arrange
        Mockito.when(repositoryMock.findById(1)).thenReturn(policy1);
        policy1.setDriverDetails(new DriverDetails());
        policy1.getDriverDetails().setEmail("user1@mail.com");

        //Act
        Policy result = service.approvePolicy(1);


        //Assert
        Assert.assertTrue(result.isApproved());
    }

    @Test
    public void reject_Should_ReturnRejectedPolicy_WhenSuccessful() {
        //Arrange
        Mockito.when(repositoryMock.findById(1)).thenReturn(policy1);
        policy1.setDriverDetails(new DriverDetails());
        policy1.getDriverDetails().setEmail("user1@mail.com");

        //Act
        Policy result = service.rejectPolicy(1);


        //Assert
        Assert.assertFalse(result.isApproved());
    }

    @Test
    public void getAlPendingPolicies_Should_ReturnPendingPolicies_Wher_PoliciesExist() {
        //Arrange
        List<Policy> policyList = new ArrayList<>();
        policy1.setPending(true);
        policy2.setPending(true);
        policy3.setPending(true);
        policyList.add(policy1);
        policyList.add(policy2);
        policyList.add(policy3);
        Mockito.when(repositoryMock.findAllByPendingTrueAndCanceledByUserFalse())
                .thenReturn(policyList);

        //Act
        List<Policy> result = service.getPendingPolicies();

        //Assert
        Assert.assertEquals(3, result.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getAlPendingPolicies_Should_ThrowException_When_PoliciesDoesNotExist() {
        //Arrange
        Mockito.when(repositoryMock.findAllByPendingTrueAndCanceledByUserFalse())
                .thenThrow(IllegalArgumentException.class);

        //Act
        service.getPendingPolicies();
    }


    @Test
    public void getAlApprovedPolicies_Should_ReturnApprovedPolicies_Wher_PoliciesExist() {
        //Arrange
        List<Policy> policyList = new ArrayList<>();
        policy1.setApproved(true);
        policy2.setApproved(true);
        policy3.setApproved(true);
        policyList.add(policy1);
        policyList.add(policy2);
        policyList.add(policy3);
        Mockito.when(repositoryMock.findAllByApprovedTrue())
                .thenReturn(policyList);

        //Act
        List<Policy> result = service.getApprovedPolicies();

        //Assert
        Assert.assertEquals(3, result.size());
    }


    @Test(expected = IllegalArgumentException.class)
    public void getAlApprovedPolicies_Should_ThrowException_When_PoliciesDoesNotExist() {
        //Arrange
        Mockito.when(repositoryMock.findAllByApprovedTrue())
                .thenThrow(IllegalArgumentException.class);

        //Act
        service.getApprovedPolicies();
    }

    @Test
    public void getAlRejectedPolicies_Should_ReturnRejectedPolicies_Wher_PoliciesExist() {
        //Arrange
        List<Policy> policyList = new ArrayList<>();
        policy1.setApproved(false);
        policy2.setApproved(false);
        policy3.setApproved(false);
        policy1.setPending(false);
        policy2.setPending(false);
        policy3.setPending(false);
        policyList.add(policy1);
        policyList.add(policy2);
        policyList.add(policy3);
        Mockito.when(repositoryMock.findAllByApprovedFalseAndPendingFalse())
                .thenReturn(policyList);

        //Act
        List<Policy> result = service.getRejectedPolicies();

        //Assert
        Assert.assertEquals(3, result.size());
    }


    @Test(expected = IllegalArgumentException.class)
    public void getAlRejectedPolicies_Should_ThrowException_When_PoliciesDoesNotExist() {
        //Arrange
        Mockito.when(repositoryMock.findAllByApprovedFalseAndPendingFalse())
                .thenThrow(IllegalArgumentException.class);

        //Act
        service.getRejectedPolicies();
    }

    @Test
    public void getAlCanceledPolicies_Should_ReturnPoliciesCanceledByUser_When_PoliciesExist() {
        //Arrange
        List<Policy> policyList = new ArrayList<>();
        policy1.setCanceledByUser(true);
        policy2.setCanceledByUser(true);
        policy3.setCanceledByUser(true);
        policyList.add(policy1);
        policyList.add(policy2);
        policyList.add(policy3);
        Mockito.when(repositoryMock.findAllByCanceledByUserTrue())
                .thenReturn(policyList);

        //Act
        List<Policy> result = service.getCanceledByUser();

        //Assert
        Assert.assertEquals(3, result.size());
    }


    @Test(expected = IllegalArgumentException.class)
    public void getAlCancledolicies_Should_ThrowException_When_PoliciesDoesNotExist() {
        //Arrange
        Mockito.when(repositoryMock.findAllByCanceledByUserTrue())
                .thenThrow(IllegalArgumentException.class);

        //Act
        service.getCanceledByUser();
    }

    @Test
    public void getPolicy_Should_Policy_When_PolicyExists() {
        //Arrange
        Mockito.when(repositoryMock.findById(1)).thenReturn(policy1);

        //Act
        service.getPolicy(1);

        // Assert
        Mockito.verify(repositoryMock, Mockito.times(1)).findById(1);
    }

    @Test(expected = ResponseStatusException.class)
    public void getPolicy_Should_ThrowException_When_PolicyDoesntExists() {
        //Act
        service.getPolicy(1);
    }

    @Test
    public void getAllPolicies_Should_ReturnPolicies_When_PoliciesExist() {
        //Arrange
        List<Policy> policyList = new ArrayList<>();
        policyList.add(policy1);
        policyList.add(policy2);
        policyList.add(policy3);
        Mockito.when(repositoryMock.findAll())
                .thenReturn(policyList);

        //Act
        List<Policy> result = service.getAll();

        //Assert
        Assert.assertEquals(3, result.size());
    }


    @Test(expected = IllegalArgumentException.class)
    public void getAllPolicies_Should_ThrowException_When_PoliciesDoesNotExist() {
        //Arrange
        Mockito.when(repositoryMock.findAll())
                .thenThrow(IllegalArgumentException.class);

        //Act
        service.getAll();
    }

    @Test
    public void update_Should_updatePolicy_When_PolicyExists() {
        //Act
        service.update(policy1);

        // Assert
        Mockito.verify(repositoryMock, Mockito.times(1)).save(policy1);
    }

    @Test
    public void create_Should_createPolicy_When_PolicyExists() {
        //Arrange
        List<Policy> policyList = new ArrayList<>();
        policy1.setPending(true);
        policy1.setUser(user1);
        policy1.setId(1);
        policy1.setDriverDetails(new DriverDetails());
        policy1.getDriverDetails().setAge(20);
        policy1.setCarDetails(new CarDetails());
        policy1.getCarDetails().setAccidents(true);
        policyList.add(policy1);

        List<Double> accdentsCoef = new ArrayList<>();
        List<Integer> minAge = new ArrayList<>();
        List<Double> taxCoef = new ArrayList<>();

        accdentsCoef.add(1.2);
        minAge.add(20);
        taxCoef.add(0.1);

        policy1.getCarDetails().setRegDate(new Date());

        Mockito.when(coeffRepository.getAccidentCoeff()).thenReturn(accdentsCoef);
        Mockito.when(coeffRepository.getMinAge()).thenReturn(minAge);
        Mockito.when(coeffRepository.getTaxCoeff()).thenReturn(taxCoef);
        Mockito.when(jwtTokenProvider.resolveToken(httpServletRequestMock)).thenReturn("token");
        Mockito.when(jwtTokenProvider.getUsername("token")).thenReturn(user1.getEmail());
        Mockito.when(userRepository.findByEmail(user1.getEmail())).thenReturn(user1);

        userRepository.findByEmail(user1.getEmail()).setUserPolicies(policyList);

        //Act
        service.create(policy1, httpServletRequestMock);

        //Assert
        Mockito.verify(repositoryMock, Mockito.times(1)).save(policy1);
        Mockito.verify(userRepository, Mockito.times(1)).save(user1);

    }


}

