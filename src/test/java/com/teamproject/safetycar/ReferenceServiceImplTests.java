package com.teamproject.safetycar;

import com.teamproject.safetycar.models.Car;
import com.teamproject.safetycar.models.CarBrand;
import com.teamproject.safetycar.models.ReferenceTable;
import com.teamproject.safetycar.repository.CarRepository;
import com.teamproject.safetycar.repository.ReferenceRepository;
import com.teamproject.safetycar.service.CarServiceImpl;
import com.teamproject.safetycar.service.ReferenceServiceImp;
import com.teamproject.safetycar.service.contracts.EmailService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class ReferenceServiceImplTests {

    @Mock
    ReferenceRepository repositoryMock;

    @InjectMocks
    ReferenceServiceImp service;

    private ReferenceTable referenceTable = new ReferenceTable();
    private ReferenceTable referenceTable1 = new ReferenceTable();
    private ReferenceTable referenceTable2 = new ReferenceTable();


    @Test
    public void getActive_Should_ReturnReferenceTable_When_TableExists() {
        //Arrange
        List<ReferenceTable> referenceTables = new ArrayList<>();
        referenceTables.add(referenceTable);
        referenceTables.add(referenceTable1);
        referenceTables.add(referenceTable2);
        Mockito.when(repositoryMock.findAllByActiveTrue())
                .thenReturn(referenceTables);

        //Act
        List<ReferenceTable> result = service.getActive();

        //Assert
        Assert.assertEquals(3, result.size());
    }

    @Test
    public void findByDate_Should_ReturnReferenceTable_When_CorrectDateIsGiven() throws ParseException {
        //Arrange
        Date date1;
        String date = "2019-12-12";
        int lastday = Integer.parseInt(date.substring(date.length() - 1));
        lastday++;
        String d = date.substring(0, date.length() - 1) + lastday;

        date1 = new SimpleDateFormat("yyyy-MM-dd").parse(d);
        service.findByDate(date);

        //Assert
        Mockito.verify(repositoryMock, Mockito.times(1)).findByDate(date1);
    }


    @Test
    public void getDates_Should_ReturnReferenceTable_When_DateExists() {
        //Arrange
        Date date1 = new Date();
        Date date2 = new Date();
        List<Date> dates = new ArrayList<>();
        dates.add(date1);
        dates.add(date2);
        referenceTable.setDate(date1);
        referenceTable1.setDate(date2);
        Mockito.when(
                repositoryMock.getDates()
        ).thenReturn(dates);

        //Act
        List<Date> result = service.getDates();
        //Assert
        Assert.assertEquals(2, result.size());
    }

}