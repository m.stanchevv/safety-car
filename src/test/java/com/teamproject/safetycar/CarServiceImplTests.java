package com.teamproject.safetycar;

import com.teamproject.safetycar.models.Car;
import com.teamproject.safetycar.models.CarBrand;
import com.teamproject.safetycar.models.Message;
import com.teamproject.safetycar.models.dto.MessageResponseDTO;
import com.teamproject.safetycar.repository.CarRepository;
import com.teamproject.safetycar.repository.MessagesRepository;
import com.teamproject.safetycar.service.CarServiceImpl;
import com.teamproject.safetycar.service.MessageServiceImpl;
import com.teamproject.safetycar.service.contracts.CarService;
import com.teamproject.safetycar.service.contracts.EmailService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class CarServiceImplTests {

    @Mock
    CarRepository repositoryMock;

    @Mock
    private EmailService emailService;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    CarServiceImpl service;

    private Car car = new Car();
    private Car car1 = new Car();
    private Car car2 = new Car();


    @Test
    public void getAllCars_Should_ReturnCars_When_CarsExists() {
        //Arrange
        List<Car> carList = new ArrayList<>();
        carList.add(car);
        carList.add(car1);
        carList.add(car2);
        Mockito.when(repositoryMock.findAll())
                .thenReturn(carList);

        //Act
        List<Car> result = service.getAll();

        //Assert
        Assert.assertEquals(3, result.size());
    }

    @Test
    public void getAllModelsForBrand_Should_ReturnModels_When_CorrectBrandIsGiven() {
        //Arrange
        CarBrand audi = new CarBrand();
        audi.setBrand("Audi");
        car.setCarBrand(audi);
        car1.setCarBrand(audi);
        car2.setCarBrand(audi);
        List<Car> carList = new ArrayList<>();
        carList.add(car);
        carList.add(car1);
        carList.add(car2);
        Mockito.when(repositoryMock.findAll())
                .thenReturn(carList);

        //Act
        List<String> result = service.getAllModelsForBrand("Audi");

        //Assert
        Assert.assertEquals(3, result.size());
    }

}