package com.teamproject.safetycar;

import com.teamproject.safetycar.exception.CustomException;
import com.teamproject.safetycar.models.Policy;
import com.teamproject.safetycar.models.Role;
import com.teamproject.safetycar.models.User;
import com.teamproject.safetycar.models.dto.MessageResponseDTO;
import com.teamproject.safetycar.models.dto.UserResponseDTO;
import com.teamproject.safetycar.models.dto.UserUpdateAccountDTO;
import com.teamproject.safetycar.repository.PolicyRepository;
import com.teamproject.safetycar.repository.UserRepository;
import com.teamproject.safetycar.security.JwtTokenProvider;
import com.teamproject.safetycar.service.UserServiceImpl;
import com.teamproject.safetycar.service.contracts.EmailService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.mail.MessagingException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {

    @Mock
    UserRepository repositoryMock;
    @Mock
    PolicyRepository policyRepository;
    @Mock
    private HttpServletRequest httpServletRequestMock;
    @Mock
    private HttpSession httpsSessionMock;
    @Mock
    private ServletContext servletContextMock;
    @Mock
    private EmailService emailService;
    @Mock
    private UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private JwtTokenProvider jwtTokenProvider;
    @Mock
    private AuthenticationManager authenticationManager;
    @InjectMocks
    UserServiceImpl service;

    private User user1 = new User("User01", "user1@mail.com");
    private User user2 = new User("User02", "user2@mail.com");
    private User user3 = new User("User03", "user3@mail.com");

    private Policy policy1 = new Policy();
    private Policy policy2 = new Policy();
    private Policy policy3 = new Policy();

    @Test
    public void signUp_Should_CallRepositoryCreate_When_EmailIsUniqueAndPasswordIsCorrect() throws MessagingException {
        String[] stringArray = new String[5];
        emailService.sendSSLMessage(stringArray, "status", "subject", "message");
        Mockito.when(passwordEncoder.encode(user1.getPassword())).thenReturn("encodedPassword");

        // Act
        service.signup(user1);

        // Assert
        Mockito.verify(repositoryMock, Mockito.times(1)).save(user1);
    }

    @Test(expected = CustomException.class)
    public void signUp_Should_ThrowException_When_EmailIsNotUnique() {
        //Arrange
        Mockito.when(repositoryMock.existsByEmail(user1.getEmail()))
                .thenReturn(true);
        // Act
        service.signup(user1);
    }


    @Test(expected = CustomException.class)
    public void signUp_Should_ThrowException_When_PasswordIsNotFilledIn() {
        //Arrange
        User user1 = new User("", "user1@mail.com");
        // Act
        service.signup(user1);
    }

    @Test(expected = CustomException.class)
    public void signUp_Should_ThrowException_When_PasswordIsNotCorrect() {
        //Arrange
        User user1 = new User("123", "user1@mail.com");
        // Act
        service.signup(user1);
    }

    @Test
    public void getPendingRequests_Should_ReturnPendingPolicyList_When_PoliciesExists() {
        //Arrange
        List<Policy> policyList = new ArrayList<>();
        policy1.setPending(true);
        policy1.setUser(user1);
        policy1.setId(1);
        policyList.add(policy1);

        Mockito.when(jwtTokenProvider.resolveToken(httpServletRequestMock)).thenReturn("token");
        Mockito.when(jwtTokenProvider.getUsername("token")).thenReturn(user1.getEmail());
        Mockito.when(repositoryMock.findByEmail(user1.getEmail())).thenReturn(user1);

        repositoryMock.findByEmail(user1.getEmail()).setUserPolicies(policyList);


        //Act
        List<Policy> policiesPending = service.getPendingPolicies(httpServletRequestMock);

        //Assert
        Assert.assertEquals(1, policiesPending.get(0).getId());
    }

    @Test
    public void getHistoryRequests_Should_ReturnPolicyHistoryList_When_PoliciesExists() {
        //Arrange
        List<Policy> policyList = new ArrayList<>();
        policy1.setPending(false);
        policy1.setApproved(true);
        policy1.setUser(user1);
        policyList.add(policy1);

//        Mockito.when(jwtTokenProvider.resolveToken(httpServletRequestMock)).thenReturn("token");
//        Mockito.when(jwtTokenProvider.getUsername("token")).thenReturn(user1.getEmail());
//        Mockito.when(repositoryMock.findByEmail(user1.getEmail())).thenReturn(user1);
        Mockito.when(repositoryMock.findById(1)).thenReturn(user1);
        user1.setUserPolicies(policyList);


        //Act
        List<Policy> policiesHistory = service.getUserPolicies(1);

        //Assert
        Assert.assertTrue(policiesHistory.get(0).isApproved());
    }

    @Test
    public void cancelPolicyRequest_Should_SetPolicyToCanceledByUser_When_PoliciesBelongsToUser() {
        //Arrange
        List<Policy> policyList = new ArrayList<>();
        policy1.setUser(user1);
        policyList.add(policy1);


        Mockito.when(jwtTokenProvider.resolveToken(httpServletRequestMock)).thenReturn("token");
        Mockito.when(jwtTokenProvider.getUsername("token")).thenReturn(user1.getEmail());
        Mockito.when(repositoryMock.findByEmail(user1.getEmail())).thenReturn(user1);
        Mockito.when(policyRepository.findById(1)).thenReturn(policy1);

        user1.setUserPolicies(policyList);


        //Act
        service.cancelPolicyRequest(1, httpServletRequestMock);

        //Assert
        Assert.assertTrue(user1.getUserPolicies().get(0).isCanceledByUser());
    }

    @Test(expected = IllegalArgumentException.class)
    public void cancelPolicyRequest_Should_ThrowException_When_PolicyDoesNotBelongToUser() {
        //Arrange
        List<Policy> policyList = new ArrayList<>();
        policy1.setUser(user2);
        policyList.add(policy1);


        Mockito.when(jwtTokenProvider.resolveToken(httpServletRequestMock)).thenReturn("token");
        Mockito.when(jwtTokenProvider.getUsername("token")).thenReturn(user1.getEmail());
        Mockito.when(repositoryMock.findByEmail(user1.getEmail())).thenReturn(user1);
        Mockito.when(policyRepository.findById(1)).thenReturn(policy1);

        user2.setUserPolicies(policyList);


        //Act
        service.cancelPolicyRequest(1, httpServletRequestMock);
    }

    @Test
    public void update_Should_CallRepositoryUpdate_When_UserWithIdExists() {
//        //Arrange
        UserUpdateAccountDTO userUpdateAccountDTO = new UserUpdateAccountDTO();
        userUpdateAccountDTO.setEmail("user1@mail.com");
        Mockito.when(repositoryMock.findByEmail("user1@mail.com")).thenReturn(user1);
        userUpdateAccountDTO.setFirstName("NewFirstName");
        user1.setFirstName("OldFirstName");


        //Act
        User result = service.update("user1@mail.com", userUpdateAccountDTO);

        //Assert
        Mockito.verify(repositoryMock, Mockito.times(1)).save(user1);
    }

    @Test(expected = NullPointerException.class)
    public void update_Should_ThrowException_When_PasswordDoesntMatchOldPass() {
        //Arrange
        UserUpdateAccountDTO userUpdateAccountDTO = new UserUpdateAccountDTO();

        //Act
        User result = service.update("user1@mail.com", userUpdateAccountDTO);
    }

    @Test
    public void whoAmI_Should_ReturnUser_When_UserExists() {

        //Arrange
        Mockito.when(jwtTokenProvider.resolveToken(httpServletRequestMock)).thenReturn("token");
        Mockito.when(jwtTokenProvider.getUsername("token")).thenReturn(user1.getEmail());
        Mockito.when(repositoryMock.findByEmail(user1.getEmail())).thenReturn(user1);

        //Act
        User userResult = service.whoami(httpServletRequestMock);

        //Assert
        Assert.assertEquals(user1.getEmail(), userResult.getEmail());

    }

    @Test
    public void refresh_Should_ReturnToken_When_UserIsLoggedIn() {

        //Arrange
        List<Role> roles = new ArrayList<>();
        roles.add(Role.ROLE_USER);
        user1.setRoles(roles);
        Mockito.when(repositoryMock.findByEmail(user1.getEmail())).thenReturn(user1);
        Mockito.when(jwtTokenProvider.createToken(user1.getEmail(), user1.getRoles())).thenReturn("token");


        //Act
        String token = service.refresh(user1.getEmail());

        //Assert
        Assert.assertEquals("token", token);

    }

    @Test
    public void signIn_Should_ReturnToken_When_UserIsLoggedIn() {

        //Arrange
        List<Role> roles = new ArrayList<>();
        roles.add(Role.ROLE_USER);
        user1.setRoles(roles);
        Mockito.when(repositoryMock.findByEmail(user1.getEmail())).thenReturn(user1);
        Mockito.when(jwtTokenProvider.createToken(user1.getEmail(), user1.getRoles())).thenReturn("token");


        //Act
        String token = service.signin(user1);

        //Assert
        Assert.assertEquals("token", token);

    }

    @Test
    public void createAgent_Should_CallRepositoryCreate_When_EmailIsUniqueAndPasswordIsCorrect() {

        Mockito.when(passwordEncoder.encode(user1.getPassword())).thenReturn("encodedPassword");
        List<Role> roles = new ArrayList<>();
        roles.add(Role.ROLE_USER);
        user1.setRoles(roles);
        // Act
        service.createAgent(user1);

        // Assert
        Mockito.verify(repositoryMock, Mockito.times(1)).save(user1);
    }

    @Test
    public void search_Should_ReturnUser_When_UserExists() {
        //Arrange
        Mockito.when(repositoryMock.findByEmail("user1@mail.com")).thenReturn(user1);

        //Act
        User result = service.search("user1@mail.com");

        //Assert
        Assert.assertEquals("User01", result.getPassword());
    }

    @Test(expected = CustomException.class)
    public void search_Should_ThrowException_When_UserWithEmailDoesNotExists() {

        //Act
        service.search("user4@mail.com");
    }


    @Test
    public void getAllUsers_Should_ReturnUsers_Wher_UsersExists() {
        //Arrange
        List<User> userList = new ArrayList<>();
        userList.add(user1);
        userList.add(user2);
        userList.add(user3);
        Mockito.when(repositoryMock.findAll())
                .thenReturn(userList);

        //Act
        List<User> result = service.getAllUsers();

        //Assert
        Assert.assertEquals("user1@mail.com", result.get(0).getEmail());
    }


    @Test
    public void delete_Should_DeleteUser_When_UserExists() {
        //Act
        service.delete(user1.getEmail());

        // Assert
        Mockito.verify(repositoryMock, Mockito.times(1)).deleteByEmail(user1.getEmail());
    }

    @Test
    public void makeUserAdmin_Should_AddRoleAdminToUser_When_UserExists() {
        //Arrange
        user1.setId(1);
        List<Role> roles = new ArrayList<>();
        roles.add(Role.ROLE_USER);
        user1.setRoles(roles);
        Mockito.when(repositoryMock.findById(1)).thenReturn(user1);
        //Act
        service.makeUserAdmin(1);
        //Assert
        Assert.assertEquals(Role.ROLE_ADMIN, user1.getRoles().get(1));
    }

    @Test
    public void makeUserAgent_Should_AddRoleAgentToUser_When_UserExists() {
        //Arrange¬
        user1.setId(1);
        List<Role> roles = new ArrayList<>();
        roles.add(Role.ROLE_USER);
        user1.setRoles(roles);
        Mockito.when(repositoryMock.findById(1)).thenReturn(user1);

        //Act
        service.makeUserAgent(1);
        //Assert
        Assert.assertEquals(Role.ROLE_AGENT, user1.getRoles().get(1));
    }


//    @Test(expected = CustomException.class)
//    public void createAgent_Should_ThrowException_When_EmailIsNotUnique() {
//        // Act
//        service.createAgent(user1);
//    }

    @Test(expected = CustomException.class)
    public void createAgent_Should_ThrowException_When_PasswordIsNotFilledIn() {
        //Arrange
        User user1 = new User("", "user1@mail.com");
        // Act
        service.createAgent(user1);
    }

    @Test(expected = CustomException.class)
    public void createAgent_Should_ThrowException_When_PasswordIsNotCorrect() {
        //Arrange
        User user1 = new User("123", "user1@mail.com");
        // Act
        service.createAgent(user1);
    }

}
