package com.teamproject.safetycar;

import com.teamproject.safetycar.models.Message;
import com.teamproject.safetycar.models.dto.MessageResponseDTO;
import com.teamproject.safetycar.repository.MessagesRepository;
import com.teamproject.safetycar.service.MessageServiceImpl;
import com.teamproject.safetycar.service.contracts.EmailService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class MessageServiceImplTests {

    @Mock
    MessagesRepository repositoryMock;

    @Mock
    private EmailService emailService;


    @InjectMocks
    MessageServiceImpl service;

    private Message message1 = new Message();
    private Message message2 = new Message();
    private Message message3 = new Message();


    @Test
    public void getAllUsers_Should_ReturnMessages_When_MessagesExists() {
        //Arrange
        List<Message> messageList = new ArrayList<>();
        messageList.add(message1);
        messageList.add(message2);
        messageList.add(message3);
        Mockito.when(repositoryMock.findAll())
                .thenReturn(messageList);

        //Act
        List<Message> result = service.getAll();

        //Assert
        Assert.assertEquals(3, result.size());
    }

    @Test
    public void getAllWaitingResponse_Should_ReturnMessages_When_MessagesUnansweredExist() {
        //Arrange
        List<Message> messageList = new ArrayList<>();
        messageList.add(message1);
        messageList.add(message2);
        messageList.add(message3);
        Mockito.when(repositoryMock.findByRespondedFalse())
                .thenReturn(messageList);

        //Act
        List<Message> result = service.getAllWaitingResponse();

        //Assert
        Assert.assertEquals(3, result.size());
    }


    @Test
    public void create_Should_CallRepositorySave_When_MessageIsCreated() {
        // Act
        service.create(message1);

        //Assert
        Mockito.verify(repositoryMock, Mockito.times(1)).save(message1);
    }

    @Test
    public void respond_Should_CallRepositorySave_When_MessageIsAnswered() {
        MessageResponseDTO messageResponseDTO = new MessageResponseDTO();
        messageResponseDTO.setInitialMessageID(1);
        Mockito.when(repositoryMock.findById(1)).thenReturn(message1);
        // Act
        service.respond(messageResponseDTO);

        //Assert
        Mockito.verify(repositoryMock, Mockito.times(1)).save(message1);
    }

    @Test
    public void getById_Should_ReturnMessgae_When_MessageExists() {
        message1.setName("Message1");
        Mockito.when(repositoryMock.findById(1)).thenReturn(message1);
        // Act
        Message message = service.getMessageById(1);

        //Assert
        Assert.assertEquals("Message1", message.getName());
    }


}

