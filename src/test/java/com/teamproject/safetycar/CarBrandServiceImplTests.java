package com.teamproject.safetycar;

import com.teamproject.safetycar.models.Car;
import com.teamproject.safetycar.models.CarBrand;
import com.teamproject.safetycar.repository.CarBrandRepository;
import com.teamproject.safetycar.repository.CarRepository;
import com.teamproject.safetycar.service.CarBrandServiceImpl;
import com.teamproject.safetycar.service.CarServiceImpl;
import com.teamproject.safetycar.service.contracts.CarBrandService;
import com.teamproject.safetycar.service.contracts.EmailService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class CarBrandServiceImplTests {

    @Mock
    CarBrandRepository repositoryMock;

    @InjectMocks
    CarBrandServiceImpl service;

    private CarBrand carBrand = new CarBrand();
    private CarBrand carBrand1 = new CarBrand();
    private CarBrand carBrand2 = new CarBrand();


    @Test
    public void getAll_Should_ReturnCarBrand_When_CarBrandExist() {
        //Arrange
        List<CarBrand> carBrands = new ArrayList<>();
        carBrands.add(carBrand);
        carBrands.add(carBrand1);
        carBrands.add(carBrand2);
        Mockito.when(repositoryMock.findAll())
                .thenReturn(carBrands);

        //Act
        List<String> result = service.getAll();

        //Assert
        Assert.assertEquals(3, result.size());
    }

    @Test
    public void getAllModelsForBrand_Should_ReturnModels_When_CorrectBrandIsGiven() {
        //Arrange
        carBrand.setId(1);
        Mockito.when(repositoryMock.findByBrand("Audi"))
                .thenReturn(carBrand);

        //Act
        CarBrand carBrandResult = service.getByBrand("Audi");

        //Assert
        Assert.assertEquals(1, carBrandResult.getId());
    }

}